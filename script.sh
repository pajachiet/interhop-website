#!/bin/bash

set -eux

USERNAME="$1"
PASSWORD="$2"

UA_H='user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36' 

LOGIN_R=$(curl 'https://www.doctolib.fr/login.json' \
        --cookie "doctolib-cookies.txt" \
        --cookie-jar "doctolib-cookies.txt" \
        -H "$UA_H" \
        -H 'content-type: application/json; charset=utf-8' \
        --data-raw "{\"remember\":false,\"remember_username\":false,\"username\":\"$USERNAME\",\"password\":\"$PASSWORD\",\"kind\":\"patient\"}")

curl 'https://www.doctolib.fr/account/appointments.json?page=1' \
        --cookie "doctolib-cookies.txt" \
        --cookie-jar "doctolib-cookies.txt" \
        -H "$UA_H"

exit 0
