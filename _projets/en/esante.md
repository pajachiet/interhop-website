---
name: "Caregivers"
subtitle: "Our e-health software"
order: 1
image_path: /images/projets/heart.png
show_image: false
type: main
projets_page: true
ref: esante
lang: en
---



<div class="container">
	<ul class="image-grid">
		{% assign _projets = site.projets | where: "lang", "en" | where: "type", "esante" | sort: 'order' %}
		{% for projet in _projets %}
			<li>
			<!--	<a href="{{ site.baseurl }}{{ projet.url }}"> -->
					<img src="{% include relative-src.html src=projet.image_path %}" alt="{{ person.name }}">
					<div class="details">
						<div class="name">{{ projet.name }}</div>
						<div class="position">{{ projet.subtitle }}</div>
					</div>
			<!--	</a> -->
			</li>
		{% endfor %}
		<li class="filler"></li>
	</ul>
</div>

<div class="alert alert-green" markdown="1">
### Jitsi or video conferencing
Jitsi allows for 100% open source and secure video conferencing. 
The service benefits from a high quality audio and video that facilitates your remote exchanges between health professionals or between caregivers and patients.

This solution does not require the creation of an account. The tool is accessible on PC and Mac but also on mobile and tablet.

When you create a chat room on Jitsi, a link is generated. This link is a unique key and you just have to copy it to your participants.

### Messagerie instantanée avec Element !

Element is a secure chat application for healthcare professionals.

It allows you to stay in control of your conversations, free from data mining and advertising.

Exchanges made via Element are end-to-end encrypted.

### Cryptpad : an encrypted and secure storage space

CryptPad is an alternative to Microsoft tools and proprietary cloud services. This software is privacy friendly.

All content stored in CryptPad is encrypted before being sent, which means that no one can access your data unless you give them the keys (not even us!).

### Goupile: health research forms
[Goupile](https://goupile.fr/){:target="_blank"} is a software for creating forms. It is perfect for scientific studies, especially in health.

It allows you to create Electronic Case Report Forms (or eCRF).

For your project, InterHop can open access to Goupile and develop new features adapted to your specific needs.

Are you a researcher? Do not hesitate to contact us by email or [via notre formulaire de contact]({{site.baseurl}}/en/nous).


</div>

<div class="alert alert-red" markdown="1">
### Servers "Health Data Hosting" or in French "Hébergeur de Données de Santé" HDS

To run these programs we need computers. These computers or servers must meet specific security requirements for Health. Interhop works with a certified [Health Data Host](https://esante.gouv.fr/labels-certifications/hebergement-des-donnees-de-sante){:target="_blank"}(): [GPLExpert](https://gplexpert.com/hebergement-donnees-sante-hds/){:target="_blank"}.

All data are hosted in France. Our partner, GPLExpert is strictly dependent on French and European law. Its head office is in France, in Arpajon.

We work actively to deliver these services in the necessary security conditions for health data.

Our E-health tools are not yet available. They will be available for healthcare **in the first quarter of 2021**.

</div>

Subscribe to our newsletter to stay informed.
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/en/nous/">Newsletter</a></p>

<div class="alert alert-red" markdown="1">
### Budget

900 euros / month.

We already rent the HDS servers and you can already help us by donating. :-) 
The money will first be used to finance dedicated health or HDS servers.

</div>

Help us by donating to InterHop.
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/en/dons/">Donate !</a></p>


