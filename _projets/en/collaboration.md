---
name: Our Collaborations
subtitle: Let's work together
order: 3
image_path: /images/projets/collaboration.png
show_image: true
type: main
ref: collaboration
lang: en
---

### [Goupile](https://goupile.fr){:target="_blank"}
<div class="alert alert-blue" markdown="1">
- **Goal:** Data collection form
- **Techno:**  Lit-html, c++, Postgresql
- **Contacts:** contact@goupile.fr
- **Website:** [goupile.fr](https://goupile.fr){:target="_blank"}
</div>

Goupile is a free and open-source electronic data capture application that strives to make form creation and data entry both powerful and easy. 

It is currently in development and there are no public builds yet.
It is perfect for scientific studies especially in health.

It is currently under development and there is no public version yet.

### [Toobib](https://toobib.org){:target="_blank"}
<div class="alert alert-blue" markdown="1">
- **Goal:** Open source health appointment scheduling application (test phase)
- **Gitlab:** https://framagit.org/toobib/toobib-website
- **Techno:** React, Flask, Postgres
- **Contacts:** 
  - interhop@riseup.net
  - contact@toobib.org
- **Site Internet:** [toobib.org](https://toobib.org){:target="_blank"} under construction
</div>

Toobib.org is a web application for making appointments between healthcare professionals and patients.

It is designed to protect personal data since it only stores an email associated with an appointment scheduling schedule.

The future development of Toobib.org is intended to promote decentralization and the federation of authorities. Toobib.org also uses health interoperability standards such as FHIR.


### Transmed
<div class="alert alert-blue" markdown="1">
- **Goal:** Open source application for medical transmission (test phase)
- **Techno:** React, Django, Postgres
- **Contact:** 
  - interhop@riseup.net
</div>

We'll tell you more soon.


### [Susana](https://susana.interhop.org){:target="_blank"}
<div class="alert alert-blue" markdown="1">
- **But:** Web application for medical terminology alignment
- **Gitlab:** https://framagit.org/interchu/conceptual-mapping
- **Techno:** openUI, Flask, Postgres
- **Contact:**
  - interhop@riseup.net
</div>

Susana is a web application allowing terminology alignment between source terminologies (ex hospitals one) and standard terminologies defined by the OHDSI (Observational Health Data Sciences and Informatics) teams.
It is a collaborative, international, ergonomic and user-based website.
It combines
- a free text search tool
- a user-based mapping tool

As a collaboration tool, Susana enables the convergence of terminology between databases and languages.

### Formatting and exploitation of INSEE death files
<div class="alert alert-blue" markdown="1">
- **But:** Formatting and use of INSEE death files
- **Gitlab:** https://gitlab.com/antoinelamer/fichiers_deces_insee/
- **Techno:** R, Python
- **Contact:**
  - interhop@riseup.net
</div>

INSEE makes public the files of deceased persons since 1970: [https://insee.fr/fr/information/4190491](https://insee.fr/fr/information/4190491){:target="_blank"}.

The files are updated monthly and contain the following information: surname, first names, gender, date of birth, place of birth code and wording, date of death, place of death code and death certificate number.

The python and R scripts in this directory offer the following features :
- compile and clean up annual and monthly files into a single file to facilitate record linkage operations with other data sources
- search for matches in the INSEE database from a local file
- view records (number of deaths per week)

### Transformation of SNDS to OMOP format
<div class="alert alert-blue" markdown="1">
- Goal:** Sharing information on structural mapping from [SNDS](https://www.snds.gouv.fr/SNDS/Accueil) to [OMOP](https://www.ohdsi.org/data-standardization/the-common-data-model/)
- **Gitlab:** https://framagit.org/interchu/snds-structural-mapping
- **Techno:** Python
- **Contact:**
  - interhop@riseup.net
</div>

The various organizations participating in the project are invited to submit their mapping proposals in a humanly usable format in a folder on behalf of their organization.

### Pseudonymization of Medical Reports
<div class="alert alert-blue" markdown="1">
- **But:** Pseudonymization reporting pipeline.
- **Gitlab:** https://framagit.org/interchu/omop-note-deid
- **Techno:** Spark, Python
- **Contact:**
  - interhop@riseup.net
</div>

Entity extraction, fictitious assignment, generation of de-identified reports.
