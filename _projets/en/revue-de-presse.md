---
name: Newspaper
subtitle: Our Watch
order: 2
external_name: Zotero
external_url: https://www.zotero.org/groups/2538389/revue_presse/library
image_path: /images/projets/newspaper.png
type: acces-aux-droits
redirect_from:
  - /press_en
ref: revue-de-presse
lang: en
---

# Newspaper
[« La politique publique des données de santé est à réinventer »](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html) <em><span style="color:orange;">4 juin</span></em>
- "We therefore need to rethink the subject, with clear guidelines: rebuilding trust and defining a
strategy; covering the entire health and medico-social field; simplifying access to enable
go fast; develop the use of real-time data to identify problems
anchoring the technical architecture in a decentralized ecosystem, respectful of the actors involved
and conducive to a "sovereign" control of data hosting; guarantee respect for secrecy
and the right to privacy; to promote the emergence of new technologies while respecting the
of high ethics and deontology."

[Gaia-X : la France et l'Allemagne lancent leur Airbus européen du cloud](https://www.journaldunet.com/web-tech/cloud/1491859-gaia-x-la-france-et-l-allemagne-lancent-leur-airbus-europeen-du-cloud/) <em><span style="color:orange;">4 juin</span></em>

[Pour une souveraineté européenne de la santé](https://forumatena.org/pour-une-souverainete-europeenne-de-la-sante/) <em><span style="color:orange;">27 mai</span></em>
- "Can you rely on pseudonymization when the attributes of each individual are counted in the thousands?"
- "The committee (CESREES) in charge of ruling on the public interest character of projects will only exercise its filter sporadically: only a request from the Minister of Health, the CNIL or CESREES itself will trigger the examination. The CNIL had nevertheless recommended a systematic study."


[Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852) <em><span style="color:orange;">25 mai</span></em>
- "It is a question of national policy, already raised in The Conversation France, since it is a question of having a public good managed by a private actor, and with no hope of reversibility. But it is also a political question of European digital sovereignty, since this US actor is subject to the Cloud Act, the 2018 law that allows US judges to request access to data on servers located outside the United States.

[Health Data Hub : pourquoi cette plateforme inquiète tant ?](https://www.presse-citron.net/health-data-hub-pourquoi-cette-plateforme-inquiete-tant/) <em><span style="color:orange;">18 mai</span></em>
- "The collection of this data will be supplemented by two recent files linked to Covid-19, SIDEP which groups together Laboratory data and the Covid Contact where the persons potentially contaminated with Covid-19 are listed. In other words, a considerable number of data will be stored through this new system. "
- "The stated objective of Health Data Hub would be to make the health system more efficient and to help manage the health crisis we are going through. Ultimately, all the data recorded on the social security site will be automatically integrated into the file. "

[Health Data Hub: pourquoi le fichier national qui abritera nos données de santé suscite des craintes](https://www.bfmtv.com/tech/health-data-hub-pourquoi-le-fichier-national-qui-abritera-nos-donnees-de-sante-suscite-des-craintes-1913849.html) <em><span style="color:orange;">17 mai</span></em>

[Health Data Hub : nos données de santé vont-elles être livrées aux Américains ?](https://www.01net.com/actualites/health-data-hub-nos-donnees-de-sante-vont-elles-etre-livrees-aux-americains-1913351.html) <em><span style="color:orange;">14 mai</span></em>
- "the government is pushing through its giant health platform project in the name of the state of emergency. Problem: the data would be hosted by Microsoft which could on the sole order of the United States ... transfer it across the Atlantic "

[Thomas Gomart (IFRI) : « Le Covid-19 accélère le changement de mains de pans entiers de l'activité économique au profit des plateformes numériques »](https://www.lesechos.fr/amp/1201806) <em><span style="color:orange;">13 mai</span></em>
- "European consumer data has been exploited by American platforms far beyond what users could have known. The Snowden affair in 2013 exposed certain mechanisms."
- "American platforms are stakeholders in the military-digital complex. While being private companies paying little dividends but investing a lot, they are vectors of American power. In a short time, they invented and offered services to which Europeans don't want to give up. The danger is to focus on service without wanting to see the power architecture. "
- "The fundamental challenge for Europeans is to be able to maintain their autonomy of thought."

[Données de santé : pourquoi (et comment) vont-elles être hébergées sur des serveurs de Microsoft ?](https://www-lci-fr.cdn.ampproject.org/c/s/www.lci.fr/amp/sante/donnees-de-sante-pourquoi-et-comment-vont-elles-etre-hebergees-sur-des-serveurs-de-microsoft-2153527.html) <em><span style="color:orange;">11 May</span></em>
- "shameless copying of your health information"

[La Cnil s’inquiète d’un possible transfert de nos données de santé aux Etats-Unis](https://www.mediapart.fr/journal/france/080520/la-cnil-s-inquiete-d-un-possible-transfert-de-nos-donnees-de-sante-aux-etats-unis) <em><span style="color:orange;">8 May</span></em>

[Le Health Data Hub ou le risque d’une santé marchandisée](https://lvsl.fr/le-health-data-hub-ou-le-risque-dune-sante-marchandisee/)
- "It seems that here once again lodges the fundamental contradiction between the logic of unconditional care specific to the public sector as well as to the Hippocratic oath and the requirements of efficiency and profitability today denounced by medical personnel and hospitable through their strike movements and their reactions to the Covid-19 crisis"

[Données de santé: l’Etat accusé de favoritisme au profit de Microsoft](https://www.mediapart.fr/journal/france/110320/donnees-de-sante-l-etat-accuse-de-favoritisme-au-profit-de-microsoft)
- "Failure to respect the principles of equality and transparency in the choice of Microsoft Azure"

[Pour des données de santé au service des patients](https://interhop.org/donnees-de-sante-au-service-des-patients/)
- "According to a recent survey, the hospital is even the institution in which the French have the most confidence. What would be the impact of a loss of trust if massive data breaches were found?"

[«Health Data Hub»: le méga fichier qui veut rentabiliser nos données de santé](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante)
- "Is the government setting up a "big brother medical" and offering our health data to the digital giants?"

[Opinion | Soignons nos données de santé](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640)
- "Once broken, trust is no longer rebuilt"

[Données de santé & intelligence artificielle : rencontre avec Emmanuel Bacry](https://www.actuia.com/actualite/donnees-de-sante-intelligence-artificielle-rencontre-avec-emmanuel-bacry/)
- "I would tend to think that we have to open the data completely in a completely secure place, and then we control what is happening."

[Le Cloud Act, la riposte américaine au RGPD européen](https://www.lesechos.fr/idees-debats/cercle/le-cloud-act-la-riposte-americaine-au-rgpd-europeen-139429)
- "But all these good intentions did not weigh heavily against the logic of extraterritoriality (judicial and digital) advocated by the American administration, which ratified, just two months before the implementation of the GDPR, its Cloud Act, thereby widening its prerogatives to the whole world. international… whereas the new American law is perfectly unilateral. "

[Comment les GAFAM et Capgemini s'invitent dans la mine d'or des données médicales](https://www.lalettrea.fr/action-publique_executif/2019/02/18/comment-les-gafam-et-capgemini-s-invitent-dans-la-mine-d-or-des-donnees-medicales,108345136-ge0)

# Commission nationale de l'informatique et des libertés CNIL

[Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf) <em><span style="color:orange;">20 avril</span></em>
- "The contract mentions the existence of data transfers outside the European Union as part of the day-to-day operation of the platform"
- "Concerns […] regarding access by the United States authorities to data transferred to the United States, in particular the collection and access to personal data for national security purposes"
- "With state-of-the-art algorithms from keys generated by those responsible for the platform on an encrypting box controlled by the health data platform [...]. They will be kept by the host at within an encrypting box, which has the consequence of technically allowing the latter to access the data "
- "In view of the sensitivity of the data in question, that its hosting and the services related to its management can be reserved for entities exclusively under the jurisdiction of the European Union"

[Anonymisation vs. Pseudonymisation](https://www.cnil.fr/fr/identifier-les-donnees-personnelles)

[Avis sur un projet de loi relatif à l’organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&id=CNILTEXT000038142154&fastReqId=177029739&fastPos=1)
"Beyond a simple enlargement, this evolution changes the very dimension of the SNDS, which would aim to contain all the medical data giving rise to reimbursement"

# Agence nationale de la sécurité des systèmes d'information ANSSI
[Sénat, audition de M. Guillaume Poupard, directeur général de l'ANSSI](https://videos.senat.fr/video.1608918_5ebaa04d6b8cd.audition-de-m-guillaume-poupard-directeur-general-de-l-agence-nationale-de-la-securite-des-systeme?timecode=4319000)  <em><span style="color:orange;">12 mai</span></em>
- "To emphasize the notions of sovereignty: It worries me to see the digital giants advancing on these health topics. We know very well that they know a lot about us: what we read, our emails, what 'we buy, where we are...<br><br>
If tomorrow we also entrust them with our health, there are major conflicts of interest that I see looming. What will happen one day when a company like Apple decides to take out health insurance. As they know everything, they make sensors, watches and have access to different health parameters, if we are a little paranoid and I am paid to be, they will have the idea of ​​doing health insurance with obviously what every insurer dreams of, insuring healthy people and refusing people who may not be healthy and who are expensive. And in this model, all mutualist models explode. Unless we are in good health we will no longer be insured "


# Conseil National de l'Ordre Des Medecins CNOM
[Medecins et patients dans le monde des data, des algorithmes et de l'intelligence artificielle](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)
- Recommendation # 1: “A free person and society not subject to technological giants. This fundamental ethical principle must be reaffirmed at a time when the most excessive dystopias and utopias are widely publicized"
- Recommendation # 33: “Finally, the Cnom warns that the data infrastructures, collection and exploitation platforms, constitute a major issue on the scientific, economic and cybersecurity fronts. The location of these infrastructures and platforms, their functioning, their purposes, their regulation represent a major stake of sovereignty so that, tomorrow, France and Europe will not be vassalized by supranational digital giants”
- "Respect for people's secrets is the very basis of their trust in doctors. We must therefore put this ethical requirement in the massive processing of data during the construction of algorithms”

#  Conseil National des Barreaux CNB

[Motion portant sur la creation du GIP Plateforme des donnees de sante dit HealthDataHub](https://www.cnb.avocat.fr/sites/default/files/11.cnb-mo2020-01-11_ldh_health_data_hubfinal-p.pdf)
- "CAUTION against the risks of breach of medical confidentiality and invasion of the right to privacy"

# Comite consultatif National d'Ethique CCNE

[Données massives et santé : une nouvelle approche des enjeux éthiques](https://www.ccne-ethique.fr/sites/default/files/publications/avis_130.pdf)
- "Faced with the technological challenge posed, for national and European sovereignty, the storage, sharing and processing of big data in the health field, the CCNE recommends the development of mutualized and interconnected national platforms. Open according to modalities which will have to be defined for public and private actors, they must allow our country and Europe to preserve their strategic autonomy and not to lose control of the wealth that constitutes data, while favoring controlled sharing which is essential for the efficiency of medical care and research."

[Numérique et Santé : Quels enjeux éthiques pour quelles régulations ?](https://www.ccne-ethique.fr/sites/default/files/publications/rapport_numerique_et_sante_19112018.pdf)

# Articles of law

[Décret n° 2020-551 du 12 mai 2020 relatif aux systèmes d'information](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000041869923&dateTexte=&categorieLien=id) <em><span style="color:orange;">12 mai</span></em>

[Arrêté : épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421) <em><span style="color:orange;">21 avril</span></em>

[Article 48 - RGPD - "Transferts ou divulgations non autorisés par le droit de l'Union"](https://www.privacy-regulation.eu/fr/48.htm)
- "Any decision of a court or administrative authority of a third country requiring a controller or a processor to transfer or disclose personal data cannot be recognized or made enforceable in any way unless it is based on an international agreement, such as a mutual legal assistance treaty, in force between the requesting third country and the Union or a Member State, without prejudice other grounds for transfer under this Chapter. "

[Health Data Hub mission de préfiguration](https://solidarites-sante.gouv.fr/IMG/pdf/181012_-_rapport_health_data_hub.pdf)
- "The wealth of health data is a national wealth and everyone is gradually becoming aware of it. [...] The sovereignty and independence of our health system in the face of foreign interests, as well as the competitiveness of our research and of our industry will depend on the speed of France to seize the subject. "

[Confirmation de Microsoft au Sénat](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[Naissance HealthDataHub : LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[Loi pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=id)
- "ensure that the control, sustainability and independence of its information system are preserved"
- "encourage the use of free software and open formats during the development, purchase or use, in whole or in part, of this information system"

# CloudAct - GDPR

[Mme Denis : CloudAct vs. RGPD](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543)
- “Regarding the Cloud-Act […] what is certain is that there is a conflict of law on article 48 of the GDPR”

[Évaluation du CCBE de la loi CLOUD Act des États-Unis](https://www.ccbe.eu/fileadmin/speciality_distribution/public/documents/SURVEILLANCE/SVL_Position_papers/FR_SVL_20190228_CCBE-Assessment-of-the-U-S-CLOUD-Act.pdf)
- "As recipients of a CLOUD Act warrant, technology companies will find themselves between two conflicting data laws. Since the DMPR strictly limits the circumstances under which data can be lawfully transferred to third countries and provides for heavy fines for violations (up to 4% of a company's turnover), companies are caught between the failure to comply with a US warrant (for violation of a CLOUD Act order) and the risk of significant monetary or even criminal penalties (for violation of the provisions of the DMPR)".
- "The CLOUD Act threatens to undermine the inviolability of legal advice without offering lawyers or their clients any procedural safeguards to protect the professional secrecy granted by EU law."

[Rapport Gauvain](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf) (p28-30)
- “Personal data of natural persons, such as data of legal persons are concerned”
- "In all, however, the scope of the offenses concerned is very broad and undoubtedly includes all economic, commercial and financial offenses likely to affect French companies"

[Rapport Longuet](http://www.senat.fr/notice-rapport/2019/r19-007-1-notice.html)
- "to meet this requirement [of mastering the data], but also in order to achieve, as much as possible, savings in acquisition, management, maintenance and training, several administrations have chosen to develop their own solutions IT, using software whose source codes are public. This is, for example, the case of the Gendarmerie which, since 2009, has equipped the 80,000 computer workstations with its services with free IT solutions which have enabled it to regain its independence and sovereignty vis-à-vis private publishers. It would be very useful to quickly take stock of this unique experience and assess the possibilities of its extension to other ministries. "

[Arnaud Coustillière, directeur général de la DGNUM](https://www.dailymotion.com/video/x7mhqgf)
- “We need trusted partners”
  - “A legal framework without extraterritoriality”
  - “A community of destiny”

[Microsoft devient le premier hébergeur de données de santé certifié en France](https://www.usinenouvelle.com/article/microsoft-devient-le-premier-hebergeur-de-donnees-de-sante-certifie-en-france.N765904)
- "Microsoft s’investit beaucoup dans le big data et le machine learning en santé en France"

[Selon l’avocat général Saugmandsgaard Øe, la décision 2010/87/UE de la Commission relative aux clauses contractuelles types pour le transfert de données à caractère personnel vers des sous-traitants établis dans des pays tiers est valide](https://curia.europa.eu/jcms/upload/docs/application/pdf/2019-12/cp190165fr.pdf)

# Importance of health for Gafam
[How the “Big 4” Tech Companies Are Leading Healthcare Innovation](https://healthcareweekly.com/how-the-big-4-tech-companies-are-leading-healthcare-innovation/)
- "According to Business Insider, GV is investing about one-third of its Alphabet funds into healthcare and life science startups."

[Owkin annonce une extension de sa série A et atteint 18 millions de dollars levés](https://www.maddyness.com/2018/05/23/medtech-owkin-leve-11-millions-mettre-lia-service-de-recherche-medicale/)
- "Owkin Announces Extension of Series A to Google Venture"

# Scientific articles

[Estimating the success of re-identifications in incomplete datasets using generative models](https://www.nature.com/articles/s41467-019-10933-3.pdf)
- "we find that 99.98% of Americans would be correctly re-identified in any dataset using 15 demographic attributes"

[The Biggest Cybersecurity Threats Are Inside Your Company](https://hbr.org/2016/09/the-biggest-cybersecurity-threats-are-inside-your-company)
- "IBM found that 60% of all attacks were carried out by insiders"

# Alternatives
[Teralab](https://www.teralab-datascience.fr/)
- "DATA SCIENCE FOR EUROPE: Artificial intelligence and big data platform"

[Elixir](https://elixir-europe.org/services/covid-19)
- "ELIXIR is an intergovernmental organisation that brings together life science resources from across Europe. These resources include databases, software tools, training materials, cloud storage and supercomputers. The goal of ELIXIR is to coordinate these resources so that they form a single infrastructure."

[GAIA-X](https://www.dotmagazine.online/issues/on-the-edge-building-the-foundations-for-the-future/gaia-x-a-vibrant-european-ecosystem)
- "GAIA-X: Growing a Vibrant European Ecosystem"
- "GAIA-X is an ambitious and timely project which will create an efficient, secure, distributed, and sovereign European data infrastructure in dialogue between state, industry, and research"
- "However, GAIA-X is not about trying to build the next global cloud provider, but rather about enabling the participation of the many specialized providers across Europe and  including them in a distributed and interconnected data infrastructure"

[Le CERN revient sur son projet Microsoft Alternatives (MALt) pour passer sur des solutions open source](https://www.nextinpact.com/brief/le-cern-revient-sur-son-projet-microsoft-alternatives--malt--pour-passer-sur-des-solutions-open-source-9025.htm)
- "the attractive financial conditions which had initially attracted CERN service managers have gradually disappeared to be replaced by systems based on licenses and business models adapted to the private sector"

[OVH-Outscale : le cloud souverain vraiment ressuscité ?](https://www.lemondeinformatique.fr/actualites/lire-ovh-outscale-le-cloud-souverain-vraiment-ressuscite-76657.html)

[Health Data Hub : un collectif critique le choix Microsoft](https://www.nextinpact.com/news/108781-health-data-hub-collectif-critique-choix-microsoft.htm)
- "The choice to have recourse to the American company Microsoft for the hosting of the data, without there having been a call for tenders or prior competitive call challenge the actors of the French free software"
