---
name: Justice
subtitle: Advocacy, legal actions
order: 2
image_path: /images/projets/justice.png
type: main
ref: actions-en-justice
lang: en
---

To change the law and protect your rights, Interhop wishes to develop its advocacy and strategic litigation activities.

The [last recourse before the Conseil d'Etat](https://interhop.org/2020/11/23/la-reversibilite-est-acquise) that we brought with the Santénathon collective has indeed shown that the legal tool was appropriate to influence reality.

 <div class="responsive-video-container">
	<iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/13964678-d65f-4fe7-9806-ef071441a477" frameborder="0" allowfullscreen></iframe>
</div>

We will give you more details very soon!
