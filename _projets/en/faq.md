---
name: Access to law
subtitle: Frequently Asked Questions
order: 1
image_path: /images/projets/faq.png
show_image: true
type: acces-aux-droits
ref: faq
lang: en
---

I wanted to create this FAQ to answer your questions about the RGPD. In fact, I learned a lot while building it.

**You have access to our FAQ 24/7**. It is free.

I wanted the FAQ to be **dynamic**, which means that you can enter your question, your key words, to arrive on the page where you will find the answer.

Don't hesitate to ask me, by mail (dpd@interhop.org), the questions that you won't find an answer in the FAQ. You will help me to enrich it.

You can pull my ears if I'm too technical for your taste, that will allow me to readjust.

In fact, my goal is to make you autonomous in the implementation of good practices concerning the RGPD.

<p><a style="text-align: center;" class="button alt" type="blue" target="_blank" href="https://rgpd.interhop.org">Here is the FAQ of interhop</a></p>

- **Our FAQ is free**, which means you can access it without a donation. However, I must admit that you help us by participating through a donation (even a small one).

- **You can also want to make a watch, ask a question to help you understand and apply the GDPR** in your structure, in short! Go further. Then InterHop puts at your disposal the necessary resources (lawyer, engineers, IT specialists, DPO...). If necessary, we will ask the supervisory authority to help you and ask the right question. In all cases, you will receive a written response within 24 to 72 hours. We estimate our research work at about 50 euros per question (research, team exchange, link with you...).

- **If you want us to help you to apply**, we will fill together a schedule of conditions to really answer your request and satisfy you. We will establish an estimate in adequacy with the mission that you entrust to us.

In any case, I hope our FAQ will make you happy :)

See you soon,

Chantal, DPO
