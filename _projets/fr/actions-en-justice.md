---
name: Justice
subtitle: Plaidoyer, actions en justice
order: 2
image_path: /images/projets/justice.png
type: main
ref: actions-en-justice
lang: fr
---

Pour faire changer la loi et protéger tes droits, Interhop souhaite développer ses activités de plaidoyer et de contentieux stratégique.

Le [dernier recours devant le Conseil d'Etat](https://interhop.org/2020/11/23/la-reversibilite-est-acquise) que nous avons porté avec le collectif Santénathon a en effet montré que l'outil juridique était opportun pour infléchir sur le réel.

 <div class="responsive-video-container">
	<iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/13964678-d65f-4fe7-9806-ef071441a477" frameborder="0" allowfullscreen></iframe>
</div>

Nous te donnerons plus de précisions très bientôt !
