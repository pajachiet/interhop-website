---
name: "Citoyen.ne.s"
subtitle: Accès aux droits
order: 4
external_name: RGPD
external_url: https://www.cnil.fr/fr/reglement-europeen-protection-donnees
show_image: false
type: main
projets_page: true
image_path: /images/projets/droit.png
ref: acces-aux-droits
lang: fr
---

InterHop facilite l'accès aux droits des personnes

<div class="container">
	<ul class="image-grid">
		{% assign _projets = site.projets | where: "lang", "fr" | where: "type", "acces-aux-droits" | sort: 'order' %}
		{% for projet in _projets %}
			<li>
				<a href="{{ site.baseurl }}{{ projet.url }}"> 
					<img src="{% include relative-src.html src=projet.image_path %}" alt="{{ person.name }}">
					<div class="details">
						<div class="name">{{ projet.name }}</div>
						<div class="position">{{ projet.subtitle }}</div>
					</div>
				</a> 
			</li>
		{% endfor %}
		<li class="filler"></li>
	</ul>
</div>

### Foire Aux Questions FAQ
On veut te rendre autonome dans la mise en œuvre de bonnes pratiques concernant le Réglement Général sur la Protection des Données ou RGPD.
C'est le texte qui protége les citoyen.nes européen.ne.s dans leur droits.

Voici notre [FAQ](https://rgpd.interhop.org){:target="_blank"}.

### Courrier Type

Pour faciliter les démarches administratives et réglementaires nous prérédigeons des courriers types pour faire le lien avec nos institutions.

La CNIL propose déjà [des courriers pour agir](https://www.cnil.fr/fr/modeles/courrier){:target="_blank"}.

Nous compléterons l'offre :-)
Voici un [billet de blog]({{site.baseurl}}/2020/11/03/microsoft-centralise-toujours-les-donnees-de-sante) sur le sujet et le Health Data Hub.

### Veille techno-juridique

Tu peux faire une veille grâce à InterHop :-)

InterHop relaie les informations pertinentes dans le domaine de la santé numérique, du droit européen et international.

Tu peux les retrouver sur notre [compte Zotero]({{site.baseurl}}/zotero) ou ici [interhop.org/press](https://interhop.org/press), traduits quand ils sont diffusés en anglais.
