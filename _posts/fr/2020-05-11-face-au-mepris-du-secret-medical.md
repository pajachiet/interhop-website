---
layout: post
title: "Face au mépris du secret médical, des soignants résistent !"
categories:
  - HDH
  - Microsoft
  - CloudAct
  - RGPD
  - Aide
  - Loi Pour une République Numérique
redirect_from:
  - /aidez-interhop
show_comments: true
ref: aidez-interhop
lang: fr
---

Chers ami.e.s,

Vous avez signé notre dernier texte.
Les conditions de signature étaient sans doute contraignantes puisque
vos prénoms, noms, fonction, association, entreprise ou école ainsi que
vos emails étaient requis.
Donc un énorme merci. Vous êtes un peu plus de 700 personnes à avoir
signé.

Par ailleurs, voici le  début d'un [courrier]({{site.baseurl}}/lettre-hopitaux) que nous avons
envoyé récemment aux directeurs.trices d'hôpitaux.

<!-- more -->

> Mesdames les directrices, Messieurs les directeurs d’hôpitaux,
Chers collègues, chères collègues de la santé, <br><br>
> Un arrêté<sup>[^arrete]</sup> publié le 21 avril incite les hôpitaux et centres de recherche à intensifier l'envoi des données de santé des patients dans le "Health Data Hub" (HDH), hébergé actuellement par la société Microsoft<sup>[^microsoft]</sup>.<br><br>
Au-delà des questions d'opportunité du choix de la société Microsoft[^choixmicrosoft], notamment en matière de protection des données personnelles, l'application de cet arrêté pourrait vous placer dans une situation juridique délicate au titre du deuxième alinéa de l’article 40 du Code de procédure pénale<sup>[^article40]</sup>.

Ce courrier vise à nous défendre, à vous défendre. Si les plus hauts
niveaux de certification n'étaient pas nécessaires pour le HDH, alors
pourquoi avoir choisi Microsoft ?
D'après l’article 16 de la loi Lemaire[^repnum], nos responsables auraient dû :
- "veiller à préserver la maîtrise, la pérennité et l’indépendance de son
système d’information"
- "encourager l’utilisation des logiciels libres et des formats ouverts
lors du développement, de l’achat ou de l’utilisation, de tout ou
partie, de ce système d’information"

Nous aimerions vous alerter. Certains des logiciels que vos soignants utilisent au quotidien, hébergent vos
données chez Amazon ou Microsoft. Vos soignants n'en sont peut-être pas
informés. Le principal problème est le CloudAct américain qui
s'imposerait au détriment du réglement européen RGPD. Le relation de
confiance soignant-soigné est en danger.

Merci de participer.
Soigné.e.s, alertez vos soignant.e.s sur ce sujet.
Soignant.e.s, demandez à vos prestataires de logiciels où sont les
données de vos patients.
Un exemple : où sont hébergées les données de Doctolib ? Pour
information - ou rappel ;-) - il ne suffit par que les serveurs soient
en Europe, ils doivent être sous la seule juridiction Européenne. Elles
sont peut-être stockées sur un cloud Européen comme OVH. Mais cela mérite de le
vérifier. :-) Aidez-nous !

Enfin deux choses :
- nous avons créer une rubrique : "Presse" où nous relayerons les
derniers articles [https://interhop.org/press]({{site.baseurl}}/press)
- nous avons besoin d'aide : soignant.e.s, informaticien.ne.s, juristes,
sociologues, citoyen.ne.s. Si vous pensez que l'informatique POURRAIT
participer à l'amélioration du système de santé MAIS vous voulez que les
logiciels soient LIBRES/OPENSOURCE, vous refusez l'argumentaire selon
lequel vous êtes réactionnaires et anti-inventions MAIS le partage et la
DEGOOGLISATION du monde sont importants pour vous, alors rejoignez-nous
: [https://interhop.org/rejoins-nous]({{site.baseurl}}/rejoins-nous)

Dans tous les cas, si vous pensez nos actions utiles, merci de les
relayer dans vos réseaux.

"Ceux qui pensent que c'est impossible sont priés de ne pas déranger
ceux qui essaient"

Le collectif interhop.org

[^arrete]: [Arrêté du 21 avril 2020 complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421)

[^listehebergeur]: [Liste des hébergeurs certifiés pour les données de santé](https://esante.gouv.fr/labels-certifications/hds/liste-des-herbergeurs-certifies)

[^microsoft]: [Modalités de stockage du « health data hub » -  Question écrite n° 14130 de M. Claude Raynal (Haute-Garonne - SOCR) publiée dans le JO Sénat du 30/01/2020](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^choixmicrosoft]: [Données de santé: l’Etat accusé de favoritisme au profit de Microsoft](https://www.mediapart.fr/journal/france/110320/donnees-de-sante-l-etat-accuse-de-favoritisme-au-profit-de-microsoft)

[^article40]: [Article 40 du Code de procédure pénale](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=B5F65545EFB0882820C41758AE863B27.tpdjo14v_1?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006574931&dateTexte=&categorieLien=cid)

[^repnum]: [Loi pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=id)

