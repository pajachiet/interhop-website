---
layout: post
title: "Communiqué de Presse : Courrier Ministre de la Santé"
categories:
  - Courrier
  - Ministre de la Santé
  - Contrats
  - Microsoft
  - Appel offre
redirect_from:
  - /communique-presse-ministre-sante-appel-offre
ref: communique-presse-ministre-sante-appel-offre
lang: fr
---

Envoi au ministre de la santé d’un courrier appelant à **la diffusion rapide d'un Appel d'Offre pour le choix de l'hébergeur de données de la "Plateforme des Données de Santé" ou "HealthDataHub”**.

<!-- more -->

# Objet

Publication du calendrier de la consultation pour le choix de l'hébergeur de la "Plateforme des Données de Santé".

# Contenu de la lettre

Monsieur Le Ministre,

Le déploiement d’une “Plateforme des Données de Santé” par la création d’un Groupement d’Intérêt Public, a été proclamé par la loi n° 2019-774 du 24 juillet 2019 relative à l’organisation et à la transformation du système de santé[^loisante].
Cette “Plateforme des Données de Santé” vise à développer l’intelligence artificielle dans le secteur de la santé et à devenir le guichet unique d’accès à l’ensemble des données de santé du territoire national.
Les données concernées sont celles des centres hospitaliers, des pharmacies, du dossier médical partagé et des données de recherche issues de divers registres. La quantité des données hébergées est amenée à exploser avec l’émergence de la génomique, de l’imagerie et des objets connectés.

Actuellement dans la phase de préfiguration du projet, ces données sont stockées chez Microsoft Azure[^senat_microsoft], cloud public du géant américain Microsoft. Ce choix a été critiqué par de nombreux acteurs publics[^lemonde] [^lenouvelobs] [^theconversation] et privés[^lepoint].

La CNIL dans sa délibération n° 2020-044 du 20 avril 2020[^CNILdel] fait état de « contrats fournis » (p.6 avis CNIL du 20 avril 2020) évoquant les risques de transferts de données vers des pays tiers et les divulgations non autorisées par le droit de l’Union dans le cadre du contrat de sous-traitance de la solution technique de la "plateforme" à Microsoft Azure. Le 19 juin 2020, le Conseil d'État a enjoint la "Plateforme des Données de Santé" d'informer les citoyens du "possible transfert de données hors de l'Union Européenne, compte tenu du contrat passé avec son sous-traitant"[^conseil_etat].

Consécutivement Monsieur Cédric O[^reunion_stopcovid], alors Secrétaire d'État au numérique, et vous-même, très récemment au Sénat, a**vez annoncé un Appel d'Offre pour la mise en production de cette "plateforme"**[^senat_appeloffre]. **Nous vous en remercions et vous demandons de nous en communiquer le calendrier.**

Compte tenu du nombre particulièrement important de personnes concernées (plus de 67 millions d’utilisateurs) et du caractère sensible des données personnelles contenus dans la "Plateforme des Données de santé", nous avons décidé de rendre publique ce courrier.
**Au même titre que la publication rapide du calendrier de l'Appel d'Offre, cette publicité contribue à l’objectif de transparence que nous souhaitons tous.**

Dans l'attente de votre réponse, nous vous prions de croire, Monsieur le Ministre, en l’expression de notre très haute considération.

Association interhop.org<br>

[^stopcovid_cnil]: [Application « StopCovid » : la CNIL tire les conséquences de ses contrôles](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^reunion_stopcovid]: [Conférence de presse sur l'application StopCovid, le 23 juin](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#)

[^CNIL_allemande]: [Berlin: Berlin Commissioner issues statement on Schrems II case, asks controllers to stop data transfers to the US](https://www.dataguidance.com/news/berlin-berlin-commissioner-issues-statement-schrems-ii-case-asks-controllers-stop-data)

[^curia]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^arrete_sortie_etat_urgence]: [ Arrêté du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042106233&dateTexte=&categorieLien=id)

[^ccomptes]: [LES DONNÉES PERSONNELLES DE SANTÉ GÉRÉES PAR L'ASSURANCE MALADIE](https://www.ccomptes.fr/sites/default/files/EzPublish/20160503-donnees-personnelles-sante-gerees-assurance-maladie.pdf)

[^senat_appeloffre]: [Protection des données de santé : MORIN-DESAILLY Catherine](http://videos.senat.fr/video.1710660_5f10400c7efbf.seance-publique-du-16-juillet-2020-apres-midi?timecode=16471000)


[^reunin_stopcovid]: [Conférence de presse sur l'application StopCovid, le 23 juin](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#)


[^senat_microsoft]: [Modalités de stockage du « health data hub »](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^CNILdel]: [Mesures d’organisation et de fonctionnement du système de santé nécessaires pour faire face à l’épidémie de covid-19 dans le cadre de l’état d’urgence sanitaire ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^loisante]: [LOI n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[^theconversation]: [The Conversation - Données de santé : l’arbre StopCovid qui cache la forêt Health Data Hub ](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^lenouvelobs]: [Le Nouvel Obs - Nos données de santé à Microsoft ? « On offre aux Américains une richesse nationale unique au monde »](https://www.nouvelobs.com/sante/20200623.OBS30391/nos-donnees-de-sante-a-microsoft-on-offre-aux-americains-une-richesse-nationale-unique-au-monde.html)

[^lemonde]: [Le Monde - « La politique publique des données de santé est à réinventer »](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html)

[^lepoint]: [Le Point - Health Data Hub : « Le choix de Microsoft, un contresens industriel ! »](https://amp.lepoint.fr/2379394?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter&Echobox=1591814194&__twitter_impression=true)

[^conseil_etat]: [Conseil d'État, 19 juin 2020, Plateforme Health Data Hub](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)
