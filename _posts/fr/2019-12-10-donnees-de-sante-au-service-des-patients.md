---
layout: post
title: "Pour des données de santé au service des patients"
categories: 
  - Presse
  - Tribune 
  - LeMonde
  - HDH
  - Microsoft
  - CloudAct
  - RGPD
redirect_from:
  - /donnees-de-sante-au-service-des-patients
show_comments: true
ref: tribune_monde
lang: fr
---

[Signez ce texte](https://forms.interhop.org/node/3).

> Alors que le gouvernement compte s’appuyer sur le géant américain
pour stocker les
données de santé, un [collectif](https://pad.interhop.org/s/Sk7K8qpaS#) initié par des professionnels du secteur
et de l’informatique
médicale s’inquiète, dans une tribune [au « Monde »](https://www.lemonde.fr/idees/article/2019/12/10/l-exploitation-de-donnees-de-sante-sur-une-plate-forme-de-microsoft-expose-a-des-risques-multiples_6022274_3232.html), de ce choix du
privé.

Le gouvernement français propose le déploiement d’une plate-forme
nommée
[Health Data Hub](https://solidarites-sante.gouv.fr/actualites/presse/communiques-de-presse/article/communique-de-presse-agnes-buzyn-health-data-hub-officiellement-cree-lundi-2) (HDH) pour développer l’intelligence artificielle
appliquée à la santé.
Le HDH vise à devenir un guichet unique d’accès à l’ensemble des
données de santé.

Les données concernées sont celles des centres hospitaliers, des
pharmacies, du dossier médical partagé et les données de recherche
issues de divers registres. La quantité des données hébergées est
amenée
à exploser, notamment avec l’émergence de la génomique, de l’imagerie
et
des objets connectés. Il est prévu que ces données soient stockées chez
[Microsoft Azure](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640), cloud public du géant américain Microsoft. Ce choix
est
au centre de nos
inquiétudes.


<!-- more -->

Les GAFAM (Google, Apple, Facebook, Amazon et Microsoft), les start-up
et même les [assureurs](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante?onglet=full) pourraient accéder aux données de santé et au
pouvoir financier qu’elles représentent, si ces entreprises démontrent
que leurs projets de recherche peuvent avoir un usage pour ["l'intérêt public"](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id), un concept relativement
flou.


En outre, l’utilisation de Microsoft est encadrée par des licences
payantes. Même si des discussions sont menées pour assurer la
réversibilité de la plate-forme américaine, il paraît difficile d’en
changer. Nous connaissons les risques d’une captivité numérique, avec
notamment les contrats passés entre [Microsoft et les hôpitaux](https://www.nextinpact.com/news/96401-le-contrat-a-plus-120-millions-d-euros-entre-microsoft-irlande-et-hopitaux-francais.htm).


## Une rupture du secret médical ?

Le gouvernement américain a adopté en 2018 un texte nommé [Cloud Act](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf),
qui permet à la justice américaine d’avoir accès aux données stockées
dans des pays tiers. La présidente de la Commission nationale de
l’informatique et des libertés (CNIL) a affirmé, en septembre,
à l'[Assemblée Nationale](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543) que ce texte est contraire au [Règlement Général sur la Protection des Données](http://www.privacy-regulation.eu/fr/48.htm) (RGPD), qui protège les citoyens
européens. Concrètement, les patients pourraient être soumis à une
rupture du secret médical, ce qui constitue un danger aussi personnel
que symbolique, l’intégrité du serment d’Hippocrate étant remise en
cause.

De plus, le HDH se développe sur un modèle centralisé, avec pour
conséquence un impact plus élevé en cas de piratage informatique. On
pourrait penser que les GAFAM proposent des solutions ultra-sécurisées.
Cet argument ne tient pas. En effet, les attaques viennent souvent de
l’intérieur, c’est-à-dire des [personnels ayant accès aux données](https://hbr.org/2016/09/the-biggest-cybersecurity-threats-are-inside-your-company).

Bien que les données hébergées par le HDH soient désidentifiées,
l’anonymat complet est impossible, car il suffit de croiser un nombre
limité de données pour [réidentifier un patient](https://www.nature.com/articles/s41467-019-10933-3.pdf). En outre, la base de
données médico-administrative du Système national des données de santé
(SNDS), intégrée dans le HDH, a été critiquée par la CNIL pour
l´[obsolescence de son algorithme de chiffrement](https://www.silicon.fr/snds-cnil-tousse-securite-grand-fichier-sante-166527.html).

La confiance constitutive de la relation de soin entre patients et
soignants repose sur de multiples facteurs, dont le secret, qui est
essentiel. Selon un récent sondage, l’hôpital est même l’institution en
laquelle les Français ont le plus [confiance](https://www.egora.fr/actus-pro/hopitaux-cliniques/45857-confiance-des-francais-l-hopital-champion-des-institutions). Quel serait l’impact d’une
perte de confiance si des fuites de données massives étaient
avérées ?

## Il existe des alternatives

Nous sommes convaincus de l’intérêt de la recherche sur données et du
développement des outils statistiques en médecine. Cependant, il existe
des alternatives qui protègent la vie privée et le secret médical, en
garantissant l’indépendance et le contrôle collectif des
infrastructures.

Depuis plusieurs années, les hôpitaux créent des entrepôts de données
de santé avec l’objectif de collecter celles générées localement pour
les analyser. Un effort est fait pour favoriser la décentralisation et
l’échange entre les régions et nos voisins [européens](https://www.ehden.eu/), tout en préservant la sécurité des données.

Les chercheurs et les centres hospitaliers ont une expertise
importante, car ils produisent et collectent des données avec, pour
objectif, une évolution vers des hôpitaux numériques. Ainsi, le
développement des nouvelles technologies au sein des hôpitaux va
renforcer l’interconnexion entre le soin et la
recherche.

L’Organisation européenne pour la recherche nucléaire (CERN) a
récemment lancé le projet [Malt](https://home.cern/fr/news/news/computing/migrating-open-source-technologies), pour Microsoft Alternatives, visant à
remplacer un maximum de logiciels commerciaux par des logiciels libres.
Nous pourrions suivre cet exemple et promouvoir des « clouds »
autogérés.

## Favoriser la décentralisation

La décentralisation associée à l’interopérabilité des systèmes
d’information et à l'[apprentissage fédéré](https://www.substra.ai/fr/accueil) (par opposition à l’approche
centralisée) contribue à promouvoir la recherche en réseau en
préservant, d’une part, la confidentialité des données, d’autre part,
la
sécurité de leur
stockage.

Cette technique permet de faire voyager les algorithmes dans chaque
centre partenaire sans centraliser les données. La décentralisation
maintient localement les compétences (ingénieurs, soignants)
nécessaires
à la qualification des données de
santé.

L’exploitation de données de santé sur une plate-forme
« propriétaire », comme celle de Microsoft, expose à des risques
multiples. L’incompatibilité Cloud Act-RGPD, l’autonomie numérique de
l’Europe ainsi que la possible perte de confiance des patients sont des
problématiques importantes à mettre au centre du débat
citoyen.

> « Il est essentiel de garder la main sur les technologies employées
et d’empêcher la privatisation de la santé » 

Comme l’avait fait le [Conseil National de l'Ordre des Médecins](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf), nous
réaffirmons un
principe fondamental : « Agissons pour que la
France et l’Europe ne soient pas vassalisées par les géants
supranationaux du numérique. » Les données de santé sont à la fois un
bien d’usage des patients et le patrimoine inaliénable de la
collectivité. Il est essentiel de garder la main sur les technologies
employées et d’empêcher la privatisation de la
santé.

# Signataires

La liste complète des [signataires](https://pad.interhop.org/s/Sk7K8qpaS#).

[Signez ce texte](https://forms.interhop.org/node/3).
