---
layout: post
title: "Saisine de la CNIL concernant les conséquences de la jurisprudence Schrems II sur les acteurs numériques français de la santé"
categories:
  - RGPD
  - CNIL
  - GAFAM
ref: saisine-cnil-doctolib-lifen-alan
lang: fr
show_comments: true
---

Madame La Présidente de la CNIL,

Suite à l'arrêt Schrems II, et à l’avis rendu par la Commission Nationale de l'Informatique et des Libertés, à l’occasion d'un contentieux ouvert par le collectif SantéNathon.org, en octobre dernier devant le Conseil d’État, sous le numéro 444937, les membres signataires de ce courrier rappellent que les données de santé sont des données sensibles. C’est pourquoi nous demandons à la CNIL :
- d'analyser les conséquences de la jurisprudence Schrems II relativement à l'hébergement des données personnelles chez les GAFAMs, notamment pour l'ensemble des acteurs privées en santé susceptibles de sous-traiter leurs données auprès de sociétés soumises au droit américain,
- de demander à ce que l'ensemble des traitements de données de santé, dès lors qu'ils induisent un transfert vers les États-Unis, soit stoppé.

<!-- more -->
    
Nous avons décidé de rendre public ce courrier. 

Cette publicité contribue à l’objectif de transparence défendu par votre Commission. 

En vous remerciant de l’attention que vous porterez à notre demande, nous vous prions d’agréer, Madame la Présidente,  nos salutations les plus respectueuses.

Fait à Paris, le 05 décembre 2020 

Signataires:
- Syndicat National des Jeunes Médecins Généralistes (SNJMG)
- Syndicat de la Médecine Générale (SMG)
- Syndicat de l'Union française pour une médecine (UFMLS)
- InterSyndicale Nationale des Internes (ISNI)
- ActupienNEs
- Association Constances
- Association France Hémophiles (AFH) 
- InterHop
- Syndicat national des journalistes (SNJ)
- Union générale des ingénieurs, cadres et techniciens (UGICT CGT)
- Observatoire de la transparence dans les politiques du médicament (OTMEDS)
- Conseil National du Logiciel Libre (CNLL)
- Ploss Auvergne Rhône-Alpes
- Fonds de Dotation du Libre
- Monsieur Bernard Fallery
- Monsieur Didier Sicard



# Courrier long

Titre: Saisine de la CNIL concernant les conséquences de la jurisprudence Schrems II sur les acteurs numériques français de la santé

## Concernant l'arrêt C-311/18 (Schrems II)

"Le règlement général sur la protection des données de l'UE a été adopté dans un double but : 
- faciliter la libre circulation des données à caractère personnel au sein de l'Union européenne, 
- tout en préservant les libertés et droits fondamentaux des personnes, notamment leur droit à la protection des données à caractère personnel."[^edpb_schrems]

"Dans son récent arrêt C-311/18[^curia_cjue] (Schrems II) la Cour de Justice de l'Union Européenne (CJUE) rappelle que la protection accordée aux données à caractère personnel dans l'Espace économique européen (EEE) doit s'appliquer sur les données où qu'elles se trouvent"[^edpb_schrems].  Le transfert de données à caractère personnel vers des pays tiers ne peut être un moyen d'affaiblir la protection qui est accordée aux citoyen.ne.s européens dans le cadre du RGPD. 
"La Cour affirme également que le niveau de protection dans les pays tiers ne doit pas équivalent à celui garanti dans l'EEE"[^edpb_schrems].

La Cour avait aussi considéré que “les exigences du droit américain […] entraînent des limitations de la protection des données personnelles qui ne sont pas circonscrites de manière à satisfaire à des exigences essentiellement équivalentes à celles requises par le droit de l’UE”[^curia_cjue]. En clair “tout transfert de données vers les États-Unis présente un risque”[^article].

## Concernant l'avis de la CNIL[^cnil_conseil_etat_2]
 
Dans son avis rendu à l’occasion d'un contentieux ouvert par le collectif SantéNathon.org en octobre dernier devant le Conseil d’État[^conseil_etat_2], la CNIL se questionne longuement sur les conséquences de deux textes de lois américains. Ces textes régissent les pouvoirs des services de renseignements.

Le premier est le Foreign Intelligence Surveillance Act (FISA). Il concerne le ciblage “des personnes dont on peut raisonnablement penser qu’elles se trouvent en dehors des États-Unis” et s’applique “aux fournisseurs de services de communications électroniques.” Ce texte opaque s’applique à Microsoft.

Le deuxième se nomme l’Executive Order. Ce texte est un décret présidentiel qui légalise les techniques d’interception des signaux “en provenance” ou “vers” les États-Unis.

Selon la CNIL, et sur la base de ces deux textes, Microsoft reste soumis aux injonctions des services de renseignements américains qui peuvent l’obliger à tout moment à transférer l’ensemble des données hébergées.

L'avis de la CJUE puis cet avis de la CNIL sont des “big bang réglementaires”. L’arrêt “Schrems II” de la CJUE justifie désormais l’urgence à coordonner une réponse technique au niveau européen.

Le 10 novembre dernier, le Comité Européen de Protection des Données a donc rappelé qu'au regard de la toute récente jurisprudence européenne, les autorités de contrôle ("CNILs européennes") "suspendront ou interdiront les transferts de données dans les cas où, à la suite d'une enquête ou d'une plainte, elles constateront qu'un niveau de protection essentiellement équivalent ne peut être assuré"[^edpb_schrems].

## Concernant les acteurs français de la santé utilisant les services des GAFAMs

Qu’en est-il des entreprises digitales en santé (Doctolib[^doctolib_cgu], Lifen[^lifen_cgu], Alan, Recare, Medadom[^medadom]) ? La plupart stocke les données de santé chez Amazon Web Service, Microsoft Azure (AWS) ou Google Cloud Platform. Est-ce toujours conforme au RGPD ?

Sur son site Internet Recare mentionne que des "données [personnelles] peuvent être traitées en dehors de l'EEE, notamment aux États-Unis d'Amérique. Nous avons conclu des clauses contractuelles types de l'UE avec le fournisseur de services afin de garantir un niveau adéquat de protection des données"[^recare].


Dans une interview récente Alan[^alan] explique : “en chiffrant les données hébergées, on empêche que des personnes qui y accèderaient éventuellement de manière non autorisée (y compris l’hébergeur lui-même) puissent en prendre connaissance. Toutes nos données sur AWS sont chiffrées au repos. Ce qui veut dire que même quelqu’un qui aurait accès physiquement aux serveurs […] ne pourrait pas lire les données”.

Alan reconnaît néanmoins que “quand on choisit un hébergeur, il y a aussi un certain nombre de choses qu’on ne peut pas contrôler, comme la transparence dont fait preuve l’hébergeur, les lois de son pays d’origine, ou le contrôle qu’on laisse à l’hébergeur sur ses serveurs et l’accès qu’il y donne à des tiers”.

Finalement Alan reconnaît que “si, suite à une saisine, un tribunal américain demandait à accéder aux serveurs d’AWS de manière valable et incontestable en droit américain, il le pourrait. Le fait que nous chiffrions les données nous permet d’ajouter une barrière technique à la lecture dans ces cas”.

Même si certaines startups, comme Doctolib, stockent les clefs de chiffrement chez un autre hébergeur soumis au droit européen[^doctolib_chiffrement], ceci ne resoudrait pas à lui seul les problématiques liées au FISA/Executive Order et à l’accès effectif aux données non chiffrées (données en clair).

Azure et Amazon utilisent de nombreux services. Ces services nécessitent l’utilisation de données déchiffrées pour les outils d’analyse statistiques (calcul de moyenne, de médiane …), d’intelligence arficielle ou même de visualisation des données.

Confier les clés de chiffrement à un autre hébergeur n’entraîne pas la fin de l’accès aux données en clair lors de l’usage de ces services annexés à l’hébergement des données.

En effet, en l’état actuel des connaissances, il n’est pas possible de faire des opérations informatiques complexes sur des données chiffrées (la technique dite de “chiffrement homomorphique” est actuellement à l’état de recherche et n’est donc pas utilisée en production chez Microsoft Azure[^homomorphe]). Microsoft et Amazon doivent donc avoir accès aux données déchiffrées. Il reste donc possible sur demande du gouvernement américain d’avoir accès aux données lorsque celles-ci sont en cours d’analyse (processeurs, mémoire vive…).

Pour protéger réellement les données de santé conformément au droit européen, il faudrait donc associer de façon concomittante :
- Une gestion des clés de chiffrement chez un hébergeur européen (n’ayant aucune activité aux États-Unis)
- Une absence totale de traitement au sein d’une plateforme soumise au droit américain (qui supposerait sinon pour cette dernière, soit d’avoir accès en clair aux données, soit d’avoir les clés de déchiffrement)

## Concernant les recommandations du Collectif SantéNathon

Le collectif SantéNathon.org :
- RAPPELLE que les données de santé sont des données sensibles comme définies par la CNIL[^cnil_donnee_nodate]. 
- DEMANDE à la CNIL de définir des modalités de consentement et de droits opposables (droit d'information, d'opposition et de rectifications). Ces modalités doivent être facilement compréhensibles par les citoyen.ne.s
- DEMANDE à la CNIL d'analyser les conséquences de la jurisprudence Schrems II pour l'hébergement des données personnelles chez les GAFAMs, notamment pour les acteurs privées en santé
- DEMANDE au régulateur de stopper les traitements en cas de transfert de données vers les Etats-Unis

Nous avons décidé de rendre publique ce courrier. Cette publicité contribue à l’objectif de transparence défendu par votre Commission[^transparence].

Nous vous prions d’agréer, Madame La Présidente, l’expression de nos salutations distinguées.

Fait à Paris, le 05 décembre 2020 

Signataires:
- Syndicat National des Jeunes Médecins Généralistes (SNJMG)
- Syndicat de la Médecine Générale (SMG)
- Syndicat de l'Union française pour une médecine (UFMLS)
- InterSyndicale Nationale des Internes (ISNI)
- ActupienNEs
- Association Constances
- Association France Hémophiles (AFH) 
- InterHop
- Syndicat national des journalistes (SNJ)
- Union générale des ingénieurs, cadres et techniciens (UGICT CGT)
- Observatoire de la transparence dans les politiques du médicament (OTMEDS)
- Conseil National du Logiciel Libre (CNLL)
- Ploss Auvergne Rhône-Alpes
- Fonds de Dotation du Libre
- Monsieur Bernard Fallery
- Monsieur Didier Sicard

[^doctolib_chiffrement]: [Doctolib: « Lorsque vous faites le chiffrement des données, l’hébergeur a peu d’importance »](https://www.frenchweb.fr/doctolib-lorsque-vous-faites-le-chiffrement-des-donnees-lhebergeur-a-peu-dimportance/402057)

[^cnil_donnee_nodate]: [https://www.cnil.fr/fr/definition/donnee-sensible](https://www.cnil.fr/fr/definition/donnee-sensible)

[^edpb_schrems]: [Recommendations 01/2020 on measures that supplement transfer tools to ensure compliance with the EU level of protection of personal data](https://edpb.europa.eu/sites/edpb/files/consultation/edpb_recommendations_202001_supplementarymeasurestransferstools_en.pdf)

[^curia_cjue]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^homomorphe]: [Pourquoi le Health Data Hub travestit la réalité sur le chiffrement des données de santé sur Microsoft Azure](https://interhop.org/2020/06/15/healthdatahub-travestit-le-chiffrement-des-donnees)

[^cnil_conseil_etat_2]: [Avis CNIL, Conseil d'Etat,  Mémoire en Observation](https://www.documentcloud.org/documents/7224049-Me-moireCnilHDH.html)

[^conseil_etat_2]: [Health Data Hub et protection de données personnelles : des précautions doivent être prises dans l’attente d’une solution pérenne](https://www.conseil-etat.fr/actualites/actualites/health-data-hub-et-protection-de-donnees-personnelles-des-precautions-doivent-etre-prises-dans-l-attente-d-une-solution-perenne)

[^alan]: [Où nous hébergeons les données de nos membres et pourquoi](https://blog.alan.com/tech-et-produit/pourquoi-nous-hebergeons-sur-aws)

[^article]: [L'annulation du Privacy Shield doit s'appliquer immédiatement, tranche la Cnil européenne](https://www.usine-digitale.fr/article/l-annulation-du-privacy-shield-doit-s-appliquer-immediatement-tranche-la-cnil-europeenne.N989069)

[^transparence]: [Application « StopCovid » : la CNIL tire les conséquences de ses contrôles ](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^recare]: [Recare : Politique de protection des données personnelles](https://www.recaresolutions.fr/protection-des-donnes-personnelles)

[^medadom]: [https://www.medadom.com/cgu](https://www.medadom.com/cgu)

[^doctolib_cgu]: [https://www.doctolib.fr/terms/agreement](https://www.doctolib.fr/terms/agreement)

[^lifen_cgu]: [https://www.lifen.fr/confidentialite](https://www.lifen.fr/confidentialite)


