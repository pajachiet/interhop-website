---
layout: post
title: "Logiciel libre et logiciel open source : quelles différences?"
categories:
  - définition
ref: opensource-libre-difference
lang: fr
---

On comprend assez facilement ce qu'est un logiciel propriétaire. Le terme privateur est parfois utilisé ; il permet d'ailleurs de mieux comprendre les enjeux de privation de liberté liés à ce type de logiciel[^privateur]. 
Par contre, nous mélangeons souvent les notions de **logiciel libre** et de **logiciel open source**. 

<!-- more -->

D’abord l’idée du **"libre"** est plus ancienne que celle de **l’"open source"**. 

En fait, la différence entre logiciel libre et logiciel open source n'est pas forcément facile à établir. Elle se situe dans les valeurs et dans la vision du monde que chacun porte.

Fin des années 90, certains membres de la communauté du logiciel libre ont commencé à utiliser **le terme d’"open source" au lieu de "libre"**, ce qui a engendré la constitution de deux mouvements qui aujourd’hui peuvent s'unir pour travailler ensemble tout en étant bien séparés.

Le mouvement du libre est social alors que le mouvement de l'open source met en avant la méthodologie de développement et de diffusion du logiciel. 

Les deux mouvements ne veulent pas être confondus.

**Le libre est un mouvement qui soutient des valeurs philosophiques et politiques.[^logiciel_libre]**

Selon Richard STALLMAN[^stallman], programmeur, militant du logiciel libre et initiateur du mouvement du logiciel libre, la différence fondamentale entre les deux concepts réside dans leur philosophie : *« L'open source est une méthodologie de développement ; le logiciel libre est un mouvement de société»*[^opensource-libre].

La définition du libre défendue par Richard STALLMAN dès 1980 est constituée de 4 libertés[^4_lib] :

1. Liberté d'exécuter le programme, pour tous les usages

2. Liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins

3. Liberté de redistribuer des copies. **Principe ou philosophie sous-jacente : aider autrui**

4. Liberté d'améliorer le programme et de publier les améliorations. **Philosophe sous-jacente : en faire profiter toute la communauté**


## En résumé

**Le logiciel libre est forcément open source.** Il n'est pas forcément gratuit. La confusion vient du fait que le mot "free" est traduit par libre mais aussi par gratuit.

Par contre **un logiciel peut être open source sans être libre** au sens où l'entend la Free  Software Foundation (FSF) ou « Fondation pour le logiciel libre » qui est l’organisation américaine à but non lucratif fondée par Richard STALLMAN.

Chez [Interhop.org](https://interhop.org/) nous utilisons le terme logiciel libre et l'acronyme FLOSS pour Free/Libre/Open-source software (FLOSS). Le logiciel libre s'oppose aux logiciels privateurs.
[Interhop.org](https://interhop.org/) installe et met à disposition des logiciels libres pour la santé.

[^logiciel_libre]: [www.gnu.org : Qu'est-ce que le logiciel libre ?](https://www.gnu.org/philosophy/free-sw.fr.html)

[^4_lib]: [https://vive-gnulinux.fr : 4 libertés](https://vive-gnulinux.fr.cr/logiciel-libre/4-libertes/)

[^privateur]: [https://www.april.org/logiciel-privateur](https://www.april.org/logiciel-privateur)

[^stallman]: [https://fr.wikipedia.org/wiki/Richard_Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman)

[^opensource-libre]: [https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html)
