---
layout: post
title: "InterHop répond à France Inter et à Doctolib"
categories:
  - Conseil Etat
  - France Inter
  - Doctolib
  - Amazon
ref: reponse-franceinter-doctolib
lang: fr
---

Nous répondons ici à l'article France Inter[^franceinter] et à la société Doctolib qui s'exprime sur le site Medium[^medium].

<!-- more -->

Les points surlignés reprenant l'article de Medium vont être expliqués ci-dessous.

<img src="https://i.ibb.co/CBZS49F/Screenshot-2021-03-09-at-11-55-04.png" alt="Screenshot-2021-03-09-at-11-55-04" border="0">

# Données de santé

Nous rappelons que les rendez-vous médicaux sont des données de santé, ceci a été réaffirmé par la CNIL et le Conseil National de l'Ordre des Médecins dans un papier commun[^cnom_cnil].

Les rendez-vous réalisés sur la plateforme Doctolib n'échappent donc pas à cette définition juridique.

# Schéma d'ensemble

<img src="https://i.ibb.co/82jWm7D/Screenshot-2021-03-09-at-11-58-37.png" alt="Screenshot-2021-03-09-at-11-58-37" border="0">

<sub>Source : Note blanche de la société Tanker[^tanker]</sub>

La société Doctolib fournit son service de prise de rendez-vous médicaux via un site web.

Un site web[^1] a plusieurs composants, d'abord son *Interface Utilisateur-rice* aussi appelée *Application* dans le schéma. C'est ce que l'utilisateur.rice voit à l'écran lorsqu'il ou elle ouvre son navigateur (Chrome, Firefox, Safari...).

Ensuite un *Serveur Web* [^2]  (*App Server* dans le schéma en anglais) traite les données pour l'utilisateur.rice quand le site n'est pas ouvert sur son ordinateur.

Un *App Server* utilise dans la plupart des cas une *Base de Données* [^3] (ou *Storage*) pour traiter et stocker les données. 

Le schéma ci-dessus, réalisé par la société Tanker - prestataire de Doctolib - illustre la situation d'ensemble en matière d'accès et de chiffrement des données.

Nous le verrons plus bas, la société Doctolib délègue la gestion des clés "Maîtres" à un partenaire tiers (*3rd Party*).

Ce schéma nous permet de voir que le "maillon faible" est l'*App Server* puisqu'il accède aux données "en clair" (i.e de façon non chiffrée). En ce qui concerne Doctolib, ce dernier est hébergé chez Amazon.

**Doctolib ne chiffre donc pas tout le parcours de la donnée de santé de bout en bout** : ici la prise de rendez-vous entre les professionnel.le.s de santé et les patient.e.s.

# Ce que serait le chiffrement de bout en bout

Le chiffrement de bout en bout est le chiffrement des données personnelles "côté client" au sens informatique (i.e coté utilisateur de Doctolib). Ainsi seulement et uniquement les utilisateurs-rices peuvent accéder à leurs données. Les utilisateurs-rices (et eux.elles seul.e.s) peuvent choisir de partager ou non leurs données chiffrées avec d'autres.

Le schéma suivant montre le fonctionnement d'un chiffrement de bout en bout.

<a href="https://ibb.co/6RcLx76"><img src="https://i.ibb.co/jv9s2P7/Screenshot-2021-03-04-at-17-59-50.png" alt="Screenshot-2021-03-04-at-17-59-50" border="0"></a>

<sub>Source : Note blanche de la société Tanker[^tanker]</sub>


<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/interhop/collectes/cagnotte-de-soutien-recours-devant-le-conseil-d-etat/widget-compteur" style="width:100%;height:450px;border:none;"></iframe>

# Argumentation technique phrase par phrase

Interhop entend revenir phrase par phrase sur les déclarations de Doctolib au regard de l'analyse technique précédemment exposée.

### "Il est faux d’affirmer que Doctolib ne pratique pas le chiffrement."

En effet la société Doctolib pratique du chiffrement. Mais la donnée de santé n'est pas chiffrée sur tout son parcours.

Au sein de l'*App Server* les données sont à un moment accessibles en clair (*Data Access*). Or dans le cas de Doctolib, cet *App Server* est hébergé chez Amazon.

### "L’ensemble des données des utilisateurs de Doctolib est chiffré au repos."

En effet c'est le *Storage* du Schéma.

### "Les données en transit sont toujours chiffrées."

Les données en transit correspondent au petit cadenas entre le *App Server* et l'*Application*.
On parle aussi de « Sécurité de la couche de transport »[^wiki_tls] qui est un protocole de chiffrement des échanges par réseau informatique, notamment par Internet. Ce protocole correspond au "s" de https.

Avec ce protocole, les données sont chiffrées pendant qu'elles transitent sur le réseau.
Nous pourrions à ce stade en conclure que les données en transit sont parfaitement protégées.


Cependant, la société Doctolib a recours au service Cloudflare. 

<img src="https://i.ibb.co/R2bzP7F/Screenshot-2021-03-10-at-15-19-00.png" alt="Screenshot-2021-03-10-at-15-19-00" border="0">

Voici le schéma qui s'applique :

<img src="https://i.ibb.co/5MD9JLb/Screenshot-2021-03-10-at-10-10-31.png" alt="Screenshot-2021-03-10-at-10-10-31" border="0">

<sub>Source : Note blanche de la société Tanker[^tanker]</sub>


Il s'agit donc d'analyser si l'utilisation des services de Cloudflare modifie la sécurité des données lors de leur transit.

Il est hautement probable que si les données sont chiffrées en transit entre l'*App Server* et Cloudflare puis entre Cloudflare et l'*Application*, Cloudflare puisse déchiffrer les données de santé dans le cadre de son fonctionnement habituel.

Enfin, il faut rappeler que Cloudflare est une solution soumise au droit américain. Les problématiques liées aux effets extraterritoriaux du droit américain qui s'appliqueraient à Amazon s'appliqueraient également à Cloudflare.

### "Le chiffrement de bout en bout est en place pour la protection des documents médicaux."

Pour les documents la solution Tanker est utilisée.

InterHop est d'accord que cette solution pourrait réaliser du chiffrement de bout en bout dans certaines conditions.

Cependant InterHop s'interroge sur l'intérêt de mettre en place cette couche supplémentaire  si comme l'indique la société Doctolib "Ces mesures rendent impossibles l'accès aux données par AWS" ? Est-ce à avouer que l'existant ne serait pas suffisant ?

Dans ce cas elle se pose une autre question :
```Pourquoi ne pas avoir mis en place la solution Tanker sur toutes les données stockées chez Doctolib et Amazon Web Service ?```

En effet les documents (arrêt de travail, ordonnance...) comme les rendez-vous sont des données de santé. 


### "Les différentes méthodes de chiffrement mises en place par Doctolib représentent l’état de l’art des recommandations des agences gouvernementales sur la cybersécurité."

Les pratiques adoptées par la société Doctolib sont probablement en accord avec les préconisations des agences gouvernementales comme l'ANSSI. 

Mais les préconisations de l'ANSSI sont fondées au regard d'un risque externe : est-ce qu'un assaillant externe peut avoir accès aux données ?

Avec le FISA et l'ingérence américaine le problème est autre. Il s'agit de se protéger d'une demande d'accès aux données par les services de renseignement américain. Cette demande peut être réalisée directement auprès du serveur Amazon Web Service. Le risque est ici interne. C'est d'ailleurs pour cela que la Cour de Justice de l'Union Européenne a invalidé le "bouclier de protection des données" ou Privacy Shield[^curia].



### "L’ensemble des données des utilisateurs de Doctolib est chiffré."

Les données au repos (*Storage*) et en transit (*https*) sont bien chiffrées.

Encore une fois, le problème ne vient pas du chiffrement en transit mais de l'*App Server* où les données sont accessibles en clair alors qu'elles sont hébergées chez Amazon.

#### "Les clefs de chiffrement sont hébergées en France chez ATOS, une société française."

Doctolib utilise la méthode BOYK ou "*Bring Your Own Key*". Avec cette méthode le fournisseur d'application comme la société Doctolib délégue la gestion des clés à un partenaire tiers (*3rd Party* dans le schéma).
Dans notre exemple, c'est l'entreprise Atos qui fournit ce service à la société Doctolib hébergée chez Amazon.

<img src="https://i.ibb.co/jg5XRCx/Screenshot-2021-03-09-at-11-41-06.png" alt="Screenshot-2021-03-09-at-11-41-06" border="0">
<img src="https://i.ibb.co/mTzVvMg/Screenshot-2021-03-09-at-13-19-40.png" alt="Screenshot-2021-03-09-at-13-19-40" border="0">

<sub>Source : Politique de protection des Données à caractère personnel - Professionnels de santé </sub>

Cependant la problématique reste entière et est la suivante.
**Au regard du FISA, la criticité en termes de sécurité reste majeure.** Toute société soumise au droit américain doit répondre aux demandes émanents de ses autorités, et peut être contraint de donner accès aux données herbergées sur l’App Server.

La société Tanker énonce dans sa note blanche[^tanker] :
> Ces dernières années, nous avons assisté à la montée en puissance du programme "Bring Your Own Key" (BYOK), comme l'a annoncé le gestionnaire de clés externes (EKM) de Google Cloud lors du salon "Next London 2019". Le principe de ces systèmes est d'externaliser la gestion des clés de cryptage, en utilisant soit une gestion des clés sur site (avec des boîtes noires transactionnelles BNT ou encore HSM), soit une gestion des clés tierce partie basée sur le cloud. Avec ces systèmes, les fournisseurs de cloud ne sont plus responsables de la gestion des clés de cryptage utilisées pour le cryptage au repos.
> 
> Les administrateurs de serveurs ont également accès à la solution de gestion des clés choisies et, dans la plupart des cas, ils peuvent accéder directement aux clés ou disposer d'un passe-partout. En outre, le chiffrement est toujours effectué sur les serveurs d'applications. Cette évolution ne change donc rien au problème et c'est principalement un moyen pour les fournisseurs de services dans les nuages d'avoir la conscience tranquille !


### "Ces mesures rendent impossible l’accès aux données par AWS."

InterHop ainsi que 12 autres requérants ont déposé un recours en urgence au Conseil d'État parce que :
- les lois américaines (FISA et Executive Order 12 333) imposent aux clouders américains (GAFAM) de fournir dans le secret des accès distants
- or dans l'*App Server* ces données sont en clair ce qui les rend vulnérables car techniquement accessibles. En effet l'*App Server* y accède avant de les envoyer au navigateur (ce qui peut être constaté par les initiés par la lecture des fichiers JSON).

# Aidez-nous 

Médecins, patient.e.s les requérant.e.s sont très inquiet.ète.s.

Nous savons que ni les hôpitaux[^hopitaux_faille], ni les sociétés expertes dans la santé[^dedalus_faille], ni même Microsoft[^microsoft_faille1][^microsoft_faille2] ne sont à l'abri de faille de sécurité.

Vous êtes expert.e en sécurité informatique ? 
Aidez nous ! Participer au débat ! C'est grâce à celui-ci que les données de santé seront mieux protégées.

Vous pouvez aussi donner pour financer notre combat.

<iframe id="haWidget" allowtransparency="true" scrolling="auto" src="https://www.helloasso.com/associations/interhop/collectes/cagnotte-de-soutien-recours-devant-le-conseil-d-etat/widget" style="width:100%;height:750px;border:none;" ></iframe>

Dans tous les cas votre aide participera à apprendre à mieux sécuriser - juridiquement et techniquement - ces données si sensibles que sont les données de santé. 

[^1]: [https://fr.wikipedia.org/wiki/Site_web](https://fr.wikipedia.org/wiki/Site_web)
[^2]: [https://fr.wikipedia.org/wiki/Serveur_web](https://fr.wikipedia.org/wiki/Serveur_web)
[^3]: [https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es)
[^hopitaux_faille]: [Cyberattaques : les hôpitaux, une cible de choix pour «des hackers sans éthique»](https://www.leparisien.fr/high-tech/cyberattaques-les-hopitaux-une-cible-de-choix-pour-des-hackers-sans-ethique-17-02-2021-QXXGT2HZGBAE5HUJ6I5YKWGXGE.php)
[^dedalus_faille]: [Les informations confidentielles de 500 000 patients français dérobées à des laboratoires et diffusées en ligne](https://www.liberation.fr/checknews/les-informations-confidentielles-de-500-000-patients-francais-derobees-a-des-laboratoires-medicaux-et-diffusees-en-ligne-20210223_VO6W6J6IUVATZD4VOVNDLTDZBU/)
[^microsoft_faille1]: [Les hackers de SolarWinds ont accédé au code source de Microsoft Azure, Intune et Exchange ](https://www.usine-digitale.fr/article/les-hackers-de-solarwinds-ont-accede-au-code-source-de-microsoft-azure-intune-et-exchange.N1062819)
[^microsoft_faille2]: [L’Autorité bancaire européenne victime d’une cyberattaque permise par les failles de Microsoft Exchange](https://www.lemonde.fr/pixels/article/2021/03/08/l-autorite-bancaire-europeenne-victime-d-une-cyberattaque-permise-par-les-failles-de-microsoft-exchange_6072361_4408996.html)


[^tanker]: [https://tanker.io/tanker-whitepaper.pdf](https://tanker.io/tanker-whitepaper.pdf)

[^curia]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^medium]: [Réponse à plusieurs fausses affirmations relayées sur France Inter : les données des utilisateurs de Doctolib sont bien chiffrées](https://stanniox.medium.com/r%C3%A9ponse-%C3%A0-plusieurs-fausses-affirmations-relay%C3%A9es-sur-france-inter-les-donn%C3%A9es-des-utilisateurs-b6bf6250d229)

[^franceinter]: [Doctolib : le chiffrement des données incomplet ?](https://www.franceinter.fr/justice/doctolib-le-chiffrement-des-donnees-incomplet)

[^cnom_cnil]: [GUIDE PRATIQUE SUR LA PROTECTIONDES DONNÉES PERSONNELLES](https://www.cnil.fr/sites/default/files/atoms/files/guide-cnom-cnil.pdf)


[^wiki_tls]: [https://fr.wikipedia.org/wiki/Transport_Layer_Security#Pr%C3%A9sentation](https://fr.wikipedia.org/wiki/Transport_Layer_Security#Pr%C3%A9sentation)
