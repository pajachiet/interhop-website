---
layout: post
title: "Les codes sources entre-ouverts ?"
categories:
  - Code Source
  - Open Source
ref: codes-sources-entre-ouverts
lang: fr
---

Le 4 janvier dernier Stefane Fermigier, co-président du Conseil National du Logiciel Libre (CNLL) demandait la transmission de “l’intégralité des codes sources de la plateforme Health Data Hub.”

<!-- more -->


Rappelons que le Health Data Hub est hébergé chez Microsoft et vise à centraliser l'ensemble des données de santé française.

Rappelons que les codes sources sont des documents administratifs. Depuis la loi pour une République Numérique[^rep_num] le code source[^code_source_rep_num] des logiciels produits par les administrations est soumis au principe d'ouverture par défaut et de gratuité.

### Financement public = Code public ! 

La medecine est une profession open source par essence.
Le partage libre des connaissances informatiques permet d'améliorer la santé.
Le code source des algorithmes de soin ainsi que la plateforme permettant de les lancer devraient (doivent) être libres.

Le 17 avril la Commission d'Accès aux Documents Administratifs[^cada_code_source] a finalement rejeté cette demande d'accès. Le motif invoqué :  l'exception "sécurité des systèmes d'information des administrations".

InterHop s'oppose fermement à cette argumentation. 
La sécurité par l'obscurité n'est plus un argument tenable. L'Agence Nationale de Sécurité des Systèmes d'Informatique, l'ANSSI, rappelle même par la voix de son  directeur général, Monsieur Guillaume Poupard, que "c’est le phénomène de faux sentiment de sécurité qui est le plus néfaste pour les acteurs qui n’ouvrent pas leur code source, de sécurité par l’obscurité, qui se privent en réalité d’une connaissance de leurs propres vulnérabilités"[^rapport_bothorel]. 

Dans un rapport récent le député Éric Bothorel rappelle aussi qu' : "en réalité, les acteurs faisant valoir la sécurité des systèmes d’information semblent méconnaître la possibilité de renforcer leur résilience offerte par la démarche d’ouverture des codes sources"[^rapport_bothorel].

En effet c'est l'open source et l'intelligence collective qui augmente la sécurité les projets informatiques. 
Ainsi l’ANSSI a élaboré un système d’exploitation multiniveau sécurisé dénommé CLIP OS, pour répondre aux besoins de l’administration. Basé sur un noyau Linux et capable de gérer des informations de plusieurs niveaux de sensibilité, CLIP OS est à présent disponible en open source dans le cadre d’un projet de développement collaboratif"[^clip_os].
Linux, un système d'exploitation libre, est installé sur près de 90% des serveurs du monde entier[^linux_serveur].

En fait "la sécurité ne réside pas dans le fait de cacher la porte d’entrée, mais dans la robustesse de la clé"[^hollandais_volant].

### Le fond, la forme. Tout un art

Le rapport Bothorel avait également pointé la "mauvaise foi" des administrations. "Certains acteurs publics prennent prétexte de dispositions de sécurité, qu’ils interprètent dans leur seul intérêt, pour ne pas ouvrir". La mission proposait "d’associer  plus  étroitement  l’ANSSI  à  la  politique  d’ouverture,  pour  apporter l’expertise dans le domaine de la sécurité des systèmes d’information". 
InterHop adhère à ces propos. L'ANSSI a-t-elle été consultée avant de bloquer l'ouverture des codes sources du Health Data Hub ? A notre connaissance la réponse est négative.

Alors que l'administration s'oppose à publier ses codes sources, le 27 avril dernier une circulaire[^circulaire] est publiée par le Premier Ministre ! **Celle-ci promet que "l'année 2021 doit poser les fondements d'une politique  ambitieuse de la donnée, des algorithmes et des codes sources dans chacun de vos ministères".**

Même si cette circulaire rappelle des idées anciennes[^april] et toujours sans effet, l'association InterHop souligne l'initiative. Elle se pose cependant une question : pourquoi ne pas aligner les paroles avec les actes ?

[^hollandais_volant]: [https://lehollandaisvolant.net/?mode=links&id=20210428175456](https://lehollandaisvolant.net/?mode=links&id=20210428175456)

[^linux_serveur]: [L’écrasante domination de Linux dans le Cloud : vers 90% de part de marché](https://www.journaldunet.com/solutions/cloud-computing/1102096-l-ecrasante-domination-de-linux-dans-le-cloud-vers-90-de-part-de-marche/)

[^rep_num]: [Promulgation de la loi « pour une République numérique » - logiciels libres - mobilisation de l'April](https://www.april.org/promulgation-de-la-loi-pour-une-republique-numerique-logiciels-libres-mobilisation-de-l-april)

[^code_source_rep_num]: [https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000033202948](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000033202948)

[^cada_code_source]: [https://www.acteurspublics.fr/upload/media/default/0001/34/de3f9a4a878fa999d76a7425c7761b725a183478.pdf](https://www.acteurspublics.fr/upload/media/default/0001/34/de3f9a4a878fa999d76a7425c7761b725a183478.pdf)

[^rapport_bothorel]: [Pour une politique publique de la donnée](https://www.gouvernement.fr/sites/default/files/contenu/piece-jointe/2020/12/rapport_-_pour_une_politique_publique_de_la_donnee_-_23.12.2020__0.pdf)

[^clip_os]: [https://www.ssi.gouv.fr/administration/services-securises/clip/](https://www.ssi.gouv.fr/administration/services-securises/clip/)


[^circulaire]: [https://circulaire.legifrance.gouv.fr/circulaire/id/45162](https://circulaire.legifrance.gouv.fr/circulaire/id/45162)

[^april]: [Circulaire données et codes sources : un premier pas dans la bonne direction qui doit être confirmé](https://www.april.org/circulaire-donnees-et-codes-sources-un-premier-pas-dans-la-bonne-direction-qui-doit-etre-confirme)
