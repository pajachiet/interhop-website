---
layout: post
title: "Communiqué de presse : audition d’InterHop devant l’Assemblée Nationale le 18/02/2021"
categories:
  - Souveraineté Numérique
  - Assemblée Nationale
  - Latombe
ref: commission-souverainete
lang: fr
---

InterHop a eu le plaisir d'être entendue dans le cadre de la mission d'information sur le thème : "[Bâtir et promouvoir une souveraineté numérique nationale et européenne](https://www2.assemblee-nationale.fr/instances/fiche/OMC_PO773655){:target="_blank"}

<!-- more -->

## Contexte de la journée

Cette mission est présidée par M. Jean-Luc Warsmann, M. Philippe Latombe en est le rapporteur.

InterHop a été entendue le 18 février 2021 dans le cadre d'une journée centrée autour des enjeux de la santé.
La journée a commencé avec [un entretien avec Madame Stéphanie Combes](http://videos.assemblee-nationale.fr/video.10378648_602e234d85c35.batir-et-promouvoir-une-souverainete-numerique-nationale-et-europeenne--mme-stephanie-combes-direc-18-fevrier-2021){:target="_blank"}, directrice du Health Data Hub (plateforme de centralisation des données de santé hébergée chez Microsoft).

S'en est suivi un [entretien entre Monsieur Marc Cuggia et Madame Laurence Jay-Passot](http://videos.assemblee-nationale.fr/video.10378648_602e234d85c35.batir-et-promouvoir-une-souverainete-numerique-nationale-et-europeenne--mme-stephanie-combes-direc-18-fevrier-2021){:target="_blank"} concernant le réseau du grand Ouest et la plateforme Ouest Data Hub.

InterHop a cloturé la session. [La vidéo est en ligne](http://videos.assemblee-nationale.fr/video.10384612_602e62bcebf7b.batir-et-promouvoir-une-souverainete-numerique-nationale-et-europeenne---m-adrien-parrot-medecin--18-fevrier-2021){:target="_blank"} sur le site de l'Assemblée Nationale.

A noter qu'une journée d'échange avec la Sécurité Sociale (CNAM) et les Hôpitaux de Paris est à prévoir.

Le point principal de l'intervention de l'association InterHop est la problématique de l'extraterritorialité du droit américain. Les GAFAM[^GAFAM] sont tous concernés. InterHop souligne que le problème n'est pas tant le CLOUDAct (qui est une procédure judiciaire balisée) que le FISA et l'Executive Order 12 333.  C'est à cause de ces lois que le Privacy Shield ou "bouclier de protection des données" a été annulé par la Cour de Justice de l'Union Européenne. 
> "Les limitations de la protection des données à caractère personnel qui découlent de la réglementation interne des États-Unis portant sur l’accès et l’utilisation, par les autorités publiques américaines, de telles données transférées depuis l’Union [Européenne] vers ce pays tiers ne sont pas encadrées d’une manière à répondre à des exigences équivalentes à celles requises, en droit de l’Union"[^curia]

InterHop rappelle le rôle central des législateurs français et européens : tant que le droit américain n'évolue pas, tout nouveau texte voulant remplacer le Privacy Shield est inutile et dangereux pour les européen.ne.s ; en effet celui-ci serait faussement sécurisant.

[^curia]: [La Cour invalide la décision 2016/1250 relative à l'adéquation de la protection assurée par le bouclier de protection des données UE-États-Unis](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

## Compte-rendu détaillé

InterHop rappelle qu'elle n'est pas opposée à la recherche en santé.
InterHop rappelle qu'elle souhaite favoriser le partage et l'interopérabilité des systèmes d'information.

Les problématiques liées aux GAFAM[^GAFAM] et au Health Data Hub se jouent donc uniquement sur le terrain de la protection des libertés fondamentales en particulier à cause de : 
- l'hyper centralisation de l'ensemble des données de santé
- l'hébergement chez le géant américain Microsoft Azure

InterHop rappelle que de nombreuses alternatives existent : 
- Les Entrepôts de Données de Santé de : Lille, Rennes, Nantes, Paris, Bordeaux, Marseille, Grenoble, Lyon, Toulouse... Les Hôpitaux de Paris traitent par exemple plusieurs millions de patients grâce aux logiciels libres
- Le Centre d'Accès Sécurisé aux Données CASD[^casd] est également une alternative importante puisqu'il fabrique un boitier chiffrant qui permet de s'enregistrer avec empreinte digitale. Il est certifié "Hébergeur de Données de Santé" et homologué au référentiel de sécurité du Système National des Données de Santé SNDS.

[^casd]: casd.eu
[^GAFAM]: Google, Amazon, Facebook, Apple, Microsoft

[^assureurs]: [«Health Data Hub»: le méga fichier qui veut rentabiliser nos données de santé](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante?onglet=full)

InterHop rappelle qu'elle n'est pas la seule à critiquer le Health Data Hub et l'extraterritorialité du droit américain : elle rappelle le rôle de SanteNathon[^santenathon], la CNIL[^memoire_cnil], la CNAM[^cnam_hdh], la Cour de Justice de l'Union Européenne ainsi que du directeur de l'APHP Martin Hirsch[^assureurs] qui, dans une note adressée à Monsieur Auber (lorsqu'il était encore directeur du Health Data Hub), a  pointé le "risque de perte de confiance" des patient.e.s.

[^santenathon]: santenathon.org

[^cnam_hdh]: ['Conditions pas réunies' pour que Microsoft gère les données de santé, selon l'Assurance maladie](https://interhop.org/2021/02/19/depeche-hdh-cnam)

InterHop revient sur le contentieux contre le Health Data Hub HDH auquel elle a participé devant le Conseil d'Etat[^conseil_etat] en octobre 2020.
Le Health Data Hub est déployé depuis 2019 et hébergé chez Microsoft, sans appel d'offre. Normalement, il faudrait attendre un décret pour régir le traitement des données du SNDS. Or, la Covid19 a accéléré la mise en place du HDH avec l'utilisation d'un cadre dérogatoire lié à l'état d'urgence sanitaire pour forcer le droit commun. L'ensemble des données traitées doivent normalement être supprimées à l'issue de l'état d'urgence sanitaire.

[^conseil_etat]: [Saisi en référé par le collectif SantéNathon, le Conseil d’Etat reconnait que le gouvernement américain peut accéder sans contrôle aux données de santé des Français hébergées par le Health Data Hub chez Microsoft](https://interhop.org/2020/10/14/communique-presse-conseil-detat-reconnait-acces-donnes)

Le 16 juillet 2020[^curia], la Cour de Justice de l'Union Européenne CJUE dans son avis "Schrems II" révèle que le droit américain n'assure pas un niveau de protection équivalent au RGPD et annule le Privacy Shield.
La CJUE réalise donc une appréciation concrète des pratiques des services de renseignement sur les fondements:
- de la section 702 du FISA avec l'obtention immédiate et rapide par gouvernement à des informations très larges, sans notification ni garantie
- de l'Executive Order 12 333 : c'est un décret présidentiel qui met en avant les techniques d'interception à des fins de renseignement sur les signaux en transit et en dehors des USA (cables sous-marins par exemple).

La CJUE révèle qu'il existe une collecte large, soumise uniquement à la discrétion du gouvernement, sans ciblage, sans autorité indépendante, sans respect des droits opposables pour les citoyen.ne.s.

InterHop précise que Microsoft ne peut pas s'opposer à ces demandes ; cette société est soumise au droit américain et à ses lois.
        
InterHop rappelle que l'ordonnance du Conseil d'Etat du 13 octobre 2020 reconnait :
1. Les risques liés au FISA et Executive Order 12 333 qui constituent une violation du droit à la protection des données personnelles et cela même en l'absence de transfert des données. Ceci est notamment pointé dans un avis particulièrement clair de la CNIL[^memoire_cnil]
2. Des enjeux de sécurité sur ensemble des données de santé centralisées (plus de 67 millions de personnes) avec des clés de chiffrement détenues par Microsoft qui déchiffre les données dans le cadre du fonctionnement normal de la plateforme pour les fournir aux datascientists.

[^memoire_cnil]: https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf

InterHop rappelle que même si les données sont pseudonymisées, il est toujours possible de réindentifier un individu.

InterHop définit l'autonomie numérique comme la Capacité d’autodétermination dans l’environnement Cyber[^coup_data]. Elle peut s’entendre à plusieurs niveaux : des individus, des institutions (sécurité sociale, régions, départements, villes, hôpitaux …), des Nations, de l’Europe, des États-Plateformes.
Le logiciel libre est garant de l'autonomie des utilisateurs qui l'exécute directement sur leur poste de travail. Depuis les années 2010, nous voyions qu'avec le cloud cette autonomie s'amoindrit.

[^coup_data]: [Coup Data : 3 Questions Sur… La souveraineté numérique](https://interhop.org/2020/11/12/coup-data)

Concernant les infrastuctures bigdata InterHop rappelle cette phrase du rapport Bothorel
> "Les  infrastructures  nécessaires  à  la  donnée  sont  de  plus  en  plus  exposées  à  des  formes  de dépendances logicielles, ce qui soulève un enjeu d’autonomie stratégique[^bothorel]." 

[^bothorel]: [ Rapport Bothorel : pour une nouvelle ère de la politique publique de la donnée ](https://www.vie-publique.fr/en-bref/277963-rapport-bothorel-pour-une-nouvelle-ere-de-la-politique-de-la-donnee)

Concernant les enjeux des données de santé InterHop rappelle
- que les données de santé sont des données sensibles
- le rôle central du secret médical dans la relation médecin-patient depuis  le serment d'Hippocrate. Sans confiance il n'y a  pas de confidences.
- la place centrale des données dans la recherche qui permettent d'entrainer les algorithmes d'intelligence articielle.

Nous sommes donc dans un rapport bénéfice-risque où nous voulons : 
- l'amelioration de la recherche
- sans aucune perte de confiance des acteurs et des patient.e.s
 
InterHop rappelle que les données de santé représentent un intérêt financier majeur. Par exemple 30 % des investissements d'Alphabet[^alphabet], la maison mère de Google sont dirigés vers la santé. Le modèle de financement de la Sécurité Sociale est lié à la mutualisation des risques. Les géants du numérique représentent un risque de délitement de la Sécurité Sociale.  Les GAFAM ciblent très spécifiquement les individus et pourraient donc vouloir assurer les personnes avec des tarifs différenciés, en fonction pratiques prédictives.

[^alphabet]: [How the “Big 4” Tech Companies Are Leading Healthcare Innovation](https://healthcareweekly.com/how-the-big-4-tech-companies-are-leading-healthcare-innovation/)

**InterHop propose des solutions concrètes.**

Concernant la gouvernance InterHop pense que l'erreur originelle du Health Data Hub a été de fondre la gouvernance et la technique dans une seule structure juridique. 

La gouvernance fixe le cadre. Elle gère les accès, fixe le cahier des charges, gère les consentements et l'application du RGPD.

Concernant la réversibilité de la plateforme Health Data Hub, InterHop pense qu'il faut créer au plus vite un comité de pilotage multipartite avec les utilisateurs, les pouvoirs publics et les acteurs du numérique.

D'un point de vue technique nous pensons qu'il peut être une bonne idée que la puissance publique favorise le développement d'une plateforme libre bigdata centralisée pour la santé. Ces développements pourraient être propulsés par l'Union Européenne. InterHop développe par exemple Libre Data Hub[^git_ldh].

Nous pronons la décentralisation des données et la fédération des acteurs en santé numérique.
La décentralisation permet de restreindre l'utilisation des données pour certaines finalités d'étude. La fédération avec l'interopérabilité des systèmes d'information permet la portabilité des données. Ces concepts répondent aux principes du  RGPD.

InterHop participe au réseau InterCHU[^crh_interchu] qui fédère des ingénieurs des hôpitaux français autour des standards d'interopérabilité.

[^git_ldh]: https://framagit.org/interhop/libre-data-hub/platform-deployment

[^crh_interchu]: [Journées InterCHU : compte-rendu du 21 janvier](https://interhop.org/2021/01/25/reunion-interchu-21-janvier)

InterHop défend une relation de proximité avec les lieux de recueil des données de santé : hôpitaux, cliniques, cabinet médicaux... 
Cette décentralisation favorise ces liens locaux qui permettent d'améliorer la qualité de la recherche. Cette qualité se co-construit avec des équipes pluridisciplinaires dans le cadre  d'interactions permanentes entre soin et recherche : enseignant.e.s, de chercheur.euse.s, d’informaticien.ne.s, de soignant.e.s ainsi qu’un réseau de personnes en confiance avec l’ensemble du système. Elle est dans l'intérêt des patient.e.s. 

D'un point de vue juridique, InterHop pense que les logiciels et les algorithmes en santé doivent être opensources.
La recherche médicale est en partie financée par l'État, donc par les contribuables. Lorsque cette recherche médicale publique aboutit au développement de logiciels ou d'algorithmes, il est logique que le code source soit ouvert, afin que l'investissement soit le plus avantageux possible pour les contribuables, éventuels patients (Loi pour une République Numérique). 

InterHop milite donc pour la protection et le secret des données mais la transparence des logiciels.

InterHop pense qu'il faut renforcer la certification "Hébergeur de données de Santé" en interdisant le traitement des données sensibles (au sens du RPGD) par des acteurs extraterritoriaux. Un label ne suffit pas car sa valeur juridique est insuffisamment contraignante.

Les pouvoirs financiers et humains de la CNIL doivent être renforcés. L'avis conforme de la CNIL doit être rétabli notamment pour les données sensibles. Celui-ci permettra une valeur contraignante des avis de la CNIL, qui s'inscrirait pleinement dans les préconisations du Comité Européen de la Protection des Données.

Enfin InterHop pense que c'est au niveau européen que les enjeux sont les plus importants. Un nouveau Privacy Shield est impossible en l'état du droit américain. La France doit être ferme sur ce point et mobiliser ses partenaires européens sur ces questions.
