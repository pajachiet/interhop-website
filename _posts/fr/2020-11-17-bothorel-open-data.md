---
layout: post
title: "Mission politique publique de la donnée"
categories:
  - Data
  - Autonomie
  - Audition
redirect_from:
  - /bothorel
ref: bothorel-open-data
lang: fr
show_comments: true
---

InterHop a eu le plaisir d'être entendue dans le cadre de la [Mission Politique Publique de la Donnée](https://www.mission-open-data.fr/).

<!-- more -->

# Mission Politique Publique de la Donnée

La mission est confiée par le Premier Ministre au député Eric Bothorel associant Stéphanie Combes, directrice du Health Data Hub et Renaud Vedel, coordinateur pour la stratégie en Intelligence.

La mission associe deux axes:
- ouverture des données et des codes sources publics
- données d’intérêt général

Voici la [lettre de mission](https://www.mission-open-data.fr/uploads/decidim/attachment/file/1/Lettre_Mission_BOTHOREL.pdf) du Premier Ministre.
Et voici le [rapport d'étape](https://www.mission-open-data.fr/uploads/decidim/attachment/file/2/Politique_publique_donn%C3%A9e_Rapport_%C3%A9tape_VF.pdf).

## Proposition InterHop

Voici les trois propositions InterHop.org :
- Développement d'une plateforme d'analyse de facon mutualisée et avec une gouvernance francaise voire Européenne permettant de ne pas migrer les données en dehors des lieux de recueil (décentralisation)
- [Ouverture des terminologies](https://www.mission-open-data.fr/processes/politique-publique-donnee/f/2/proposals/139) (interopérabilité semantique)
> Les terminologies ou les ontologies utilisées par les administrations doivent être strictement opensources. En effet celles-ci sont capitales pour permettre l’interopérabilité sémantique des systèmes d’information.
Ces terminologies doivent être distribuées dans des formats ouverts (csv, …). Elles doivent être distribuées et maintenues par des organismes à but non lucratifs.
- [Compagnons de la données](https://www.mission-open-data.fr/processes/politique-publique-donnee/f/2/proposals/140)
> Les administrations publiques collectent de nombreuses informations qui sont sous utilisées faute de cellules compétentes pour récolter, transformer, standardiser, anonymiser et publier les données. Pour être menés efficacement, cette démarche doit être réalisée de manière locale en impliquant les administrations locales. Celles-ci pourraient être accompagnées par des “compagnons de la donnée” (fonctionnaires, PME, associations, autoentrepreneurs certifiés) pour mener ces travaux.

# Notre présentation

Notre présentation est visible en [plein écran](https://pad.interhop.org/p/BytHSrl9D#/).

InterHop affirme les valeurs du Logiciel Libre FLOSS, la nécessité du consentement des personnes dans le cadre du recueil des données (y compris celles'intérêt générale) et la décentralisation permettant la sécurité des données et une recherche de qualité.

La voici ici dans le site directement :-)
 
 <div class="responsive-video-container">
<iframe width="100%" height="500" src="https://pad.interhop.org/p/BytHSrl9D#/" frameborder="0"></iframe>
</div>

