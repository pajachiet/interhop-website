---
layout: post
title: "Journées InterCHU : compte-rendu du 21 janvier"
categories:
  - OMOP
  - France
  - InterCHU
ref: journee-interchu-21-janvier 
lang: fr
---

La journée InterCHU OMOP - France du 21 janvier a réuni une soixantaine de participants issus d'hôpitaux, d'institutions publiques et d'entreprises privées.

<!-- more -->

Les journées InterCHU s'adressent préférentiellement aux personnes francophones (ingénieur.e.s du secteur public, ...) utilisant le modèle OMOP ou voulant l'utiliser dans les prochains mois.

Vous pouvez retrouvez [ici](https://interhop.org/2021/01/08/reunion-interchu) les principes d'échanges et d'entraides que nous voulons promouvoir dans ces journées.

Cette journée était l'occasion de reprendre les échanges sur l'interopérabilité en santé. Les participants ont présenté l'avancée de leurs travaux sur les thématiques suivantes :
- ETL et création d'entrepôts de données de santé : Easter-Eggs, Marseille, Bordeaux, Toulouse, Lille
- développment d'outils : visualisation du parcours patient, génération de jeux de données, extraction des charactéristiques, calcul décentralisé, outil de mapping sémantique

Vous pouvez retrouver les présentations ainsi que le programme complet à ce lien  : [https://framagit.org/interhop/omop/journees_omop_france_interchu](https://framagit.org/interhop/omop/journees_omop_france_interchu){:target="_blank"}

## Actions à venir

Nous prévoyons de faire une prochaine réunion thématique sur l'alignement sémantique fin février. Nous proposerons très prochainement une date aux personnes intéressées. 

La prochaine réunion InterCHU aura lieu en avril.

Un état des lieux des logiciels utilisés dans les CHUs pour un partage des ETLs est aussi en cours.

## En ligne

Nous utilisons une alternative aux géants du numérique.

Nous avons utilisé la solution de vidéo conférence BigBlueButton, opensource et protectrice des données personnelles car hébergée chez Octopuce, société de droit français (avec des serveurs en France, [https://www.octopuce.fr/mentions-legales/](https://www.octopuce.fr/mentions-legales/){:target="_blank"}). 

Merci à eux.
