---
layout: post
title: "Le juge sensible à notre action pour la défense des libertés fondamentales et l'application du RGPD"
categories:
  - Conseil Etat
  - Ministère
  - Doctolib
  - AWS
ref: conseil-etat-vaccination-audience
lang: fr
---

Le collectif d'associations de patients et de défense des libertés fondamentales associés à des syndicats de médecins, ayant contesté le choix du Ministère de la santé d'avoir recours à un partenariat avec Doctolib **est reçu devant le Conseil d'Etat le 8 mars prochain à 10h.**

<!-- more -->

Voici les 13 requérants:
- InterHop
- Syndicat National Jeunes Médecins Généralistes (SNJMG)
- Syndicat de la Médecine Générale (SMG)
- Union française pour une médecine libre (UFML)
- Fédération des Médecins de France (FMF)
- Didier Sicard
- Association Constances
- Marie Citrini, personne qualifiée au Conseil de l'APHP, représentante des usagers
- Les Actupiennes
- Actions Traitements
- Act-Up Sud Ouest
- Fédération SUD Santé Sociaux
- La Ligue des Droits de l'Homme

**Si vous voulez soutenir notre action pour la défense de vos droits.
Si vous voulez aider à financer les ressources humaines que nous mobilisons.**

<iframe id="haWidget" allowtransparency="true" scrolling="auto" src="https://www.helloasso.com/associations/interhop/collectes/cagnotte-de-soutien-recours-devant-le-conseil-d-etat/widget/" style="width:100%;height:750px;border:none;"></iframe>

Les requérant.e.s font valoir que le choix d’avoir recours au prestataire Doctolib pour organiser la gestion de la prise de rendez-vous dans le cadre de la politique vaccinale contre la Covid19 pourrait ne pas être conforme au RGPD. En effet, la société Doctolib a choisi de faire appel au géant américain Amazon Web Services pour héberger les données de santé. 

Il s’agit dès lors de demander au juge qu’il soit mis fin à l’utilisation de cette plateforme de prise de rendez-vous en ligne dans le cadre de la politique de vaccination car elle constituerait une atteinte grave au droit au respect de la vie privée. 


Cette action est relayée dans la presse : 
- Médiapart : ["Vaccination : le partenariat avec Doctolib contesté devant le Conseil d’Etat"](https://www.mediapart.fr/journal/france/260221/vaccination-le-partenariat-avec-doctolib-conteste-devant-le-conseil-d-etat){:target="_blank"}
- Euraktiv : [France under fire for use of Amazon-hosted Doctolib for jab bookings](https://www.euractiv.com/section/digital/news/france-under-fire-for-use-of-amazon-hosted-doctolib-for-jab-bookings/){:target="_blank"}
- Maddyness : [Le partenariat entre Doctolib et l’État contesté par des professionnels de la santé](https://www.maddyness.com/2021/03/01/vaccination-professionnels-sante-mettent-en-cause-partenariat-doctolib-etat/){:target="_blank"}
- Usine Digital : [Covid-19 : Le partenariat pour la vaccination entre l'Etat et Doctolib attaqué en justice](https://www.usine-digitale.fr/article/covid-19-le-partenariat-pour-la-vaccination-entre-l-etat-et-doctolib-attaque-en-justice.N1066499){:target="_blank"}
- Numerama : [Pourquoi la présence de Doctolib dans la campagne de vaccination fait encore polémique](https://www.numerama.com/tech/692317-pourquoi-la-presence-de-doctolib-dans-la-campagne-de-vaccination-fait-encore-polemique.html){:target="_blank"}
- Hospimedia : [Un collectif conteste au Conseil d'État le choix par l'État de Doctolib qui se défend](https://www.hospimedia.fr/actualite/articles/20210302-judiciaire-un-collectif-conteste-au-conseil-d-etat){:target="_blank"}
- France Soir : [Vaccinations : La France devant le Conseil d’Etat pour son partenariat avec Doctolib](https://www.francesoir.fr/societe-sante/vaccinations-la-france-devant-le-conseil-detat-pour-son-partenariat-avec-doctolib){:target="_blank"}
- Clubic : [Doctolib et rendez-vous vaccinal : le ministère de la Santé face au Conseil d’État pour justifier sa stratégie](https://www.clubic.com/amazon-web-services-aws/actualite-363700-doctolib-et-rendez-vous-vaccinal-le-ministere-de-la-sante-face-au-conseil-d-etat-pour-justifier-sa-strategie.html){:target="_blank"}
- Genethique : [Vaccin contre le Covid-19 : le partenariat avec Doctolib devant le Conseil d’Etat](https://www.genethique.org/vaccin-contre-le-covid-19-le-partenariat-avec-doctolib-devant-le-conseil-detat/){:target="_blank"}
- France 24 : [Vaccination : les données de santé de Doctolib suscitent l'inquiétude de médecins](https://www.france24.com/fr/france/20210305-vaccination-les-donn%C3%A9es-de-sant%C3%A9-de-doctolib-suscitent-l-inqui%C3%A9tude-de-m%C3%A9decins){:target="_blank"}

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/interhop/collectes/cagnotte-de-soutien-recours-devant-le-conseil-d-etat/widget-compteur" style="width:100%;height:450px;border:none;"></iframe>
