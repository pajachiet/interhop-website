---
layout: post
title: "Présentation des Webinars RGPD "
categories:
  - Webinar
  - RPGD
ref: webinar-rgpd-fevrier
lang: fr
---

Le prochain Webinar RGPD aura lieu le jeudi 11 février à 18h00 pour une durée de 20 à 30 minutes, il traitera de la mise en conformité RPGD.

## Webinar RGPD
Les Webinar RPGD ont lieux en ligne et s'adressent préférentiellement aux professionnel.le.s de santé. La Déléguée à la Protection des données (DPD/DPO) d'InterHop veut accompagner la mise en oeuvre et pérenniser la démarche de mise en conformité au RGPD au sein des organisations de santé (cabinets médicaux, paramédicaux...) et sensibiliser les professionnels de santé salariés aux enjeux des textes réglementaires européens. 

<!-- more -->

## Le prochain Webinar RGPD d'InterHop : Mise en conformité au RGPD

## Quoi ?
Pour se mettre en jambes, le premier traitera de la conformité au RGPD, et d'autres suivront.
Tu peux nous communiquer tes questions, nous y répondrons:)

## A qui s'adresse ce webinar?
Ce webinar s'adresse plus particulièrement aux **professionnels de santé**.

## Quand ?

Le jeudi 11 février à 18h00 !

## Points traités
* Le RGPD : c'est quoi ?
* Les principes de la mise en conformité,
* Les premières actions simples à mettre en oeuvre,
* Les questions à se poser,
* Comment InterHop peut aider ?

# Pour nous rejoindre

Ca sera sur ce lien : [https://visio.octopuce.fr/b/int-isp-0fz-iy](https://visio.octopuce.fr/b/int-isp-0fz-iyb){:target="_blank"}

Pour s'inscrire c'est ici [https://forms.interhop.org/node/24](https://forms.interhop.org/node/24){:target="_blank"}.

Nous t'enverrons le code d'accès au salon de discussion 24h avant par courriel. Cela nous servira à avoir une idée du nombre de participants (pour que le serveur tienne le coup :-)). 

Nous utilisons la solution de vidéo conférence BigBlueButton, opensource et protectrice des données personnelles car hébergée chez Octopuce, société de droit français (avec des serveurs en France) : [https://www.octopuce.fr/mentions-legales/](https://www.octopuce.fr/mentions-legales/){:target="_blank"}
