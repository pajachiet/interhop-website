---
layout: post
title: "Le Health Data Hub centralise TOUJOURS les données de santé chez Microsoft"
categories:
  - HDH
  - Microsoft
  - CloudAct
  - RGPD
redirect_from:
  - /courrier-dpd-liste-des-donnees-stockees-au-hdh
show_comments: true
lang: fr
ref: courrier-dpd-liste-des-donnees-stockees-au-hdh
---

Alors que les risques inhérents au transfert de données issues du Health Data Hub vers les États-Unis est pointé par la CNIL et le Conseil d'État, la plus haute juridiction administrative française, est-ce possible de continuer d’utiliser un Microsoft Azure pour centraliser l’ensemble de nos données de santé ?

<!-- more -->

# Point d'actualité
- Juillet 2019 : élargissement du Système National des Données de Santé. Il contient donc l’ensemble des données médicales donnant lieu à remboursement de soins de plus de 67 millions de personnes : création du Health Data Hub avec le stockage de l'ensemble des données de santé **chez Microsoft Azure, sans appel d’offre**
- Juillet 2020 : la Cour de Justice de l’Union Européenne invalide un accord international permettant les transferts de données aux États-Unis. En effet les services de renseignements américains peuvent accéder à l'ensemble des données des citoyen.ne.s non américain.e.s.
- Octobre 2020 : un collectif citoyen attaque le Health Data Hub géré par Microsoft au Conseil d'État. Microsoft n'est plus la solution pérenne pour le Health Data Hub.

**Cependant actuellement la plateforme Microsoft Azure est toujours active.**

# Que pouvons nous faire pour stopper cette atteinte à nos droits fondamentaux ?

### Signer la pétition d'interhop

Une pétition officielle sur le site du Sénat veut obtenir la création d'une commission d'enquête sur l'accord avec la société Microsoft concernant la gestion des données de santé des Français.

<a style="text-align: center;" class="button alt" type="blue" href="https://petitions.senat.fr/initiatives/i-455">Signer la pétition !</a>

### Envoyer un mail aux délégués à la Protection des Données du Health Data Hub et de la Sécurité Sociale

#### Destinataires:
- contact@health-data-hub.fr
- thomas.duong@health-data-hub.fr
- dpd@health-data-hub.fr
- dpo@santepubliquefrance.fr
- dpo.cnam@assurance-maladie.fr
- sidep-rgpd@sante.gouv.fr
- dgs-rgpd@sante.gouv.fr 

#### Objet du mail
Demande d’accès à l’information concernant les données stockées et traitées au sein du Health Data Hub Microsoft

#### Contenu du courriel

N'oubliez pas de signer le courriel avec votre Nom et votre Prénom :-)

<div class="alert alert-red" markdown="1">
Madame, Monsieur Madame, Monsieur le.la Délégué.e à la Protection des Données

Dans le cadre des obligations telles qu’issues du Règlement Général sur la Protection des Données et de la loi Informatique et Libertés, je vous adresse cette présente demande aux fins de recevoir l’ensemble des informations concernant les données à caractère personnel qui me concernent et sont traitées au sein de la Plateforme des Données de Santé (Health Data Hub).

Conformément au 1.b de l’article 15 du RGPD, j’aimerais connaître la liste exhaustive des données à caractère personnel me concernant et susceptibles d’être traitées au sein de votre plateforme.

J’aimerais également recevoir des informations sur les délais de conservation de chacune de ces données, les destinataires de ces données.

Par ailleurs, conformément à l’article 16 du RGPD sur le droit de rectification ou encore à l’article 17 sur le droit à l’effacement pourriez-vous me confirmer dans quelle mesure les informations me concernant sont susceptibles d’être effacées ou rectifiées pour garantir l’effectivité de mes droits.

Si vous avez besoin de quelconques informations supplémentaires pour répondre à mes demandes, merci de m'en faire part par retour de mail.

Dans l’attente de votre retour, veuillez recevoir mes salutations sincères.

Nom Prénom
</div>

### On garde le contact :-)

Tenez-nous au courant sur les réseaux sociaux:
- [https://mstdn.io/@interhop](https://mstdn.io/@interhop)
- [https://twitter.com/interchu](https://twitter.com/interchu)
