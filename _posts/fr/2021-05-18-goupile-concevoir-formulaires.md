---
layout: post
title: "Goupile ou comment concevoir des formulaires"
categories:
  - Goupile
  - eCRF
  - Projets
  - Open Source
ref: goupile-concevoir-formulaires
lang: fr
---
# Un eCRF facile et convivial...

Non, Goupile n’est pas un renard !

Goupile est un éditeur de formulaires pour le recueil de données pour la recherche. C’est une application web utilisable sur ordinateur, sur mobile et hors ligne.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.interhop.org/videos/embed/3cf50015-e374-4998-98ae-c28c6976db1d" frameborder="0" allowfullscreen></iframe>

<!-- more -->

Le nom Goupile n'a aucune signification particulière. Lorsque Niels Martignène  a créé le dossier du projet, il lui fallait un nom. Il a demandé de l’aide auprès de sa compagne, Apolline, grande amoureuse des renards… Et le nom "goupil" est sorti tout seul. 

Après quelques mois, le "e" a été ajouté pour faciliter la recherche sur Google et avoir un nom de domaine disponible :)

Traditionnellement, le  Formulaire de rapport de cas (CRF) est un document papier destiné à recueillir les données des personnes incluses dans un projet de recherche.
Ces documents sont de plus en plus remplacés ou complétés par des portails numériques, on parle alors d'eCRF (''e'' pour *electronical*).

![](https://goupile.fr/static/screenshots/overview.webp) 

Goupile est né parce que les données sont encore souvent recueillies dans des fichiers Excel, et stockés sur les machines personnelles des chercheurs. D'autres utilisent des outils en ligne, comme "Google Sheets" ou "Office 365". Les données sont alors stockées chez les GAFAMs, sur des serveurs non adaptés aux données de santé.

Il existe aussi des logiciels spécifiquement développés pour la santé. Ces outils proposent une approche glisser/déposer et/ou un métalangage, à partir d'une palette de champs de saisie (ex : champs date, nombre, texte). Cette approche est facile et intuitive, mais elle limite les possibilités de développement. De plus, ces outils sont souvent lourds à mettre en place, et généralement utilisés dans le cadre d'études financées.

Goupile est un outil de conception d’eCRF libre et opensource, qui s'efforce de rendre la création de formulaires et la saisie de données à la fois puissantes et faciles. Son accès est nominatif et protégé.

Toutes les pages de Goupile sont *responsive*, ce qui signifie qu'elles s'adaptent à la navigation sur mobile, tablette et ordinateur de bureau.

Goupile a un mode hors ligne en cas de coupure du réseau. Les données sont synchronisées lorsque la connexion est à nouveau disponible.

Aujourd’hui, le projet "Goupile" est porté et développé par l'association InterHop.org, laquelle est l’éditrice du logiciel libre Goupile. Niels Martignène en est le créateur et le développeur principal.

L'équipe d'InterHop est en support pour héberger Goupile sur des serveurs certifiés HDS (Hébergeurs de données de santé), pour accompagner les utilisateurs dans le développement de leurs formulaires et de nouvelles fonctionnalités. Enfin, InterHop propose également une aide pour les questions relatives à la protection et au Règlement Général sur la Protection des Données (RGPD).

Goupile est utilisé par les chercheurs pour des études prospectives, rétrospectives, mono-centriques ou multicentriques.

Goupile est aussi utilisé par les étudiants en santé pour leurs projets de thèses et mémoires nécessitant généralement de recueillir des données. Ces projets relativement simples techniquement sont notre cible immédiate pour plusieurs raisons : ils n’ont pas besoin de fonctionnalités très complexes, le nombre de thèses (par an) est de plusieurs centaines et les alternatives proposées ne sont ni libres ni sécurisées. 

Les membres d'InterHop utilisent Goupile au quotidien pour développer leurs propres formulaires, lesquels sont testés et utilisés par leurs proches collaborateurs (et ami.e.s).

**Parlons maintenant des fonctionnalités de Goupile…**

Goupile permet de concevoir un eCRF avec une approche un peu différente des outils habituels, puisqu'il s'agit de programmer le contenu des formulaires, tout en automatisant les autres aspects communs à tous les eCRF : 
- Types de champs préprogrammés et flexibles, 
- Publication des formulaires,
- Enregistrement et synchronisation des données,
- Recueil en ligne et hors ligne, sur ordinateur, tablette ou mobile,
- Gestion des utilisateurs et droits.

En plus des fonctionnalités habituelles, nous nous sommes efforcés de réduire au maximum le délai entre le développement d'un formulaire et la saisie des données.

Avec Goupile, je code à gauche, je vois le résultat à droite.

Les changements réalisés dans l'éditeur sont immédiatement exécutés et le panneau d'aperçu à droite affiche le formulaire qui en résulte. Il s'agit de la même page qui sera présentée à l'utilisateur final. À ce stade, il est déjà possible de saisir des données pour tester le formulaire, mais aussi de les visualiser et de les exporter.

![](https://goupile.fr/static/screenshots/editor.webp)

Une fois le formulaire prêt, les panneaux de suivi vous permettent d’observer l'avancée de la saisie, de visualiser les enregistrements et d'exporter les données.

![](https://goupile.fr/static/screenshots/data.webp)

**Goupile présente de nombreux avantages.**

Goupile n'est pas basé sur un métalangage (contrairement aux outils classiques dans ce domaine) mais utilise le langage Javascript. C'est un langage de programmation très connu. 
Le fait de programmer donne beaucoup de possibilités, notamment la réalisation de formulaires complexes, sans sacrifier la simplicité pour les formulaires usuels. Ainsi, il y a très peu de limites de développement de nouveaux champs de saisie ou de fonctionnalités.

Nous tenons à vous rassurer sur la complexité. Vous pouvez commencer à créer votre formulaire sans connaissances en programmation. En effet, Goupile fournit un ensemble de fonctions prédéfinies qui permettent de créer les champs de saisie (champ texte, numérique, liste déroulante, échelle visuelle, date, etc.) très simplement.

![](https://goupile.fr/static/screenshots/instant.png)

Il est possible de tester l’outil sur une [instance de démo](https://goupile.fr/demo) proposée par l'association InterHop.
Attention ! L'instance de démo n'est pas certifiée pour la santé : seules des données fictives peuvent être récoltées.

Enfin, dans la mesure où Goupile est un logiciel libre, deux possibilités s'offrent aux utilisateurs.trices : 
-  Utiliser et installer Goupile localement dans leur centre. En effet, Goupile est  délivré sous licence libre : licence AGPL 3.0. Le code source est disponible gratuitement en ligne [ici](https://framagit.org/interhop/goupilemagit.org/interhop/goupile){:target="_blank"}.  Dans ce cas, il.elle.s doivent gérer tous les aspects d'hébergement.
-  Utiliser le service clé en main fourni par l'association InterHop via les serveurs loués chez notre hébergeur de données de santé, [GPLExpert](https://gplexpert.com/hebergement-donnees-sante-hds/){:target="_blank"}.

Dans tous les cas, les utilisateurs.rices peuvent nous solliciter pour le développement de nouvelles fonctionnalités.
