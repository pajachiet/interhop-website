---
layout: post
title: "Lettre aux Hôpitaux"
categories:
  - Lettre
  - Hôpitaux
  - Microsoft
  - CloudAct
  - HDS
  - Loi Pour une République Numérique
redirect_from:
  - /lettre-hopitaux
ref: lettre-hopitaux
lang: fr
---

Mesdames les directrices, Messieurs les directeurs d'hôpitaux, <br>
Chers collègues, chères collègues de la santé,

Un arrêté<sup>[^arrete]</sup> publié le 21 avril incite les hôpitaux et centres de recherche à intensifier l'envoi des données de santé des patients dans le "Health Data Hub" (HDH), hébergé actuellement par la société Microsoft<sup>[^microsoft]</sup>.

Au-delà des questions d'opportunité du choix de la société Microsoft[^choixmicrosoft], notamment en matière de protection des données personnelles, l'application de cet arrêté pourrait vous placer dans une situation juridique délicate au titre du deuxième alinéa de l’article 40 du Code de procédure pénale<sup>[^article40]</sup>.

<!-- more -->

En effet, il n'apparaît pas aujourd'hui que le "Health Data Hub" dispose de la certification "activité 5" nécessaire à la gestion de ces données<sup>[^L1115-8]</sup>. Il ne figure d'ailleurs pas dans la liste des hébergeurs certifiés pour les données de santé[^listehebergeur], pas plus que son sous-traitant "Open".


Notamment, les articles L 1115-1<sup>[^L1115-1]</sup> et L 1115-2<sup>[^L1115-2]</sup> du Code de la santé punissent l'hébergement de données de santé à caractère personnel (DSCP) sans agrément ou certification de :
- de trois ans d'emprisonnement et 45 000 euros d'amende
- d'interdiction pour une durée de 5 ans ou plus d'exercer directement ou indirectement une ou plusieurs activités professionnelles ou sociales
- de placement, pour une durée de 5 ans ou plus, sous surveillance juridique
- de fermeture définitive ou pour une durée de cinq ans au plus des établissements ou de l'un ou de plusieurs des établissements de l'entreprise ayant servi à commettre les faits incriminés
- d'exclusion des marchés publics à titre définitif ou pour une durée de cinq ans au plus
- d'affichage de la décision prononcée ou la diffusion de celle-ci soit par la presse écrite, soit par tout moyen de communication au public par voie électronique

En envoyant des données de santé au HDH, il faut se poser la question de ce risque.

Nous sommes conscients du fait que la société Microsoft, sur laquelle s'appuie la HDH, dispose bien de la certification "activité 5". Mais celle-ci ne protège pas juridiquement car ce n'est pas Microsoft qui administre et exploite le système d’information contenant les données de santé du HDH, mais bien le HDH lui-même ou son sous-traitant "Open". Le niveau de certification dont peut légalement se prévaloir le HDH en utilisant la plate-forme technique Microsoft est l'"activité 4", insuffisant dans le cas présent puisqu'il ne concerne que les infrastructures techniques.

Pour s'en convaincre, il faut lire le "Point d’actualité sur la certification hébergeur de données de santé"<sup>[^pointactu]</sup> du 6 mai 2019 publié par l'Association Pour la Sécurité des Systèmes d'Information de Santé (APSSIS). Cette note juridique s'appuie sur les FAQ<sup>[^FAQ]</sup> publiées en 2018 et 2019 par le MES. 

Il existe pourtant de nombreuses autres solutions techniques permettant d'avancer.

Dans un cadre seulement destiné à la recherche, la certification n'est pas obligatoire. Des plateformes d'intelligence artificielle telles que Teralab - déjà gérée par l'Etat (Institut Mines Télécom), celle de l'APHP ou de nombreuses autres peuvent être utilisées. Il existe de grands logiciels libres[^repnum], notamment issus de l'INRIA, permettant de mener à bien nos travaux dans de bonnes conditions.

Si des données de santé sont utilisées dans un cadre qui nécessite une certification "activité 5", il existe de nombreuses sociétés en France disposant déjà d'une certification. L'AFHADS<sup>[^afhads]</sup>, Association Française des Hébergeurs Agréés de Données de Santé, recense ces sociétés dans un annuaire.

Amicalement

Le collectif interhop.org


[^arrete]: [Arrêté du 21 avril 2020 complétant l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421)

[^listehebergeur]: [Liste des hébergeurs certifiés pour les données de santé](https://esante.gouv.fr/labels-certifications/hds/liste-des-herbergeurs-certifies)

[^microsoft]: [Modalités de stockage du « health data hub » -  Question écrite n° 14130 de M. Claude Raynal (Haute-Garonne - SOCR) publiée dans le JO Sénat du 30/01/2020](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^choixmicrosoft]: [Données de santé: l’Etat accusé de favoritisme au profit de Microsoft](https://www.mediapart.fr/journal/france/110320/donnees-de-sante-l-etat-accuse-de-favoritisme-au-profit-de-microsoft)

[^article40]: [Article 40 du Code de procédure pénale](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=B5F65545EFB0882820C41758AE863B27.tpdjo14v_1?cidTexte=LEGITEXT000006071154&idArticle=LEGIARTI000006574931&dateTexte=&categorieLien=cid)

[^L1115-1]: [Code de la santé publique - Article L1115-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033862544&cidTexte=LEGITEXT000006072665&dateTexte=20180401)

[^L1115-2]: [Code de la santé publique - Article L1115-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020631535&cidTexte=LEGITEXT000006072665&dateTexte=20090514)

[^L1115-8]: [Code de la santé publique - Article L1115-8](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000033862549&cidTexte=LEGITEXT000006072665&dateTexte=20180401)

[^repnum]: [Loi pour une République numérique](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000033202746&categorieLien=id)

[^pointactu]: [APSSIS : Point d’actualité sur la certification hébergeur de données de santé](https://www.apssis.com/actualite-ssi/335/point-d-actualite-sur-la-certification-hebergeur-de-donnees-de-sante.htm)

[^explic]: [Explicitation du champ d’application du cadre juridique de l’hébergement de données de santé par le ministère chargé de la Santé, représenté par la Délégation à la stratégie des systèmes d’information de santé - 2018](https://esante.gouv.fr/sites/default/files/media_entity/documents/nouvelle_faq_hds_12_07_2018_v0_10.pdf)

[^explicdeux]: [Explicitation du champ d’application du cadre juridique de l’hébergement de données de santé par le ministère chargé de la Santé, représenté par la Délégation à la stratégie des systèmes d’information de santé - 2019](https://esante.gouv.fr/sites/default/files/media_entity/documents/FAQ%20HDS_02042019.pdf)

[^FAQ]: [ASIP Santé – Foire aux questions sur l’hébergement des données de santé](https://esante.gouv.fr/sites/default/files/media_entity/documents/faq_hds.pdf)

[^afhads]: [Association Française des Hébergeurs Agréés de Données de Santé (AFHADS)](https://www.afhads.fr/)
