---
layout: post
title: "[Newsletter] Lancement de nos outils et retour sur la commission Latombe"
categories:
  - Newsletter
  - Mars
ref: newsletter-mars-2021
lang: fr
show_comments: true
---

Dans cette nouvelle newsletter, nous abordons les point suivants : nos financements, le lancement de nos premiers outils et un point sur la commission Latombe et l'actuelle position de la CNAM par rapport au  Health Data Hub.

<!-- more -->

# Appel aux dons

Donateurs, Donatrices,

L'équipe d'InterHop tient à vous faire part de sa plus sincère gratitude pour votre contribution au développement de ses projets. Votre générosité lui permet d'installer des applications qui traitent et stockent des données de santé dans un cadre éthique et protecteur pour les libertés fondamentales.

# Cagnotte de soutien - Recours devant le Conseil d'État

Nous sollicitons à nouveau [votre générosité](https://interhop.org/2021/03/03/conseil-etat-vaccination-audience) pour financer les travaux d'avocat ainsi que l'expertise technique mobilisée.

En effet, le collectif InterHop a déposé une requête auprès du Conseil d’État, pour dénoncer la décision du gouvernement de confier, à Doctolib, la gestion de prise de rendez-vous concernant la vaccination contre le coronavirus.

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/interhop/collectes/cagnotte-de-soutien-recours-devant-le-conseil-d-etat/widget-compteur" style="width:100%;height:450px;border:none;"></iframe>

La ligue des Droits de l'Homme, le professeur Didier Sicard, le Syndicat de la Médecine Générale, l’Union Française pour une Médecine Libre, le Syndicat National des Jeunes Médecins Généralistes, la Fédération des Médecins de France, ainsi que des associations de patient·e·s, comme ActUp Santé Sud-Ouest ou Marie Citrini, représentante des usagers de l’AP-HP (Assistance Publique-Hôpitaux de Paris) ont rejoint le collectif InterHop.

Le référé-liberté a pour objet de dénoncer la décision du Ministère de la Santé d'avoir recours au prestataire Doctolib pour la gestion de la prise de RDVs en ligne pour la campagne de vaccination liée au COVID-19 en ce que l'hébergement des données de santé est réalisée auprès d'Amazon Web Services.
Or, en l'état actuel de la jurisprudence européenne, le droit américain n'assure pas un niveau de protection équivalent au RPGD ; pourtant les données de santé hébergées chez Amazon y seraient soumises.

[Médiapart](https://www.mediapart.fr/journal/france/260221/vaccination-le-partenariat-avec-doctolib-conteste-devant-le-conseil-d-etat?onglet=full){:target="_blank"}, [France 24](https://www.france24.com/fr/france/20210305-vaccination-les-donn%C3%A9es-de-sant%C3%A9-de-doctolib-suscitent-l-inqui%C3%A9tude-de-m%C3%A9decins){:target="_blank"}, [Numérama](https://www.numerama.com/tech/692317-pourquoi-la-presence-de-doctolib-dans-la-campagne-de-vaccination-fait-encore-polemique.html){:target="_blank"} ou encore [Euraktiv](https://www.numerama.com/tech/692317-pourquoi-la-presence-de-doctolib-dans-la-campagne-de-vaccination-fait-encore-polemique.html){:target="_blank"} parlent de cette action.

Ensemble ! Vos dons permettent de pérenniser nos actions pour défendre nos libertés fondamentales.

# Lancement de l'H.D.S.

Pour les protéger, les données de santé réputées sensibles doivent être dans un environnement HDS c'est à dire qu'elles doivent bénéficier d'un hébergement tout à fait particulier que l'on appelle Hébergement de Données de Santé (HDS), gage de qualité pour sécuriser les données de santé puiqu'il est certifié. Depuis le 24 février, notre service HDS est opérationnel: l'ensemble des données sont hébergées en France par GPLExpert, partenaire d'InterHop.


# Lancement de Goupile

[Goupile](https://goupile.fr/) est un eCRF (''e'' pour electronical). C'est la nouvelle génération de Clinical Report Form (CRF), document traditionnellement papier destiné à recueillir les données des sujets du projet de recherche, que ce soit au moment de l’inclusion dans le projet ou dans le suivi.

Avec Goupile, on peut facilement créer des questionnaires protégeant les données des patient·e·s et permettant au praticien·ne de travailler plus facilement sur son ordinateur.

Goupile n'attendait plus que l'hébergement de données de santé (HDS) pour servir la recherche, et bien d'autres missions.
 
# Retour sur la Commission Latombe

Adrien PARROT, Médecin-ingénieur et président de l'association InterHop et Juliette ALIBERT, Avocate [ont été auditionnés par le député Philippe LATOMBE le 18 février 2021 à l'Assemblée Nationale](https://interhop.org/2021/02/22/commission-souverainete), dans le cadre de commissions sur la thématique : Données de santé et souverainetê numérique.

Les interlocuteurs d'InterHop se sont montrés particulièrement intéressés par les points suivants :
- Protection des données de santé,
- Recours aux logiciels libres et opensource pour la santé : avantages, limites et risques,
- Communs numériques, nourriture de réflexion sur la souveraineté numérique,
- Health Data Hub et hébergement des données de santé : situation actuelle et devenir,
- HDH et leçons à en tirer pour les autres projets numériques portés par les pouvoirs publics,
- Initiative GAIA-X et souveraineté européenne,
- Garanties de sécurité satisfaisantes au regard de cyberattaques de plus en plus sophistiquées.

Lors de cette audition, Adrien Parrot a souligné que l'association est absolument pour la recherche en santé ainsi que pour le partage de données par le biais de différents systèmes d'information, dans le respect du RGPD. InterHop se place sur le terrain de la protection des libertés fondamentales comme peut l'être la CNIL, la CNAM... La problématique liée au HDH est la centralisation des données et l'extraterritorialité du droit américain.
Des alternatives existent comme le Ouest Data Hub ou encore les entrepôts de données de santé de l'APHP, de Marseille, de Toulouse, de Lille, et bien d'autres, grâce à des logiciels opensource...

[Á voir ou à écouter](http://videos.assemblee-nationale.fr/video.10384612_602e62bcebf7b.batir-et-promouvoir-une-souverainete-numerique-nationale-et-europeenne---m-adrien-parrot-medecin--18-fevrier-2021){:target="_blank"}

# Avis de la CNAM

[Dans un communiqué du 19 février que InterHop s'est procurée](https://interhop.org/2021/02/19/depeche-hdh-cnam), le Conseil d'Administration de la Caisse Nationale de l’Assurance Maladie (CNAM), à l'unanimité, s'est de nouveau opposé à l'hébergement du Health Data Hub par Microsoft Azure.

Le Conseil se dit "en responsabilité et soucieux de concilier l’impérative protection des données des assurés sociaux et la santé publique, et les enjeux en matière d’innovation et de développement de stratégie nationale".

Il ajoute "Les conditions juridiques nécessaires à la protection de ces données ne semblent pas réunies pour que l’ensemble de la base principale soit mis à disposition d’une entreprise non soumise exclusivement au droit européen" (RGPD).

"Seul un dispositif souverain et uniquement soumis au RGPD permettra de gagner la confiance des assurés dans l’utilisation de leurs données".
Par ailleurs, "il faut s’inscrire pleinement dans les nouvelles orientations du ministre des solidarités et de la santé, Olivier Véran, ainsi que du secrétaire d'État chargé du numérique, Cédric O, et respecter le calendrier annoncé pour bâtir une nouvelle plateforme de données de santé".

En effet, Olivier Véran s'est engagé à "faire disparaître complètement" les risques que pose l'hébergement du Health Data Hub par Microsoft et à "adopter une nouvelle solution technique" d'ici deux ans dans un courrier adressé à la CNIL en novembre 2020.

"Dès maintenant les travaux initiés doivent permettre de confier à un opérateur souverain et de confiance l’hébergement des données", estime le Conseil de la CNAM.

La portée politique de cette décision est importante et les conséquences non encore totalement connues. Cette décision s'inscrit dans la démarche d'InterHop et nous la saluons.

Rendez-vous lundi prochain, le 8 mars au Conseil d'État !

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/interhop/collectes/cagnotte-de-soutien-recours-devant-le-conseil-d-etat/widget-bouton" style="width:100%;height:70px;border:none;"></iframe>
