---
layout: post
title: "Pourquoi le Health Data Hub travestit la réalité sur le chiffrement des données de santé sur Microsoft Azure"
categories:
  - Chiffrement
  - homomorphe
  - Stockage
  - Analyse
redirect_from:
  - /healthdatahub-travestit-le-chiffrement-des-donnees
show_comments: true
ref: healthdatahub-travestit-le-chiffrement-des-donnees
lang: fr
---

> "Des travailleurs en sécurité informatique dans des hopitaux et cabinets d’audit indépendants" s'exprime sur le chiffrement des données de santé réalisé sur le Health Data Hub - Microsoft


## Avant-propos

Nous sommes des professionnels en cybersécurité, ayant l’expertise et l’expérience nécessaire pour affirmer que héberger les données de santé françaises sur des serveurs étrangers, tel que Microsoft Azure est un acte qui met en péril la sécurité des données de nos citoyens. En effet même les systèmes de chiffrement les plus avancés ne protègent pas nos données quand elles sont hébergés sur des serveurs physiques auxquels on ne peut accorder de confiance.

<!-- more -->

Dans le contexte du projet Health Data Hub, ses défenseurs ont souvent invoqué l’argument que les données de santé seraient chiffrées et que donc il ne faudrait pas s’inquieter du fait qu’elles soient stockées sur Microsoft Azure. On comprendrait donc, avec un tel argumentaire, que ce chiffrement empecherait Microsoft d’accéder aux données de santé stockées sur sa plateforme Azure.

Dans ce document nous allons explorer les détails techniques pour mieux comprendre en quoi cette affirmation ne peut pas être vraie dans le contexte du Health Data Hub. Nous allons élucider pourquoi cette affirmation n’est qu’un artifice techno-juridique qui n’a aucune pertinence technique en sécurité informatique.


## Revenons aux fondamentaux

Dans le fonctionnement d’un ordinateur nous avons aujourd’hui, pour faire simple, trois éléments; Un *processeur*, de la *mémoire vive* rapide mais moins volumineuse, et un *disque dur* plus lent mais pouvant stocker bien plus de données et surtout qui n’a pas besoin d’être alimenté en énergie pour continuer a se souvenir des données contrairement a la mémoire vive.

Le processeur, pour fonctionner, a besoin de pouvoir lire du code machine, qui est dans la majorité des cas, obtenu depuis le code source, lisible par un humain, via un compilateur. Le processeur pour fonctionner a aussi besoin de pouvoir lire les données (chiffrées ou pas) à traiter avec ce code machine. Le code machine c’est le langage que le processeur comprends et qui lui dit ce qu’il doit faire, sans celui ci le processeur ne fait rien. Les données et le code machine sont généralement stockés sur le disque dur, puis copiés dans la mémoire vive pour être ensuite utilisés par le processeur durant son fonctionnement. La raison de cela est que le disque dur est trop lent pour répondre à la cadence du processeur durant son fonctionnement. Le processeur ne peut pas fonctionner sans l’intermediaire de la memoire vive, cela serait bien trop lent pour être utilisable.

En plus de la memoire vive, le processeur s’appuie également sur d’autres types de mémoire encore plus petits et plus rapides qui font l’intermédiaire avec la memoire vive et qui sont situés à l’intérieur du processeur. On appelle ça le cache.

Dans tous les cas, le processeur pour fonctionner doit pouvoir lire du code machine en clair et qui ne soit donc pas chiffré, sinon il ne fonctionnerait pas. On peut donc conclure que si un chiffrement du code machine existe, un déchiffrement doit se faire avant de donner la possibilité au processeur d’effectuer toute opération de traitement sur les données. De la même manière, si un chiffrement existe sur les données à traiter, avant toute opération il est nécéssaire que le processeur déchiffre ces données pour pouvoir y effectuer le traitement. Le point qu’on voudrait transmettre au lecteur est que, si le traitement des données se fait chez l’hébergeur, les données sont forcément, à un moment ou un autre, présentes sur le matériel informatique de façon non chiffrée, en clair.


## Microsoft Azure: un hébergeur au sens physique de matériel informatique

Microsoft Azure possède du matériel informatique et des locaux pour le stocker. Ce matériel informatique c’est des processeurs, de la memoire vive, des disques durs, et bien plus encore. Microsoft Azure exerce un contrôle physique sur ce matériel, c’est à dire qu’il a la possibilité de faire faire au matériel ce qu’il veut, contrairement a ses clients, qui eux, fonctionnent toujours par l’intermediaire de Microsoft Azure.

À partir du moment où on sait que les données et le code machine doivent pouvoir être lus par le processeur pour que des traitements soient possibles, alors on comprend que Microsoft Azure, hebergeur physique du materiel, possède les moyens d’acceder aux données. Si cela n’était pas le cas, Microsoft Azure n’effectuerait aucun traitement lui même, or, le Health Data Hub dit avoir besoin de Microsoft Azure pour effectuer les traitements. Le Health Data Hub n’utilise donc pas Microsoft Azure seulement comme entrepot de stockage de données chiffrées, car si c’etait le cas, rien ne justifierait l’usage de Microsoft Azure. Stocker des données tous les hebergeurs savent le faire,
les hébergeurs francais aussi. Le Health Data Hub n’ayant à priori aucune infrastructure en propre, c’est donc forcément Microsoft Azure qui effectue les traitements, et si Microsoft Azure effectue les traitements, alors celui ci a la possibilité d’accéder aux données, c’est simplement logique et nécessaire d’un point de vue technique, sinon les choses ne fonctionneraient pas.

## Le chiffrement des données au repos sur le disque dur est inutile pour le Health Data Hub

Dans beaucoup de cas, et même sur nos ordinateurs personnels, on a la possibilité aujourd’hui de chiffrer des données quand elles sont stockées sur nos disques durs, notamment via des technologies telles que LUKS sur GNU/Linux ou BitLocker sur Windows. Mais ce chiffrement n’est pas utile pour protéger contre un hébergeur qui opère du matériel en état de fonctionnement et qui a besoin d’effectuer des traitements. Avant d’effectuer des traitements, les données hébergés chez Microsoft Azure ont besoin d’être déchiffrées et donc l’hébergeur doit posséder la clef de dechiffrement. Sur nos ordinateurs personnels le même problème existe, une attaque physique sur un ordinateur allumé ou suspendu (mais pas en état d’hibernation) permet de contourner le chiffrement des données sur le disque dur car la clef de dechiffrement est présente en clair dans la mémoire vive et peut donc être extraite. Il s’agit d’une technique bien connue par les mêmes forces de l’ordre, qui en cas d’une saisie pendant que le matériel est en état de fonctionnement, vont extraire les clefs de chiffrement de la mémoire vive pour avoir accès au contenu de la machine. C’est le même cas de figure pour Microsoft Azure et tout autre hébergeur. L’efficacité du chiffrement sur le disque dur est réduite a néant lorsqu’on a à faire à des systèmes en état de fonctionnement. Le chiffrement des données sur le disque dur n’est utile que pour protéger des données au repos sur des systèmes. Systèmes sur lesquels on ne va jamais effectuer des traitements et donc déchiffrer les données. Le déchiffrement ne doit pas se faire sur la machine d’archivage mais sur d’autres systèmes de confiance, par exemple un ordinateur personnel qui irait stocker des données chiffrées sur un hébergeur de données comme Microsoft OneDrive, Google Drive, OVH Hubic et autres, pour ensuite les traiter en local.

## Et les technologies "d'enclave" dans tout ça?

Qu’est-ce qu’une “enclave” me direz vous? Les enclaves sont des technologies de “chiffrement” des données effectué durant l’usage, telles que Intel SGX ou AMD SME. Ici nous ne parlerons que de ces deux, car elles seraient les seules a être pertinentes dans le contexte de Microsoft Azure et du traitement sur des données massives avec l’éco-système logiciel existant aujourd’hui.

Vous noterez que le mot chiffrement a été mis entre guillements plus haut, et la raison est que les enclaves ne contredisent pas les fondamentaux qu’on a pu aborder plus tot, le processeur doit toujours lire le code machine et les données en clair, donc sans chiffrement, pour que les choses fonctionnent. En réalité ce que font les enclaves c’est simplement cacher plus profondément une clef de déchiffrement au sein du processeur, mais celle ci est toujours présente dans le matériel appartenant à Microsoft Azure, qui le possède physiquement dans ses bâtiments, ayant donc accès aux clefs de déchiffrement des données utilisées dans l’enclave. L’expérience prouve qu’il est possible d’extraire ou de casser le chiffrement des enclaves,  surtout quand on possède le matériel physiquement. Software Guard Extensions[^Software_Guard_Extensions] nous informe que 7 failles ont été découvertes en l'espace de trois ans sur les enclaves Intel SGX. AMD SME hérite aussi des mêmes problèmes architecturaux du fait que la clef de déchiffrement se trouve toujours à l'intérieur du processeur[^Epyc_crypto].

Les technologies d’enclave chiffrent la mémoire vive mais ne chiffrent pas le cache, cela serait contradictoire au fonctionnement même du processeur. C’est pour ça que ce mécanisme n’est pas solide, et c’est pour ça qu’en quelques années 7 failles ont été trouvées, car la clef de déchiffrement est toujours stockée dans le processeur.

Il est évident qu'avec une telle cadence de découverte de nouvelle failles, même les technologies d'enclaves ne sont pas un bon mécanisme pour protéger nos données de santé. Ces données  doivent rester secrètes pour de longues périodes de temps, plusieurs dizaines d'années au minimum, pas seulement quelques mois, le temps qu'une nouvelle faille ouvre la possibilité technique pour les propriétaires et hébergeurs de matériel  informatique d'accèder aux données de santé.

Les enclaves sont-elles viables pour l’éco-système logiciel autour des données massives? En majeure partie non, pour l’instant les enclaves n’offrent pas la même performance, la quantité de mémoire dont elles peuvent bénéficier est limitée[^OpenEnclave] [^DC-series] -  maximum de “EPC Memory”, la dimension des machines virtuelles pour les traitements sans enclave sur la même machine en coopération avec les traitements avec enclave reste aussi limitée, 8 vCPUs et 32GB de RAM maximum, ce qui est limitant pour beaucoup de traitements sur les données de santé sans considérer que ces enclaves n’existent pas sur les processeurs graphiques (GPU), les logiciels de traitement sur les données massives ne supportent pas bien ces technologies d’enclave, alors même si on considerait ces technologies comme fiables aujourd’hui, cela ne serait pas pratiquable. Neanmoins, il n’est pour l’instant pas possible de les considerer comme fiable, l’experience le montre. Il serait raisonnable de considérer comme fiable une technologie de sécurité informatique matérielle dès lors que celle ci existe pendant plusieurs années avec de la recherche en failles de sécurité active mais sans aucune nouvelle découverte et que toutes les failles précedemment connues sont corrigées.

Pour l’instant tout laisse a croire que le Health Data Hub n’utilise pas d’enclaves dans son architecture, car la fonctionnalité elle même n’est pas disponible en Europe chez Microsoft Azure[^DC-series]  “Currently available in UK South, Canada Central, and US East only.”, alors que le Health Data Hub dit utiliser des centres de données Microsoft Azure aux Pays Bas.

## Les processeurs graphiques (GPU), eux aussi nécessaires pour traiter efficacement les données de santé

Les processeurs dit "graphiques" restent des processeurs, ils héritent des mêmes fondamentaux abordés plus tôt, ils sont simplement plus spécialisés quant a leur capacité de calcul, et donc sont plus rapides pour effectuer des traitements dit "graphiques", mais aujourd'hui cela s'applique largement à l'analyse de données en matière d'intelligence artificielle.

À part une vague mention dans un article[^EGX_Platform] aucune trace des enclaves chez NVIDIA, seul acteur qui pourrait prétendre aux traitements des données de santé aujourd'hui, de par son support via l'éco-système logiciel et sa performance, AMD ayant actuellement du retard technique, mais néanmoins avance vite. Les enclaves ne sont néanmoins toujours pas plus fiables même sur GPU tant qu'elles ne font que cacher des clefs de déchiffrement plus profondément.

## D'autres formes de cryptographie ne sauverons pas le Health Data Hub

Le chiffrement homomorphe[^Chiffrement_homomorphe] est encore un autre spécimen, c'est une base théorique en cryptographie qui permet de faire des opérations sur des données chiffrées sans posséder la clef de déchiffrement, c'est à dire qu'un processeur, via des algorithmes de chiffrement homomorphes pourrait effectuer des traitements sur des données qui resteraient chiffrées et dont il n'aurait pas la connaissance. Dans le cas où cette solution serait envisagée il y aurait un important problème de performance: le chiffrement homomorphe est trop lent pour des opérations à grande échelle. Beaucoup trop lent pour être applicable dans un scénario de traitement sur des données massives[^The_Fact_and_Fiction]. Utiliser le chiffrement homomorphe aujourd'hui serait contradictoire aux besoins de rapidité du Health Data Hub. L'éco-système logiciel autour des données massives est lui aussi largement inadapté au chiffrement homomorphe.

Toutes les technologies se basant sur la cryptographie visant à garantir qu'on puisse effectuer des traitements de données via un hébergeur informatique sans pour autant lui donner accès à ces mêmes données ne sont que des chantiers de recherche ouverts, certes prometteurs, mais expérimentaux et ne sont pas utilisables dans un environnement de production à ce jour. 

# Conclusion

Il est largement établi par la communauté de la sécurité informatique que les enclaves sont des barrières très éphémères et qu'elles ne permettent pas de chiffrer de manière forte les données pendant l'usage. Les données de santé demandent beaucoup plus qu'une barrière éphémère qui peut être contournée de manière triviale. L'état de l'art de la technique en sécurité informatique aujourd'hui ne permet pas d'utiliser un hébergeur, que ce soit Microsoft Azure ou autre, pour effectuer des traitements sur des données sensibles sans lui donner en même temps accès en clair à ces données. Le Health Data Hub met en place un artifice techno-juridique affirmant que Microsoft Azure ne peut pas accéder aux données car elles seraient chiffrées, **ceci est techniquement contradictoire**. Que cela soit via du chiffrement des données sur le disque dur, ou pendant l'usage avec des enclaves.

C'est avec tous ces éléments techniques qu'on comprend que, même en excluant du débat l'aspect légal problématique du Patriot Act et du Cloud Act présent aux États Unis et qui s'applique à Microsoft, il n'est pas possible aujourd'hui de protéger le secret médical en utilisant des hébergeurs tels que Microsoft Azure, qui ne sont pas résidents sur le sol français. Déployer un service en toute rapidité en utilisant Microsoft Azure signifierait ignorer tous les aspects techniques de la sécurité informatique, **c'est donner à Microsoft le coffre fort et les clefs pour l'ouvrir, en laissant à la seule discrétion de cet acteur privé la bonne volonté de ne pas accéder à ces données.** La seule solution viable qui puisse respecter le cadre légal et la vie privé des citoyens français aujourd'hui reste de monter sa propre infrastructure informatique comme le Système National des Données de Santé le fait, voir de prudemment utiliser un hébergeur local sur lequel la France exerce un controle légal direct tel que Online, Gandi, OVH ou Scaleway pour n'en citer que quelques uns. Hébergeurs qui, contrairement à Microsoft, entreprise supranationale, dépendent de la France pour leur survie et donc sur lesquels un controle légal est assuré.

[^Chiffrement_homomorphe]: [Chiffrement homomorphe — Wikipedia, 2019](https://fr.wikipedia.org/w/index.php?title=Chiffrement_homomorphe&oldid=157541583)

[^DC-series]: [DC-series - Azure Virtual Machines - Azure Virtual Machines, 2020](https://docs.microsoft.com/en-us/azure/virtual-machines/dcv2-series)

[^Epyc_crypto]: [Epyc crypto flaw? AMD emits firmware fix for server processors after Googler smashes RAM encryption algorithms, 2019, The Register]( https://www.theregister.com/2019/06/26/amd_epyc_key_security_flaw)

[^EGX_Platform]: [NVIDIA Blogs: EGX Platform to Ensure Security & Resiliency on A Global Scale, 2020, Official NVIDIA Blog](https://blogs.nvidia.com/blog/2020/05/15/egxsecurity-resiliency)

[^OpenEnclave]: [OpenEnclave - Issue - Fail to create enclave with size more than 31 MB in windows, 2020, GitHub](https://github.com/openenclave/openenclave/issues/2394)

[^Software_Guard_Extensions]: [Software Guard Extensions - Wikipedia, 2020](https://en.wikipedia.org/w/index.php?title=Software_Guard_Extensions&oldid=962210336)

[^The_Fact_and_Fiction]: [The Fact and Fiction of Homomorphic Encryption.” 2019. Dark Reading, January](https://www.darkreading.com/attacksbreaches/the-fact-and-fiction-of-homomorphicencryption/a/d-id/1333691)
