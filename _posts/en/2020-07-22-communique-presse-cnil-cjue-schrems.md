---
layout: post
title: "Press release: letter to the CNIL"
ref: communique-presse-cnil-cjue-schrems
lang: en
---

Sending a letter to the Commission Nationale de l'Informatique et des Libertés CNIL following the **cancellation of the agreement on the transfer of personal data between Europe and the United States or "Privacy Shield "**.

<!-- more -->

# Subject

Request for an explanation of the implications for the "Health Data Platform" of the cancellation of the "Data Protection Shield" or "Privacy Shield".

# Content of the letter

Madam President,

The deployment of a "Health Data Platform" through the creation of a Public Interest Grouping, was proclaimed by law n° 2019-774 of July 24, 2019 relating to the organization and transformation of the health system[^loisante].
Currently these data are stored at Microsoft Azure[^senat_microsoft], the public cloud of the American giant Microsoft.

Opinion No 2020-044 of 20 April 2020 of your Commission[^CNILdel] refers to the risks of data transfers to third countries and disclosures not authorised by [European Union] law in the context of the subcontracting of the technical solution of the "platform" to Microsoft Azure.
Following this opinion, on 19 June 2020, the Council of State enjoined the "Platform for Health Data" to inform citizens of the "possible transfer of data outside the European Union, taking into account the contract with its subcontractor" [^conseil_etat].
Attention is drawn to **the distinction between "rest" and "transit" data **. During the hearing at the Council of State on 11 June, it was mentioned that even if **Microsoft could guarantee the location of rest data (i.e. storage), this was not the case for transit data (i.e. analysis), which once copied to processors circulate worldwide.** We are not talking here about maintenance data which are not health data.

On 16 July, the Court of Justice of the European Union thus declared that "the limitations on the protection of personal data which result from the internal regulations of the United States concerning access and use by the American public authorities of such data transferred from the [European] Union to that third country are not framed in such a way as to meet requirements equivalent to those required, under [European] Union law, by the principle of proportionality, in that the monitoring programmes based on those regulations are not limited to what is strictly necessary"[^curia].

Following this judgment, the German Regulator stresses that "data should not be transferred to the United States until this legal framework has been reformed" [^CNIL_allemande].

We request your expertise to find out the consequences of the health data set of more than 67 million people at Microsoft Azure within the "Health Data Platform".
More particularly, **we would like to know the consequences of the suppression of the "Privacy Shield" on the functioning of the "Health Data Platform "**, especially since at the time of the referral to the Council of State, and until its Ordinance, the above-mentioned text was still in force.

We have decided to make this letter public. This publicity contributes to the objective of transparency defended by your Commission [^stopcovid_cnil].

With this in mind, we ask you to accept, Madam President, the expression of our highest consideration.

Association interhop.org<br>

[^CNIL_allemande]: [Berlin: Berlin Commissioner issues statement on Schrems II case, asks controllers to stop data transfers to the US](https://www.dataguidance.com/news/berlin-berlin-commissioner-issues-statement-schrems-ii-case-asks-controllers-stop-data)

[^curia]: [The Court invalidates Decision 2016/1250 on the adequacy of protection provided by the EU-US data protection shield](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-07/cp200091fr.pdf)

[^stopcovid_cnil]: [StopCovid application: the CNIL draws the consequences of its controls](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^arrete_sortie_etat_urgence]: [Order of 10 July 2020 prescribing the general measures necessary to deal with the covid-19 epidemic in the territories that have emerged from the state of public health emergency and in those where it has been extended](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042106233&dateTexte=&categorieLien=id)

[^cours_comptes]: [HEALTH PERSONAL DATA MANAGED BY HEALTH INSURANCE](https://www.ccomptes.fr/sites/default/files/EzPublish/20160503-donnees-personnelles-sante-gerees-assurance-maladie.pdf)

[^senat_appeloffre]: [Health data protection: MORIN-DESAILLY Catherine](http://videos.senat.fr/video.1710660_5f10400c7efbf.seance-publique-du-16-juillet-2020-apres-midi?timecode=16471000)

[^reunin_stopcovid]: [StopCovid application press conference, June 23rd](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#)

[^senat_microsoft]: [How to store the "health data hub"](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^CNILdel]: [Measures of organization and functioning of the health system necessary to deal with the covid-19 epidemic in the context of the state of health emergency ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^loisante]: [LAW No. 2019-774 of 24 July 2019 on the organisation and transformation of the health system](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[^theconversation]: [The Conversation - Health Data: the StopCovid tree that hides the Health Data Hub forest ](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^lenouvelobs]: [Le Nouvel Obs - Nos données de santé à Microsoft ? "We offer Americans a national wealth unique in the world"](https://www.nouvelobs.com/sante/20200623.OBS30391/nos-donnees-de-sante-a-microsoft-on-offre-aux-americains-une-richesse-nationale-unique-au-monde.html)

[^lemonde]: [Le Monde - " La politique publique des données de santé est à réinventer "](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html)

[^lepoint]: [Le Point - Health Data Hub: "The choice of Microsoft, an industrial misunderstanding! "](https://amp.lepoint.fr/2379394?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter&Echobox=1591814194&__twitter_impression=true)

[^conseil_etat]: [Council of State, 19 June 2020, Health Data Hub Platform](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)
