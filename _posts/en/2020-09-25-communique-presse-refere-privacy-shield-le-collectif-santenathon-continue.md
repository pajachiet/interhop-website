---
layout: post
title: "The Santénathon collective continues the fight against the illegal transfer of our health data to the United States."
ref: communique-presse-refere-privacy-shield-le-collectif-santenathon-continue
lang: en
show_comments: true
---

A group comprising the CNLL, the InterHop association, the Constances association and several doctors' and patients' unions - 18 claimants in all - [had asked the Conseil d'Etat](https://interhop.org/en/2020/09/16/communique-presse-refere-privacy-shield) (Council of State) to suspend the processing and centralization of our data within the Health Data Hub hosted by Microsoft. In doing so, the plaintiffs asked the Council of State to comply with the latest European case law.

This referral followed the decision ("Schrems II" ruling) of the Court of Justice of the European Union (CJEU) which had decided to cancel the "Privacy Shield", an agreement that allowed companies to legally transfer personal data from Europeans to the United States.

The CJEU had also argued that the Standard Contractual Clauses (SCC) were not sufficient. Indeed, US surveillance programs do not have any limitations on the authorization and use of data from non-US persons. This is simply not in line with European law and our protective regulation, the General Data Protection Regulation (GDPR).

**On this basis, any processing of personal data of European citizens in the United States must today be considered illegal without delay**.

<!-- more -->

However, in a summary judgment of September 21, 2020, the Council of State ruled that the collective's request was not urgent and that they had to act through a normal procedure. 

While regretting that the Council of State thus refuses to play its role as guardian of the liberties dear to our Republic, the claimers take note of this decision and will henceforth file the same petition, but on the merits.

Pending this decision, which may take several years, **the petitioners request the immediate implementation of a moratorium on the Health Data Hub** as long as they cannot be assured that no health data will be transferred to the United States, outside of any adequate protection or guarantee for French citizens.

At the same time, and in light of the reservations of the Conseil d'Etat, **the plaintiffs are filing a complaint with the CNIL regarding the illegal transfer of our health data hosted on the Health Data Hub, which will eventually integrate the data of all, i.e. more than 67 million people**.


List of 18 claimants :
- **The association Le Conseil National du Logiciel Libre (CNLL)** : "*So that the discourse on digital sovereignty does not remain empty words, strategic projects on the economic level and sensitive in terms of personal freedoms should not be entrusted to operators subject to jurisdictions incompatible with these principles, but to European actors who present serious guarantees on these subjects, including through the use of open and transparent technologies.*"
- **The Ploss Auvergne-Rhône-Alpes association**.
- **The SoLibre association**
- **The company NEXEDI**: "*It is wrong to say that there was no European solution. On the other hand, it is true that the Health Data Hub never responded to the providers of these solutions.*»
- **The National Union of Journalists (SNJ)** : " *For the National Union of Journalists (SNJ), the leading organization of the profession, these actions must make it possible to maintain the secrecy of health data of French citizens and protect the secrecy of journalists' sources, the main guarantee of independent information.*"
- **The Observatory for Transparency in Medicines Policies**
- **The InterHop association**: "*The cancellation of the Privacy Shield sounds the end of European digital naivety. However, a power struggle is taking place between the United States and the European Union concerning the transfer of personal data outside of our legal space.*
*In order to ensure the sustainability of our mutual healthcare system and in view of the sensitivity of the data involved, the hosting and services of the Health Data Hub must fall under the exclusive jurisdiction of the European Union.*"
- **The Federal Union of Doctors, Engineers, Executives, Technicians (UFMICT-CGT)**
- **L'Union Générale des Ingénieurs, Cadres et Techniciens (UGICT-CGT)**: "*For the CGT des cadres et professions intermédiaires (UGICT-CGT), this recourse is essential to preserve the confidentiality of data which has now become, in all fields, a market. As designers and users of technology, we refuse to allow ourselves to be dispossessed of the debate on digital technology on the pretext that it is technical. Only democratic debate will make it possible to place technological progress at the service of human progress!*"
- **The association Constances** : "*Volunteers of Constances, the largest health cohort in France, we are particularly aware of health data and their interest for research and public health. How can it be accepted that data from French citizens is now being transferred to the United States? How can we accept that, in the long term, all the health data of the 67 million French citizens will be hosted by Microsoft and therefore fall under American laws and surveillance programs?*"
- **The French Association of Hemophiliacs (AFH)**
- **The association "Actupiennes"**.
- **The National Union of Young General Practitioners (SNJMG)** : "*Data from care must not be used for any other purpose than to improve care. Guaranteeing the security of health data and their use for public health purposes alone is a priority for all health care providers."
- **The General Medicine Union (SMG)**: "*The security of health data is a major public health issue because it allows medical confidentiality. The Health Data Hub has so far shown no guarantee that the French people's health data will be truly secure, in particular because it has chosen to host this data at Microsoft, thus jeopardizing the medical secrecy that is so necessary for a healthy and efficient therapeutic relationship."*
- **The French Union for Free Medicine (UFML)** : "*Let's avoid the control of monopolistic systems potentially harmful to the health system and to citizens".*
- **Ms. Marie Citrini, in her capacity as User Representative on the Paris Hospitals Supervisory Board**"
- **Mr Bernard Fallery, Professor Emeritus in Information Systems**: *"The 'emergency' management claimed for the Healh Data Hub is a veritable textbook case of all the risks linked to the governance of massive data: digital sovereignty and storage without a specific purpose, but also risky technical centralization, a stranglehold on a digital commons, the oligopoly of the GAFAMs, the dangers of medical secrecy, a grid of traces and adjustments to behavior.*"
- **Mr Didier Sicard, doctor and professor of medicine at the University of Paris Descartes**: *"Offering Microsoft French health data that is among the best in the world, even if it is insufficiently exploited, is a fourfold mistake: enriching Microsoft for free, betraying Europe and French citizens, preventing French companies from participating in the data analysis".*
