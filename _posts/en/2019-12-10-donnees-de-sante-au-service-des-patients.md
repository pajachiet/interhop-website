---
layout: post
title: "For health data helping patients"
redirect_from:
  - /donnees-de-sante-au-service-des-patients_en/
ref: tribune_monde
show_comments: true
lang: en
---

[Sign this text](https://forms.interhop.org/node/3).

> While the government plans to rely on the American giant to store the
health data, a [collective](https://pad.interhop.org/s/Sk7K8qpaS#) initiated by professionals in the sector and computer science medical community is concerned, in a forum [at "Le Monde"](https://www.lemonde.fr/idees/article/2019/12/10/l-exploitation-de-donnees-de-sante-sur-une-plate-forme-de-microsoft-expose-a-des-risques-multiples_6022274_3232.html), about this choice of the private.

The French government is proposing the deployment of a platform named [Health Data Hub](https://solidarites-sante.gouv.fr/actualites/presse/communiques-de-presse/article/communique-de-presse-agnes-buzyn-health-data-hub-officiellement-cree-lundi-2) (HDH) to develop artificial intelligence applied to health. The HDH aims to become a single-window access point to all health data.

The data concerned are those of the hospital centres, the pharmacies, shared medical record and research data from various registers. The amount of data hosted is brought to explode, particularly with the emergence of genomics, imaging and connected objects. It is planned that this data will be stored at [Microsoft Azure](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640), the public cloud of the American giant Microsoft. This choice is at the centre of our concerns.

<!-- more -->

The GAFAM (Google, Apple, Facebook, Amazon and Microsoft), the start-ups and even [insurers](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante?onglet=full) could access health data and the financial power they represent, if these companies demonstrate that their research projects may have a use for ["public interest"](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id), a concept relatively fuzzy.


In addition, the use of Microsoft is governed by licences paid for. Although discussions are being held to ensure the reversibility of the American platform, it seems difficult to change. We are aware of the risks of digital captivity, with in particular the contracts between [Microsoft and hospitals](https://www.nextinpact.com/news/96401-le-contrat-a-plus-120-millions-d-euros-entre-microsoft-irlande-et-hopitaux-francais.htm).


## A breach of doctor-patient confidentiality? 

The U.S. government adopted in 2018 a text called the [Cloud Act](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf), that allows U.S. justice officials access to stored data in third countries. The President of the National Commission of l'informatique et des libertés (CNIL) said in September, to the [National Assembly](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543) that this text is contrary to the [General Regulation on Data Protection](http://www.privacy-regulation.eu/fr/48.htm) (RGPD), which protects citizens Europeans. Concretely, patients could be subjected to a Breach of medical confidentiality, which constitutes such a personal danger, the integrity of the Hippocratic Oath being restored to its symbolic value.

In addition, the HDH is developing on a centralized model, with the aim to consequence a higher impact in case of hacking. On might think that the GAFAMs offer ultra-secure solutions. This argument does not hold up. Indeed, attacks often come from inside, i.e. [personnel with access to the data](https://hbr.org/2016/09/the-biggest-cybersecurity-threats-are-inside-your-company).

Although the data hosted by the HDH is de-identified, complete anonymity is impossible, because it is enough to cross-reference a number limited data for [re-identifying a patient](https://www.nature.com/articles/s41467-019-10933-3.pdf). In addition, the medico-administrative data from the National Health Data System (SNDS), integrated into the HDH, has been criticized by the CNIL for the [obsolescence of its encryption algorithm](https://www.silicon.fr/snds-cnil-tousse-securite-grand-fichier-sante-166527.html).

The trust that constitutes the care relationship between patients and caregivers is based on multiple factors, including secrecy, which is essential. According to a recent survey, the hospital is even the institution in which the French have the most [confidence](https://www.egora.fr/actus-pro/hopitaux-cliniques/45857-confiance-des-francais-l-hopital-champion-des-institutions). What would be the impact of a loss of confidence if massive data leaks were to occur?

## There are alternatives

We are convinced of the value of data research and the development of statistical tools in medicine. However, there are alternatives that protect privacy and medical confidentiality, by guaranteeing the independence and collective control of infrastructure.

For several years now, hospitals have been creating data warehouses with the objective of collecting locally generated health data for analyze them. An effort is being made to promote decentralization and exchange between regions and our [European neighbours](https://www.ehden.eu/), while preserving data security.

Researchers and hospital centres have expertise in important, because they produce and collect data with, for objective, an evolution towards digital hospitals. Thus, the development of new technologies in hospitals will strengthen the interconnection between care and research.

The European Organization for Nuclear Research (CERN) has recently launched the [Malt](https://home.cern/fr/news/news/computing/migrating-open-source-technologies) project, for Microsoft Alternatives, which aims to replace as much commercial software as possible with free/libre software. We could follow this example and promote self-directed clouds.

## Promote decentralization

Decentralisation combined with system interoperability information and [federated learning](https://www.substra.ai/fr/accueil) (as opposed to the centralised) helps to promote networked research by preserving, on the one hand, the confidentiality of the data, on the other hand, the their storage.

This technique allows the algorithms to travel in each partner center without centralizing the data. Decentralization maintains local skills (engineers, caretakers) required to the qualification of Cheers.

The exploitation of health data on a  "proprietary" platform, such as Microsoft's, exposes the company to risks multiple. The Cloud Act-RGPD incompatibility, the digital autonomy of Europe and the possible loss of patient confidence are important issues to put at the centre of the debate citizen.

> It is essential to keep a firm grip on the technologies used.
and prevent the privatization of health care."

As the [National Council of the Medical Association](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf) did, we let us reaffirm a fundamental principle: "Let us act so that the France and Europe are not vassalized by the giants supranational digital". Health data is both a patients and the inalienable patrimony of the community. It is essential to keep control of technology and to prevent the privatization of the health.

# Signatories

The complete list of [signatories](https://pad.interhop.org/s/Sk7K8qpaS#).

[Sign this text](https://forms.interhop.org/node/3).
