---
layout: post
title: "Journées InterCHU - OMOP France"
ref: journee-interchu 
lang: en
---

# Utopia

#### Let's share

Solidarity, sharing and mutual aid between the different actors are the central values in health care. Just as the Internet is a common good, medical informatics knowledge must be available and accessible to all. We therefore want to promote the special ethical dimension that the opening up of innovation in the medical field creates and we want to take active measures to prevent the privatization of medicine.

<!-- more -->

#### Let's interoperate

The interoperability of computerized systems is the driving force behind the sharing of knowledge and skills and the means to combat technological entrapment. In health, interoperability is the guarantee of research reproducibility, sharing and comparison of practices for efficient and transparent research. There can be no interoperability without community.

# Overview

The InterCHU days are preferably addressed to French-speaking people (public sector engineers, ...) using the OMOP model or wanting to use it in the coming months.

Within the InterCHU meetings, we want :
1. Make progress reports on the various transformations carried out in the hospital centers. We have code and experience to share!
2. Make an inventory of the algorithms already implemented with OMOP.
3. Talk about possible study designs to use these algorithms in our hospitals in the near future.
4. To share the work of future developments between hospitals.
5. To discuss the harmonization of terminologies in France.
Within InterHop.org, we continue the development of SUSANA, a terminology alignment and collaborative software.
6. Talk about the development of BigData opensource and ubiquitous architectures for healthcare.
We are developing within InterHop an easily deployable BigData infrastructure (Ansible, GreenPlum, Jupyter...). Here is a link to this platform :
 [https://framagit.org/interhop](https://framagit.org/interhop){:target="_blank"}

# First meeting

#### When?

Thursday, January 21, 2020 - 14h-18h

#### Program

<img src="https://i.ibb.co/QHYbhpB/Screenshot-2021-01-08-at-17-41-01.png" alt="Screenshot-2021-01-08-at-17-41-01" border="0">

#### Where?

We use the video conferencing solution BigBlueButton, opensource and protective of personal data because it is hosted by Octopuce, a French company (with servers in France) : [https://www.octopuce.fr/mentions-legales/](https://www.octopuce.fr/mentions-legales/){:target="_blank"}
