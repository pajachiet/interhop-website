---
layout: post
title: "Goupile or how to design forms"
ref: goupile-concevoir-formulaires
lang: en
---

# An easy and user-friendly eCRF...

No, Goupile is not a fox!

Goupile is a form editor for research data collection. It is a web application that can be used on computer, mobile and offline.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://peertube.interhop.org/videos/embed/3cf50015-e374-4998-98ae-c28c6976db1d" frameborder="0" allowfullscreen></iframe>

<!-- more -->

The name Goupile has no particular meaning. When Niels Martignène created the project folder, he needed a name. He asked for help from his partner, Apolline, a great lover of foxes... And the name "goupil" came out by itself. 

After a few months, the "e" was added to facilitate the search on Google and to have a domain name available :)

Traditionally, the Case Report Form (CRF) is a paper document designed to collect data from people included in a research project.
These documents are increasingly being replaced or supplemented by digital portals, and are referred to as eCRFs (''e'' for *electronic*).

![](https://goupile.fr/static/screenshots/overview.webp) 

Goupile was born because data are still often collected in Excel files, and stored on researchers' personal machines. Others use online tools, like "Google Sheets" or "Office 365". The data is then stored at GAFAMs, on servers not adapted to health data.

There is also software specifically developed for health. These tools offer a drag and drop approach and/or a metalanguage, from a palette of input fields (e.g. date, number, text fields). This approach is easy and intuitive, but it limits the development possibilities. Moreover, these tools are often cumbersome to set up, and are generally used in the context of funded studies.

Goupile is a free and opensource eCRF design tool that strives to make form creation and data entry both powerful and easy. Its access is nominative and protected.

All Goupile pages are *responsive*, which means they are suitable for mobile, tablet and desktop browsing.

Goupile has an offline mode in case of network outage. The data is synchronized when the connection is available again.

Today, the "Goupile" project is supported and developed by the association InterHop.org, which is the publisher of the free software Goupile. Niels Martignène is the creator and the main developer.

InterHop's team is supporting Goupile on HDS certified servers (Health Data Hosts), to accompany users in the development of their forms and new features. Finally, InterHop also offers support for questions related to the protection and the General Data Protection Regulation (GDPR).

Goupile is used by researchers for prospective, retrospective, mono-centric or multicentric studies.

Goupile is also used by health students for their thesis and dissertation projects which generally require data collection. These relatively simple technical projects are our immediate target for several reasons: they do not need very complex functionalities, the number of theses (per year) is several hundreds and the proposed alternatives are neither free nor secured. 

InterHop members use Goupile daily to develop their own forms, which are tested and used by their close collaborators (and friends).

**Let's talk about Goupile's functionalities...**

Goupile allows you to design an eCRF with an approach that is a little different from the usual tools, since it consists in programming the content of the forms, while automating the other aspects common to all eCRFs: 
- Pre-programmed and flexible field types, 
- Publication of forms,
- Data storage and synchronization,
- Online and offline collection, on computer, tablet or mobile,
- User management and rights.

In addition to the usual functionalities, we have tried to reduce as much as possible the time between the development of a form and the data entry.

With Goupile, I code on the left, I see the result on the right.

Changes made in the editor are immediately executed and the preview panel on the right displays the resulting form. This is the same page that will be presented to the end user. At this point, it is already possible to enter data to test the form, but also to view and export it.

![](https://goupile.fr/static/screenshots/editor.webp)


Once the form is ready, the tracking panels allow you to observe the progress of the data entry, view the records and export the data.

![](https://goupile.fr/static/screenshots/data.webp)

**Goupile has many advantages.

Goupile is not based on a metalanguage (contrary to the classic tools in this field) but uses the Javascript language. It is a very well known programming language. 
The fact that it is programmed gives a lot of possibilities, in particular the realization of complex forms, without sacrificing the simplicity for the usual forms. Thus, there are very few limits to the development of new input fields or features.

We want to reassure you about the complexity. You can start creating your form without any programming knowledge. Indeed, Goupile provides a set of predefined functions that allow you to create input fields (text field, numeric field, dropdown list, visual scale, date, etc.) very simply.

![](https://goupile.fr/static/screenshots/instant.png)

It is possible to test the tool on a [demo instance](https://goupile.fr/demo) proposed by the InterHop association.
Be careful ! The demo instance is not certified for health: only fictitious data can be collected.

Finally, as Goupile is a free software, two possibilities are offered to the users: 
- Use and install Goupile locally in their center. Indeed, Goupile is delivered under a free license: AGPL 3.0 license. The source code is available for free online [here](https://framagit.org/interhop/goupilemagit.org/interhop/goupile){:target="_blank"}. In this case they have to manage all the hosting aspects.
- Use the turnkey service provided by the InterHop association via the servers rented from our health data host, [GPLExpert](https://gplexpert.com/hebergement-donnees-sante-hds/){:target="_blank"}.

In any case, users can ask us to develop new features.
