---
layout: post
title: "Press release: letter to the CNIL"
ref: communique-presse-cnil-pseudonymisation-chiffrement
lang: en
---

Sending a letter to the Commission Nationale de l'Informatique et des Libertés CNIL following the order of the Conseil d'Etat of 19 June.

<!-- more -->

# Subject

Request for an answer regarding the quality of the encryption and pseudonymisation processes within the "Health Data Platform" or "HealthDataHub".

# Content of the letter

Madam President,

The deployment of a "Health Data Platform" through the creation of a Public Interest Grouping, was proclaimed by law n° 2019-774 of July 24, 2019 relating to the organization and transformation of the health system[^loisante].
This "Health Data Platform" aims to develop artificial intelligence in the health sector and to become the one-stop shop for access to all health data on the national territory.
The data concerned are those from hospitals, pharmacies, shared medical records and research data from various registers. The amount of data hosted is set to explode with the emergence of genomics, imaging and connected objects.

Currently this data is stored at Microsoft Azure[^senat_microsoft], the public cloud of the American giant Microsoft. This choice has been criticized by many public[^lemonde] [^lenouvelobs] [^theconversation] and private[^lepoint] actors.

Opinion No 2020-044 of 20 April 2020 of your Commission[^CNILdel] refers to the risks of data transfers to third countries and unauthorised disclosures under EU law in the context of the subcontracting of the technical solution of the "platform" to Microsoft Azure.

Following this opinion, on 19 June 2020, the Council of State enjoined the "Platform for Health Data" to inform citizens of the "possible transfer of data outside the European Union, taking into account the contract with its subcontractor"[^conseil_etat]. **We note that this information is very difficult to access on the website health-data-hub.fr since it is only visible in the "Projects" section, in project number 3173 "Exploitation of data from emergency room visits for the analysis of the use of health care and the follow-up of the health crisis of Covid-19".**

The "Health Data Platform" was also to provide "all elements relating to the pseudonymisation procedures used, in order to enable [your Commission] to verify that the measures taken ensure sufficient protection of health data"[^conseil_etat]. The CNIL's position had been requested, especially since the **obsolescence of the encryption system** (named FOIN) had already been criticized by the Court of Auditors and the National Agency for the Security of Information Systems ANSSI in 2016[^cours_comptes].

We also warn about the quality of the encryption since "to benefit from all the capacities of the technical solution of the host these keys will have to be entrusted to him"[^CNILdel].

Finally, the CNIL and the Conseil d'État recalled that at the end of the state of health emergency the **all the data collected had to be deleted and that the processing no longer had a legal basis**. However, the decree of 10 July 2020 "prescribing the general measures necessary to deal with the covid-19 epidemic in the territories that have emerged from the state of public health emergency and in those where it has been extended" **extends the use of these data until 30 October 2020**[^arrete_sortie_etat_urgence].

Given the particularly large number of people concerned (more than 67 million users) and the sensitive nature of the personal data contained in the "Health Data Platform", we have decided to make this letter public.
This publicity contributes to the objective of transparency defended by your Commission[^stopcovid_cnil].

Pending your reply, please accept, Madam President, the assurances of our highest consideration.

Association interhop.org

[^stopcovid_cnil]: [StopCovid application: the CNIL draws the consequences of its controls](https://www.cnil.fr/fr/application-stopcovid-la-cnil-tire-les-consequences-de-ses-controles)

[^arrete_sortie_etat_urgence]: [Order of 10 July 2020 prescribing the general measures necessary to deal with the covid-19 epidemic in the territories that have emerged from the state of public health emergency and in those where it has been extended](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042106233&dateTexte=&categorieLien=id)

[^cours_comptes]: [HEALTH PERSONAL DATA MANAGED BY HEALTH INSURANCE](https://www.ccomptes.fr/sites/default/files/EzPublish/20160503-donnees-personnelles-sante-gerees-assurance-maladie.pdf)

[^senat_appeloffre]: [Health data protection: MORIN-DESAILLY Catherine](http://videos.senat.fr/video.1710660_5f10400c7efbf.seance-publique-du-16-juillet-2020-apres-midi?timecode=16471000)

[^reunin_stopcovid]: [StopCovid application press conference, June 23rd](https://www.economie.gouv.fr/direct-video-conference-presse-sur-application-stopcovid-23-juin#)

[^senat_microsoft]: [How to store the "health data hub"](http://www.senat.fr/basile/visio.do?id=qSEQ200114130&idtable=q371194%7Cq370883%7Cq370105%7Cq369641%7Cq368627%7Cq371617%7Cq371538%7Cq371754%7Cq371099%7Cq371232&_c=recherche&rch=qs&de=20170205&au=20200205&dp=3+ans&radio=dp&aff=sep&tri=dd&off=0&afd=ppr&afd=ppl&afd=pjl&afd=cvn)

[^CNILdel]: [Measures of organization and functioning of the health system necessary to deal with the covid-19 epidemic in the context of the state of health emergency ](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf)

[^loisante]: [LAW No. 2019-774 of 24 July 2019 on the organisation and transformation of the health system](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id)

[^theconversation]: [The Conversation - Health Data: the StopCovid tree that hides the Health Data Hub forest ](https://theconversation.com/donnees-de-sante-larbre-stopcovid-qui-cache-la-foret-health-data-hub-138852)

[^lenouvelobs]: [Le Nouvel Obs - Nos données de santé à Microsoft ? "We offer Americans a national wealth unique in the world"](https://www.nouvelobs.com/sante/20200623.OBS30391/nos-donnees-de-sante-a-microsoft-on-offre-aux-americains-une-richesse-nationale-unique-au-monde.html)

[^lemonde]: [Le Monde - " La politique publique des données de santé est à réinventer "](https://www.lemonde.fr/idees/article/2020/06/04/la-politique-publique-des-donnees-de-sante-est-a-reinventer_6041706_3232.html)

[^lepoint]: [Le Point - Health Data Hub: "The choice of Microsoft, an industrial misunderstanding! "](https://amp.lepoint.fr/2379394?utm_term=Autofeed&utm_medium=Social&utm_source=Twitter&Echobox=1591814194&__twitter_impression=true)

[^conseil_etat]: [Council of State, 19 June 2020, Health Data Hub Platform](https://www.conseil-etat.fr/ressources/decisions-contentieuses/dernieres-decisions-importantes/conseil-d-etat-19-juin-2020-plateforme-health-data-hub)
