# Jekyll theme 

Urban theme from [CloudCannon Academy](https://learn.cloudcannon.com/).
- github : https://github.com/CloudCannon/urban-jekyll-template 
- Live Demo : https://teal-worm.cloudvent.net/

## Usage

[Jekyll](https://jekyllrb.com/) version 3.3.1

Install the dependencies with [Bundler](https://bundler.io/):

~~~bash
$ bundle install
~~~

Run `jekyll` commands through Bundler to ensure you're using the right versions:

~~~bash
$ bundle exec jekyll serve
~~~

https://css-tricks.com/separate-form-submit-buttons-go-different-urls/
http://www.fizerkhan.com/blog/posts/Working-with-upcoming-posts-in-Jekyll.html


# More 

## Jekyll
- Cheatsheet : https://devhints.io/jekyll
- Mutltilingual : https://www.sylvaindurand.org/making-jekyll-multilingual/
- Scope : https://jekyllrb.com/docs/configuration/front-matter-defaults/
- Permalink and Slug : https://jekyllrb.com/docs/permalinks/
- redirect Module : https://github.com/jekyll/jekyll-redirect-from
- Collections : https://jekyllrb.com/docs/collections/

## Pagination Multi Language
- https://webniyom.com/jekyll-dual-language/
- example : https://github.com/bradonomics/jekyll-dual-language
 
## Online svg converter
- https://image.online-convert.com/fr/convertir-en-svg
- https://www.pngtosvg.com

## Online pixel converter
- http://convert-my-image.com/ImageConverter_Fr

## Free icons
- https://www.flaticon.com
- https://www.iconfinder.com/

## gitlab pages deploiment
- https://framacolibri.org/t/gitlab-page-explication-derreur-pipeline/5852/5
- https://docs.framasoft.org/fr/gitlab/gitlab-pages.html
- https://blog.cloudflare.com/secure-and-fast-github-pages-with-cloudflare/
