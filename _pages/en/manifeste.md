---
layout: presentation
title: Manifesto
permalink: en/manifeste/
ref: manifeste
lang: en
---

## Let's decentralize

The data centralization logic is En Marche. In the health sector, the limits and excesses of such approaches are no longer to be shown.
**interhop.org** offers an alternative to avoid data collection within the United States GAFAM (Google, Apple, Facebook, Amazon, Microsoft) or the Chinese BATX (Baidu, Alibaba, Tencent, Xiaomi).
This approach is at the initiative of French hospitals.

## Let's de-Google-ify

As early as 2014, the  « Dégooglisons Internet »  project of the Framasoft <sup>[^framasoft]</sup> association showed by example that it was possible to bring together free and ethical skills and mechanisms in order to decentralize the Internet. The initiative led by a handful of members demonstrates that it is possible to help individuals and organizations build credible alternatives to digital giants with a decentralized and liberating approach -- as opposed to a centralized approach.

**interhop.org** takes up these ideas.
The association is committed to providing free and open online office services : instant message<sup>[^zulip]</sup>, word processing<sup>[^pad]</sup>, spreadsheets<sup>[^calc]</sup>... These services will be hosted, maintained and the installation procedures documented by members of the association.

## Let's share

Solidarity, sharing and mutual assistance between the different actors of **interhop.org** are the core values of the association. Just as the Internet is a common good, knowledge in medical informatics must be available and accessible to all.
**interhop.org** wants to promote the particular ethical dimension reflected in the openness of innovation in the medical field and wants to take active measures to prevent the privatization of medicine.

## Let's interoperate

The interoperability of computerized systems is the driving force behind the sharing of knowledge and skills and the means of combating technological imprisonment. In health, interoperability is the key to ensuring the reproducibility of research, the sharing and comparison of practices for effective and transparent research. The association will maintain a roadmap on interoperability and share it with government agencies such as ASIP Santé<sup>[^asip_sante]</sup>.
There can be no interoperability without a community.

## Let us free

In the health sector specifically, the National Council of the Order of Physicians<sup>[^cnom]</sup> has warned against the risk of vassalisation to digital giants.
**interhop.org** therefore promotes free research (arXiv<sup>[^arXiv]</sup>, bioRxiv<sup>[^bioRxiv]</sup>)), free terminologies (ICD11<sup>[^ICD11]</sup>, LOINC<sup>[^LOINC]</sup>, RxNorm<sup>[^RxNorm]</sup>), free data models (FHIR<sup>[^FHIR]</sup>, OMOP<sup>[^OMOP]</sup>), openData (data.gov<sup>[^data_gouv]</sup>, physionet<sup>[^physionet]</sup>), open data processing software (postgresql, python, R, spark, scikit-learn) and the free sharing of algorithms and software produced by members - for members.  Open standards are the exclusive means by which **interhop.org** members will work and showcase their work in the health field. The free must become the norm in health.

## Let's secure

Sharing and freedom are appreciated in a secure context.
In addition to the use of copyleft licenses<sup>[^copy-left]</sup> the association wants to help protect digital health productions. Particular concerns in terms of data anomalies, cyber-security and ethical and legal values of health data will be at the heart of the association's communications.


[^copy-left]: [Copyleft licenses](https://www.gnu.org/copyleft/)
[^asip_sante]: [ASIP Santé](https://esante.gouv.fr/interoperabilite)
[^zulip]: [Instant messaging](https://interchu.zulipchat.com)
[^framasoft]: [Framasoft](https://framasoft.org/)
[^pad]: [Word processing](http://pad.interhop.org)
[^calc]: [spreadsheets](http://calc.interhop.org)
[^cnom]: [National Council of the Order of Physicians](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)
[^arXiv]: [arXiv](https://arxiv.org/)
[^bioRxiv]: [bioRxiv](https://www.biorxiv.org/)
[^ICD11]: [ICD11](https://icd.who.int/en)
[^LOINC]: [LOINC](https://loinc.org/)
[^RxNorm]: [RxNorm](https://www.nlm.nih.gov/research/umls/rxnorm/index.html)
[^FHIR]:  [FHIR](https://www.hl7.org/fhir/)
[^OMOP]: [OMOP](https://www.ohdsi.org/)
[^data_gouv]: [data.gouv](https://www.data.gouv.fr/fr/)
[^physionet]: [physionet](https://physionet.org/)
