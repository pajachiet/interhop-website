---
layout: presentation
title: Join the association
subtitle: You want to participate in the construction of free / open source, decentralized and interoperable medical informatics ?
permalink: en/rejoins-nous/
ref: rejoins-nous
lang: en
---

<div class="container">

	<form id="comeForm" action="https://newsletter.api.interhop.org/ukiaqn/" method="post" class="contact-form">
		<div class="halves">
			<div class="form-group">
				<label class="checkbox-container">Are you a health professional ?
				  <input type="checkbox" name="pro-sante_en" id="pro-sante_en">
				  <span class="checkmark"></span>
				</label>
	            	</div>
	
			<div class="form-group">
				<label class="checkbox-container">Are you a computer engineer ?
				  <input type="checkbox" name="inge_en" id="inge_en">
				  <span class="checkmark"></span>
				</label>
	            	</div>
		</div>
	
		<div class="form-group">
			<label class="checkbox-container">Are you a student ?
			  <input type="checkbox" name="student_en" id="student_en">
			  <span class="checkmark"></span>
			</label>
	        </div>

		<div class="form-group">
			<label class="checkbox-container">Do you want to subscribe to our newsletter ?
			  <input type="checkbox" name="newsletter-subscribe-come_en" id="newsletter-subscribe-come_en">
			  <span class="checkmark"></span>
			</label>
	        </div>
		
	       <label for="email-come_en">Email Address : </label>
		<input type="email" name="email-come_en" id="email-come_en" placeholder="Email address" required />
	
	       <br />
	
		<input type="submit" value="Send"/>

		<p class="subtext editable">* Of course no personal information will be given to anyone !</p>
	</form>
	<br />
	<br />
	<br />

	<p class="subtext editable">
                We'll be in touch :-) <br />
		Of course it's not binding.
	</p>
</div>
