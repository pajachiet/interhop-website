---
layout: presentation
title: Donate
ref: dons
lang: en
permalink: en/dons/
---

# Why give?

To give for Interhop, is to refuse to see your health data used in ignorance of the European Data Protection Regulation, and [to assert your rights]({{ site.baseurl }}}//2020/11/23/the-reversibility-is-acquired).

To give for Interhop is to understand that health data is a [sensitive and precious matter]({{ site.baseurl }}}/2019/12/10/patient-service-data), which we must remain in control, and work to make this imperative before our public actors.

To give for Interhop is to refuse an excessive privatization of data covered by the [medical secrecy]({{ site.baseurl }}}/2020/05/27/covid-donnees-de-sante-microsoft), which cannot benefit to the interests of the GAFAM.

To give for Interhop is to promote and finance alternative, free, decentralized, certified, interoperable and secure tools.

Giving for Interhop is to disseminate popular education initiatives for patients and caregivers,

Giving for Interhop is to contribute to the emergence of ethical and decentralized health data hosts.

To give for Interhop is to want to act as a watchdog of democracy on health and digital issues.

Giving for Interhop is stylishhhhh !

Patients, caregivers, we are all concerned by the preservation of medical confidentiality.


# What are your donations for?

Your donations are essential to perpetuate the digital tools that we make available to everyone in an associative framework.

Your donations are essential to guarantee our independence.


#### Financing certified servers
Your donations will be used to finance **certified servers** "[Health Data Hosting](https://esante.gouv.fr/labels-certifications/hebergement-des-donnees-de-sante){:target="_blank"}" to install websites and open source software (instant messaging, video conferencing, bigdata analysis platform ...).

We are inspired by the Framasoft association and invite you to look at what [CHATONS.org](https://entraide.chatons.org){:target="_blank"} does outside of health.

#### Developing ethical tools in e-health
These donations will then be used for the **installation, development** of open source software in a secure enclave for health.

#### Changing the law and enforcing the law
InterHop will coordinate a strategy of **advocacy at national and European level** to defend your fundamental freedoms. Prospects for actions before the Court of Justice of the European Union and national jurisdictions are under consideration.

# How to give ?

### Hello Asso


<iframe id="haWidget" allowtransparency="true" scrolling="auto" src="https://www.helloasso.com/associations/interhop/formulaires/1/widget/en" style="width:100%;height:750px;border:none;" onload="window.scroll(0, this.offsetTop)"></iframe>

<div id="haWidgetMobile">
	<p>You will be transfered on HelloAsso.com.</p>
	<p><a class="button alt" style="text-align: center;" type="blue" href="https://www.helloasso.com/associations/interhop/formulaires/1/widget/en">Faire un don</a></p>
</div>

### Bank transfer

Transfers from bank to bank are possible free of charge: check with your bank!

Domiciliation : CREDIT COOPERATIF

#### Account identification for use in France

- Banque : 42559
- Guichet : 10000
- Compte : 08024330959
- Clé RIB : 32
- BIC : CCOPFRPPXXX

#### Account identification for international use (IBAN)

FR76 4255 9100 0008 0243 3095 932
