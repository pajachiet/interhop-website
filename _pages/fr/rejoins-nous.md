---
layout: presentation
title: Rejoins l'association
subtitle: Vous voulez participer à la construction d’une informatique médicale libre / opensource, décentralisée et interopérable ?
permalink: rejoins-nous/
ref: rejoins-nous
lang: fr
---

<div class="container">

	<form id="comeForm" action="https://newsletter.api.interhop.org/ukiaqn/" method="post" class="contact-form">
		<div class="halves">
			<div class="form-group">
				<label class="checkbox-container">Es-tu professionnel.le de santé ?
				  <input type="checkbox" name="pro-sante" id="pro-sante">
				  <span class="checkmark"></span>
				</label>
	            	</div>
	
			<div class="form-group">
				<label class="checkbox-container">Es-tu ingénieur.e en informatique ?
				  <input type="checkbox" name="inge" id="inge">
				  <span class="checkmark"></span>
				</label>
	            	</div>
		</div>
	
		<div class="form-group">
			<label class="checkbox-container">Es-tu étudiant.e ?
			  <input type="checkbox" name="student" id="student">
			  <span class="checkmark"></span>
			</label>
	        </div>

		<div class="form-group">
			<label class="checkbox-container">Veux-tu t'abonner à notre lettre d'information ?
			  <input type="checkbox" name="newsletter-subscribe-come" id="newsletter-subscribe-come">
			  <span class="checkmark"></span>
			</label>
	        </div>
	
		
	       <label for="email-come">Adresse E-mail : </label>
		<input type="email" name="email-come" id="email-come" placeholder="Adresse email" required />
	
	       <br />
	
		<input type="submit" value="Envoyer"/>
		<p class="subtext editable">* Aucune information personnelle ne sera pas cédée à qui que ce soit !</p>
	</form>
	<br />
	<br />
	<br />

	<p class="subtext editable">

		On te recontacte :-) <br />
		Bien sûr ça n'engage en rien.
	</p>
</div>
