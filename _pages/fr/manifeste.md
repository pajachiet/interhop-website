---
layout: presentation
title: Manifeste
permalink: manifeste/
ref: manifeste
lang: fr
---

## Décentralisons

La logique de centralisation des données est "En Marche". Dans le secteur de la santé, les limites et les dérives de telles démarches ne sont plus à démontrer.
**interhop.org** propose une alternative permettant d'éviter la collecte des données au sein des GAFAMs État-Uniens (Google, Apple, Facebook, Amazon, Microsoft), des BATX chinois (Baidu, Alibaba, Tencent, Xiaomi) ou d'autres grandes entreprises étrangères et européennes présentant des risques similaires.

## Dégooglisons

Dès 2014, le projet « Dégooglisons Internet » de l'association Framasoft<sup>[^framasoft]</sup> a montré par l'exemple, qu'il était possible de rassembler des compétences et des dispositifs libres et éthiques en vue de décentraliser Internet. L'initiative portée par une poignée de membres démontre qu'il est possible d'aider les individus ainsi que les organisations à construire des alternatives crédibles aux géants du numérique avec une approche décentralisée et libérante --- par opposition à l´approche centralisée.

**interhop.org** reprend à son compte ces idées.
L'association s'engage à fournir des services de bureautique en ligne libres et gratuits : messagerie instantanée<sup>[^zulip]</sup>, traitement de texte<sup>[^pad]</sup>, feuilles de calcul<sup>[^calc]</sup>... Ces services seront hébergés, maintenus et les modalités d´installation documentées par des membres de l'association.

## Partageons

La solidarité, le partage et l'entraide entre les différents acteurs d'**interhop.org** sont les valeurs centrales de l'association. Au même titre qu'Internet est un bien commun, le savoir en informatique médical doit être disponible et accessible à tous.
**interhop.org** veut promouvoir la dimension éthique particulière qu'engendre l'ouverture de l'innovation dans le domaine médical et veut prendre des mesures actives pour empêcher la privatisation de la médecine.

## Interopérons

L'interopérabilité des systèmes informatisés est le moteur du partage des connaissances et des compétences ainsi que le moyen de lutter contre l'emprisonnement technologique. En santé, l'interopérabilité est le gage de la reproductibilité de la recherche, du partage et de la comparaison des pratiques pour une recherche performante et transparente. L'association tiendra une feuille de route sur l´interopérabilité, et la partagera avec des organismes gouvernementaux tels que l'ASIP santé<sup>[^asip_sante]</sup>.
Il ne peut pas y avoir d'interopérabilité sans communauté.

## Libérons

Dans le milieu de la santé spécifiquement, le Conseil National de l'Ordre des Médecins<sup>[^cnom]</sup> a mis en garde contre le risque de vassalisation aux géants du numérique.
**interhop.org** promeut donc la recherche libre (arXiv<sup>[^arXiv]</sup>, bioRxiv<sup>[^bioRxiv]</sup>), les terminologies libres (ICD11<sup>[^ICD11]</sup>, LOINC<sup>[^LOINC]</sup>, RxNorm<sup>[^RxNorm]</sup>), les modèles de données libres (FHIR<sup>[^FHIR]</sup>, OMOP<sup>[^OMOP]</sup>), l'openData (data.gouv<sup>[^data_gouv]</sup>, physionet<sup>[^physionet]</sup>), les logiciels de traitement des données libres (postgresql, python, R, spark, scikit-learn) ainsi que le partage libre d'algorithmes et de logiciels produits par les membres --- pour les membres. Les standards ouverts sont les moyens exclusifs par lesquels les membres d'**interhop.org** travailleront et exposeront leurs travaux dans le milieu de la santé.
Le libre doit devenir la norme en santé.

## Sécurisons

Le partage et la liberté s´apprécient dans un contexte sécurisant.
Outre l´utilisation de licences copyleft<sup>[^copy-left]</sup>, l'association veut aider à la protection des productions numériques en santé. Les préoccupations particulières en termes d´anomymisation des données, de cyber-sécurité et de valeurs éthiques et juridiques des données de santé seront au centre des communications de l'association.

[^copy-left]: [Les licenses copyleft](https://www.gnu.org/copyleft/)
[^asip_sante]: [ASIP Santé](https://esante.gouv.fr/interoperabilite)
[^zulip]: [Messagerie instantanée](https://interchu.zulipchat.com)
[^framasoft]: [Framasoft](https://framasoft.org/)
[^pad]: [Traitement de texte](http://pad.interhop.org)
[^calc]: [Feuilles de calcul](http://calc.interhop.org)
[^cnom]: [Conseil National de l'ordre des Médecins](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf)
[^arXiv]: [arXiv](https://arxiv.org/)
[^bioRxiv]: [bioRxiv](https://www.biorxiv.org/)
[^ICD11]: [ICD11](https://icd.who.int/en)
[^LOINC]: [LOINC](https://loinc.org/)
[^RxNorm]: [RxNorm](https://www.nlm.nih.gov/research/umls/rxnorm/index.html)
[^FHIR]:  [FHIR](https://www.hl7.org/fhir/)
[^OMOP]: [OMOP](https://www.ohdsi.org/)
[^data_gouv]: [data.gouv](https://www.data.gouv.fr/fr/)
[^physionet]: [physionet](https://physionet.org/)
