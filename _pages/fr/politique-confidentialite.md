---
layout: presentation
title: Politique de confidentialité
permalink: politique-confidentialite/
ref: politique-confidentialite
lang: fr
---


# Introduction

L'association **InterHop** s'engage à ce que les traitements de données personnelles effectués sur [InterHop.org](https://interhop.org), [Toobib.org](https://toobib.org){:target="_blank"}  et [Goupile.fr](https://goupile.fr){:target="_blank"} (services d'InterHop) soient conformes au Règlement Général sur la Protection des Données (RGPD) et à la loi Informatique et Libertés et aux autres lois applicables.

Pour ce faire, **InterHop** s’efforce de mettre en place toutes les précautions nécessaires à la préservation de la confidentialité et à la sécurité des données à caractère personnel communiquées et traitées afin de faire obstacle notamment à leur altération, déformation, endommagement ou encore leur utilisation par des tiers.

Notre politique de confidentialité a donc pour objet de vous faire part des engagements d’**InterHop** en matière de collecte, de traitement et de transfert des données à caractère personnel et, plus largement, en matière de protection des données à caractère personnel.

Il est rappelé que la notion de données à caractère personnel (ci-après, intitulée les « données personnelles ») désigne toute information relative à une personne permettant de l’identifier directement ou indirectement, comme par exemple votre nom, votre numéro de téléphone, votre adresse IP ou encore votre adresse électronique.

# À qui s’adresse notre politique ?

Notre politique de confidentialité est destinée aux visiteurs des sites Internet [InterHop.org](https://interhop.org), [Toobib.org](https://toobib.org){:target="_blank"} , [Goupile.fr](https://goupile.fr){:target="_blank"}, aux adhérents, aux donateurs, aux prospects, aux clients, aux fournisseurs ou aux sous-traitants d’**InterHop**, aux employés, ou candidats pour une offre d’emploi au sein d’**InterHop** et, plus largement à toute personne dont les données personnelles seraient susceptibles d’être ou sont collectées, reçues, conservées, traitées, transférées et utilisées, quel que soit le format.

En utilisant notre site internet ainsi qu’en communiquant vos données personnelles à InterHop, vous reconnaissez avoir lu et compris la présente politique de confidentialité et consentez à ce que vos données personnelles soient utilisées et traitées selon les termes de la présente politique, dans le cadre de la législation en vigueur.

# Les données recueillies

### Les informations que vous transmettez à InterHop
En utilisant nos sites Internet, vous êtes amenés à nous transmettre des données personnelles permettant de vous identifier et/ou d’identifier votre organisation (société, association, établissement public…). Parmi ces informations figurent notamment votre identité, votre email, votre numéro de téléphone, votre identifiant, …

De plus, dans le cadre d’une relation contractuelle avec **InterHop**, divers documents sont nécessaires pour la bonne exécution de cette relation : adresse, lieu d’exécution de la prestation, nom de l’organisation, nom et prénom de l’interlocuteur, email, numéro de téléphone, numéro de SIREN, Numéro IC …


### Les données relatives aux réseaux sociaux

Dans le cas où vous vous connectez à nos services en utilisant les fonctionnalités de réseaux sociaux mises à votre disposition, **InterHop** aura accès à certaines de vos données telles que votre prénom, nom de famille, adresse email, de votre compte sur ledit réseau social conformément aux conditions générales d’utilisation du réseau social concerné.
NB : En utilisant les réseaux sociaux, vos données personnelles sont susceptibles d’être diffusées, utilisées par tous et partout, sans votre consentement préalable.

### Les données relatives aux Cookies

Le site [InterHop.org](https://interhop.org) et ses sites de services sont édités par l'association InterHop, et n'utilisent pas de cookies.<br />
Le site [Toobib.org](https://toobib.org){:target="_blank"} présente un service d'InterHop et est édité par l'association InterHop. InterHop n'utilise pas de cookies.<br />
Le site [Goupile.fr](https://goupile.fr){:target="_blank"} présente un service d'InterHop et est édité par l'association InterHop. InterHop n'utilise pas de cookies.<br />

Pour les dons (page : [interhop.org/dons](https://interhop.org/dons) notamment) InterHop utilise les widgets du site [HelloAsso.com](https://helloasso.com){:target="_blank"} qui contient des traceurs "Google Tag Manager".

### Les données de santé dites "sensibles"

Lorsque le responsable de traitement collecte des données de santé avec des logiciels fournis par **l'association InterHop** ces derniers sont hébergées chez **GPLExpert**, certifié Hébergeur de données de Santé sur les périmètres « Hébergeur d’infrastructure physique » et « Hébergeur infogéreur » (Art. R. 1111-11. – I ; 1 CSP).

Le Datacenter de GPLExpert est situé exclusivement en France.

# L’utilisation des données personnelles

Les données personnelles que vous transmettez à **InterHop** ou que **InterHop** collectent sont traitées notamment pour les finalités suivantes :
* L’exécution de nos obligations contractuelles respectives ;
* Création, gestion et maintien de sa relation d’affaires avec ses clients, prospects, fournisseurs et sous-traitants ;
* Fourniture de produits et de services, ainsi que leur gestion notamment administrative ;
* Traitement de la facturation et des paiements ;
* Prospection commerciale ;
* Études, analyses et statistiques marketing.

# Les destinataires des données personnelles

Les données personnelles récoltées par **InterHop** sont hébergées en France.

**InterHop** s’engage à ne pas transmettre vos données personnelles à une tierce partie sauf si vous avez donné votre accord préalable pour le partage de vos données personnelles avec des tiers, ou que le partage de vos données personnelles avec ladite tierce partie est nécessaire à la fourniture de produits ou services, ou qu’une autorité judiciaire ou administrative compétente exige à **InterHop** de lui communiquer lesdites données personnelles.

# Durée de conservation

Les données personnelles sont conservées pendant toute la durée nécessaire aux finalités pour lesquelles elles ont été communiquées à **InterHop** dans le respect de la réglementation applicable et sont, en tout état de cause, détruites à l’issue de celle-ci.
Pour plus d’informations concernant les durées de conservation, vous pouvez nous contacter à l’adresse mail suivante : <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>
 
# Transfert de vos données

Nous conservons vos données personnelles au sein de l’Union Européenne (France).

# Vos droits sur vos données personnelles

À tout moment, vous avez la faculté d’exercer l’un de vos droits en ce qui concerne vos données personnelles, à savoir :
* Votre droit d’accès ;
* Votre droit de rectification ;
* Votre droit d’opposition;
* Votre droit à l'effacement ;
* Votre droit à la portabilité ;
* Votre droit à la limitation du traitement ;
* Votre droit à la décision individuelle automatisée ;

### Pour exercer vos droits vous pouvez soit :

Pour toute information ou exercice de vos droits Informatique et Libertés sur les traitements de données personnelles gérés par **InterHop**, vous pouvez contacter son Délégué à la Protection des Données (DPO) :

* Envoyer un courrier à l’adresse suivante : **InterHop**,- A l’attention du Responsable DPD (DPO en anglais)
142 Rue du Faubourg Saint Martin 75010 PARIS
* Envoyer un courriel à l’adresse suivante : <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>

Veuillez cependant noter que les données personnelles relatives au nom de l’entité, l’adresse du siège social, l’adresse de livraison et les coordonnées de la personne s’occupant de la facturation sont obligatoires pour répondre à vos demandes et fournir les produits ou services demandés ; sans ces informations, **InterHop** ne pourra pas être en mesure de répondre à vos demandes et fournir les produits ou services demandés.

# Modification de notre politique de confidentialité

**InterHop** peut être amené à modifier sa politique de confidentialité. Nous veillerons à ce que vous en soyez informés avant son entrée en vigueur par une mention spéciale sur notre site internet et/ou par courrier électronique.

---

Vous reconnaissez avoir lu et compris la présente politique et consentez à ce que vos données personnelles soient utilisées et traitées selon les termes de la présente politique de confidentialité.
A défaut, vous vous engagez à ne plus :
* Naviguer sur le site internet de **InterHop**;
* Utiliser les services en ligne de **InterHop**;
* Communiquer vos données personnelles à **InterHop** par tous moyens ;
A contrario, **InterHop** ne pourra pas être en mesure de répondre à vos demandes et fournir les produits ou services demandés.
