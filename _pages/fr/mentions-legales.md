---
layout: presentation
title: Mentions légales
permalink: mentions-legales/
ref: mentions-legales
lang: fr
---

Le site [InterHop.org](https://interhop.org) et ses sites de services sont édités par l'association InterHop, et n'utilisent pas de cookies.<br />
Le site [Toobib.org](https://toobib.org){:target="_blank"} présente un service d'InterHop et est édité par l'association InterHop. InterHop n'utilise pas de cookies.<br />
Le site [Goupile.fr](https://goupile.fr){:target="_blank"} présente un service d'InterHop et est édité par l'association InterHop. InterHop n'utilise pas de cookies.<br />

Pour les dons (page : [interhop.org/dons](https://interhop.org/dons) notamment) InterHop utilise les widgets du site [HelloAsso.com](https://helloasso.com){:target="_blank"} qui contient des traceurs "Google Tag Manager".

# Éditeur : Association InterHop

### Code entreprises
- SIREN : 885 179 655 
- SIRET : 885 179 655 00018
- NAF (Nomenclature d'Activités Française) :  6311Z
- RNA (Répertoire National des Association) : W751256942
- Numéro TVA IC : FR47885179655

### Nous retrouver
- Siège social : 142 Rue du Faubourg Saint Martin  75010 PARIS 
- Site Web : [interhop.org/nous](https://interhop.org/nous/) (ou [goupile.fr](https://goupile.fr/){:target="_blank"})
- Courriel : interhop@riseup.net (ou hello@goupile.fr)

### Équipe
- Directeur de la publication : Adrien PARROT, Président
- Co-directeur de la publication : Nicolas PARIS, Ingénieur BigData, Secrétaire
- Personne en charge des demandes de suppression des inscriptions à la mailing liste : <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>

# Hébergeur des sites

- Le site web de [InterHop.org](https://interhop.org) est hébergé chez Framagit
- Le site web de [Toobib.org](https://toobib.org){:target="_blank"} est hébergé chez Framagit
- Le site web de [Goupile.fr](https://goupile.fr){:target="_blank"} est hébergé chez OVH (VPS)

# Informations personnelles

Conformément aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, et au Règlement Général sur la Protection des données,  aucune information personnelle n’est collectée à votre insu ou cédée à des tiers. 
Vous disposez d’un droit d’accès, de rectification et d’opposition aux données vous concernant que vous pouvez exercer en contactant la Déléguée à la Protection des Données (DPD). 
Pour cela, il vous suffit d’envoyer un courriel à la Déléguée à la Protection des Données d'InterHop à <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a> ou un courrier par voie postale à Association InterHop 142 Rue du Faubourg Saint Martin  75010 PARIS, en justifiant de votre identité.

# Demandes de suppression des inscriptions à la mailing liste

- Personne responsable : Déléguée à la Protection des données @ : <a href="mailto:dpo-interhop@riseup.net">dpo-interhop@riseup.net</a>

# Utilisation d’images et d’icônes

- Icônes : [flaticon.com](https://www.flaticon.com){:target="_blank"}, [iconfinder.com](https://www.iconfinder.com){:target="_blank"}
- Logo InterHop : Merci à Miguel Angel Armengol de la Hoz
- Logo Toobib : Merci à Quentin PARROT
- Logo Goupile : Merci à Niels MARTIGNENE

# Thèmes [Jekyll](https://jekyllrb.com/){:target="_blank"} utilisés
- Pour [InterHop.org](https://interhop.org), merci à [CloudCannon](https://github.com/CloudCannon/urban-jekyll-template){:target="_blank"}
- Pour [Toobib.org](https://toobib.org){:target="_blank"}, merci à [OpenDataHub](https://gitlab.com/opendatahub){:target="_blank"}
