---
layout: presentation
title: Dons
permalink: dons/
ref: dons
lang: fr
---

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/interhop/formulaires/1/widget-bouton" style="width:100%;height:70px;border:none;"></iframe>

# Pourquoi donner ? 

C'est promouvoir et financer des outils alternatifs, libres, décentralisés, certifiés, interopérables et sécurisés.

Donner pour InterHop, c'est diffuser des initiatives d'éducation populaire pour que patients et soignants,

C'est contribuer à l'émergence d'hébergeurs de données de santé éthiques et décentralisés.

C'est vouloir agir, en tant que vigie de la démocratie sur les enjeux de santé et numérique


Donner pour InterHop c'est comprendre que les données de santé sont une [matière sensible et précieuse]({{ site.baseurl }}/2019/12/10/donnees-de-sante-au-service-des-patients), dont nous devons rester maitre.sse.s, et travailler à rendre cela impératif devants nos acteurs publiques.

Pour refuser de voir nos données de santé utilisées en méconnaissance du Règlement Européen de Protection des Données, et [faire valoir nos droits]({{ site.baseurl }}//2020/11/23/la-reversibilite-est-acquise).

C'est refuser une privatisatisation à outrance de données couvertes par le [secret médical]({{ site.baseurl }}/2020/05/27/covid-donnees-de-sante-microsoft), qui ne peuvent profiter aux intérêts des GAFAM.

Donner pour InterHopHopHop, c'est stylééééé !

Patients.es, soignants.es, nous sommes tous concernés.es par la préservation du secret médical.


# A quoi servent tes dons ?

Tes dons sont essentiels pour pérenniser les outils numériques que nous mettons à disposition de tou.te.s dans un cadre associatif.

Tes dons sont essentiels pour garantir notre indépendance.


#### Financer des serveurs certifiés
Tes dons serviront à financer des **serveurs certifiés** “Hébergeurs de Données de Santé" pour installer des [sites web et des logiciels libres / opensources pour la santé]({{site.baseurl}}/projets/esante) : messagerie instantanée, vidéoconférence, plateforme d'analyse données massives...)

Nous nous inspirons de l'association Framasoft et t'invitons à regarder ce que font les [CHATONS.org](https://entraide.chatons.org){:target="_blank"} en dehors de la santé.

#### Développer des outils éthiques en e-santé
Ces dons serviront ensuite à **[l'installation, au développement]({{site.baseurl}}/projets/collaborons)** de logiciels libres dans une enclave sécurisée pour la santé.

#### Changer la loi et appliquer le droit
InterHop coordonnera une stratégie de **[plaidoyer aux niveaux national et européen]({{site.baseurl}}/projets/actions-en-jsutice)** pour défendre vos libertés fondamentales. Des perspectives d'actions devant la Cour de Justice de l'Union Européenne et de juridictions nationales sont à l'étude.

# Comment donner ?

### Via Hello Asso

<iframe id="haWidget" allowtransparency="true" scrolling="auto" src="https://www.helloasso.com/associations/interhop/formulaires/1/widget" style="width:100%;height:750px;border:none;" onload="window.scroll(0, this.offsetTop)"></iframe>


<div id="haWidgetMobile">
	<p>Tu vas être transféré.e sur HelloAsso.com.</p>
	<p><a class="button alt" style="text-align: center;" type="blue" href="https://www.helloasso.com/associations/interhop/collectes/defendez-une-vision-ethique-et-respectueuses-des-droits-numeriques-en-sante">Faire un don</a></p>
</div>

### Par virement bancaire

Des virements de banque à banque sont possibles sans frais :  renseigne-toi auprès de ta banque !

Domiciliation : CRÉDIT COOPÉRATIF

#### Identification du compte pour une utilisation nationale

- Banque : 42559
- Guichet : 10000
- Compte : 08024330959
- Clé RIB : 32
- BIC : CCOPFRPPXXX

#### Identification du compte pour une utilisation internationale (IBAN)

FR76 4255 9100 0008 0243 3095 932

