// https://fullcalendar.io/docs/event-object

{
  "title": 'Numérique et pandémie.',
  "start": '2021-06-11',
  "url": 'https://www.sciencesconf.org/browse/conference/?confid=11586',
},
{
  "title": 'Encryption Europe',
  "start": '2021-05-31',
  "url": 'https://www.encryptioneurope.eu/2021/05/17/Encryption-Europe-Webinar-Series-May-31,-2021.html',
},
{
  title: 'InterCamp',
  start: '2021-05-22',
},
{
  "title": 'Séminaire EHESS',
  "start": '2021-05-20',
  "url": 'https://www.csi.minesparis.psl.eu/seminaires/seminaire-sante-et-big-data/',
},
{
  "title": 'Rencontre LDH',
  "start": '2021-04-28',
},
{
  "title": 'Webinar RGPD - CNIL',
  "start": '2021-03-25',
  "url": '{{ site.baseurl }}/2021/03/19/webinar-rgpd-mars',
},
{
  "title": 'Ordonnance du Conseil Etat',
  "start": '2021-03-12',
  "url": '{{ site.baseurl }}/2021/03/12/communique-presse-decision-ce-rendezvous',
},
{
  "title": 'Audience Conseil Etat',
  "start": '2021-03-08',
  "url": '{{ site.baseurl }}/2021/02/26/communique-presse-conseil-etat-vaccination',
},
{
  "title": 'Avis CNAM / HDH',
  "start": '2021-02-19',
  "url": '{{ site.baseurl }}/2021/02/19/depeche-hdh-cnam',
},
{
  "title": 'PIA du HDH',
  "start": '2021-02-21',
  "url": '{{ site.baseurl }}/2021/02/21/pia-hdh-octobre-2020',
},
{
  "title": 'Commission Latombe',
  "start": '2021-02-18',
  "url": '{{ site.baseurl }}/2021/02/22/commission-souverainete',
},

{
  "title": 'Webinar RGPD',
  "start": '2021-02-11T18:00:00',
  "url": '{{ site.baseurl }}/2021/02/08/webinar-rgpd-fevrier',
},
{
  "title": 'FOSDEM',
  "start": '2021-02-06',
  "end": '2021-02-08',
  "url": '{{ site.baseurl }}/2021/02/04/fosdem-2021',
},
{
  title: 'HDS',
  start: '2021-02-15',
},
{
  title: 'InterCamp',
  start: '2021-02-06',
},
// Reu Interhop bi mensuelle
{
  title: 'InterHop Bi-mensuelle',
  start: '2021-01-09T17:00:00',
  end: '2021-01-09T18:00:00',
},
{
  title: 'InterHop Bi-mensuelle',
  start: '2021-01-23T17:00:00',
  end: '2021-01-23T18:00:00',
},
// InterCHU
{
  title: 'Webinar InterCHU',
  start: '2021-01-21T14:00:00',
  end: '2021-01-21T17:00:00',
  url: '{{ site.baseurl }}/2021/01/08/reunion-interchu',
},
// Demande audit sécu DINUM
{
  title: 'LRAR DINUM',
  start: '2020-12-23',
  url: '{{ site.baseurl }}/2020/12/23/demande-acces-documents-dinum',

},
{
  title: 'CADA DINUM',
  start: '2021-01-25',
  url: '{{ site.baseurl }}/2020/12/23/demande-acces-documents-dinum'
},
// Demande PIA
{
  title: 'LRAR HDH',
  start: '2020-12-23',
  url: '{{ site.baseurl }}/2020/12/23/demande-acces-documents-hdh-aipd',

},
{
  title: 'CADA HDH',
  start: '2021-01-25',
  url: '{{ site.baseurl }}/2020/12/23/demande-acces-documents-hdh-aipd',
},
{
  title: 'Webinar InterHop',
  start: '2020-12-16',
  url: '{{ site.baseurl }}/2020/12/13/webinar-campagne-dons-2020',
},
{
  title: "[Ext] Webinar 'ExtraTerritorialité'",
  start: '2020-12-14',
  url: 'https://france-ameriques.org/evenement/lextraterritorialite-du-droit-vecteur-dinfluence-strategique/',
},
{
  title: 'Tribune Libé',
  start: '2020-12-14',
  url: '{{ site.baseurl }}/2020/12/14/stophealthdatahub-donnees-de-sante-en-otage-chez-microsoft',

},
{
  title: 'Webinar EGRN',
  start: '2020-12-07',
  url: 'https://www.egrn.fr/la_sant_fait_sa_r_volution_num_rique_mais_comment_sous_le_contr_le_de_qui_au_profit_de_qui?utm_campaign=campagne_lancement&utm_medium=email&utm_source=pcf',
},
{
  title: 'Saisine CNIL',
  start: '2020-12-07',
  url: '{{ site.baseurl }}/2020/12/07/saisine-cnil-doctolib-alan-lifen',
  color: 'red',    // an option!
},
{
  title: "[Ext] Webinar 'A propos du HDH'",
  start: '2020-11-30',
  url: 'https://france-ameriques.org/evenement/a-propos-de-laffaire-health-data-hub/',
},
{
  title: '[Ext] Demande code source HDH',
  start: '2020-11-24',
  url: 'https://mstdn.io/web/statuses/105357832765154287',
},
{
  title: 'Courrier Véran',
  start: '2020-11-19',
  url: 'https://www.documentcloud.org/documents/7331959-Courrier-Veran.html#document/p2',
  color: 'red',    // an option!

},
// Mission Bothorel
{
  title: 'Mission Bothorel',
  start: '2020-11-17T16:00:00',
  end: '2020-11-17T17:00:00',
  url: '{{ site.baseurl }}/2020/11/17/bothorel-open-data',

},
// Conseil d'etat
{
  title: 'Conseil Etat',
  start: '2020-10-13',
  url: '{{ site.baseurl }}/2020/10/14/communique-presse-conseil-detat-reconnait-acces-donnes',

},
// Avis CNIL HDH
{
  title: 'CNIL : avis HDH',
  start: '2020-10-08',
  url: 'https://cnll.fr/documents/35/OBSERVATIONS_DE_LA_CNIL_8_OCTOBRE_2020.pdf',
  color: 'red',    // an option!

},
{
  title: "Radio 'Libre à Vous'",
  start: '2020-09-29',
  url: 'https://april.org/libre-a-vous-radio-cause-commune-transcription-de-l-emission-du-29-septembre-2020',
},
// Petition sénat
{
  title: 'Pétition Sénat',
  start: '2020-09-21',
  url: '{{ site.baseurl }}/2020/09/21/petition-senat',

},
{
  title: "Débat 'Souveraineté et des libertés à l'ère numérique'",
  start: '2020-08-22',
  url: 'https://journees-ecologistes.fr/emission/numerique-libertes-souverainete-securite-sortir-des-contradictions/',

},

// Invalidation Privacy Shield
{
  title: '[Ext] Fin Privacy Shield',
  start: '2020-07-16',
  url: '{{ site.baseurl }}/2020/08/14/le-hdh-doit-choisir-un-hebergeur-europeen#fn:CJUE_PrivacyShield',  
  color: 'red',    // an option!
},
{
  title: "Radio 'Libre à Vous'",
  start: '2020-01-28',
  url: 'https://april.org/libre-a-vous-radio-cause-commune-transcription-de-l-emission-du-28-janvier-2020',
},
